$(document).ready(function() {
    $("#locations").select2();

    checkLocationsChecked();

    $('input[name="2"]').on('change', function() {
        checkLocationsChecked();
    });

    function checkLocationsChecked() {
        if($('input[name="2"]').prop('checked'))  {
            $('#locations_block').show();
        } else {
            $('#locations_block').hide();
        }
    }
});



