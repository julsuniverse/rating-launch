$(document).ready(function() {
    var lat = "";
    var long = "";

    getLocation();

    var results = [];
    var results_no_format = [];
    var select = $('.change-location').select2({
        ajax: {
            url: '/onboarding/search',
            //url: '/leads/find',
            //delay: 500,
            data: function(params){
                return {
                    term: params.term,
                    lat: lat,
                    long: long
                };
            },
            processResults: function (data) {
                results = data.results;
                console.log('data', data);
                formattedResults = data.results.map(function(item){
                    if (item.id != 'custom'){
                        item.name = item.text;
                        item.text = item.text + " at " + item.address;
                    }
                    return item;
                });
                return {
                    results: formattedResults,
                };
            }
        },
       width: 'resolve'
    });

    $(".change-location").change(function(){
        let bussinessId = $(".change-location option:selected").val();

        let picked = results.filter(function(r){
            return r.id == bussinessId
        })[0];

        console.log('picked', picked);

        $("#place_name").text(picked.name);
        $("#place_address").text(picked.address);
        $("#place_icon").attr('src', picked.image);
        $('#place_rating').html(getRatingHtml(picked.rating));

        $("[name=name]").val(picked.name);
        $("[name=full_address]").val(picked.address);
        $("[name=icon]").val(picked.name);
        $("[name=google_rating]").val(picked.rating);

    });

    function getRatingHtml(rating) {
        var html_rating = '';
        for(var i =0; i <  Math.round(rating); i++) {
            html_rating += "<i class='mdi mdi-star' style='color: gold'></i>";
        }
        for(var i = 0; i < 5 - Math.round(rating); i++) {
            html_rating += "<i class='mdi mdi-star' style='color: gray'></i>";
        }

        return html_rating;
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        console.log("Lat: " + lat);
        console.log("Long: " + long);
    }

    $('#location-name-container').hide();

    $('#change_location').on('click', function() {
        $('#location-name-container').show();
    });

    $('#confirm_button').on('click', function () {
        $('#change_location_form').submit();
    })
});

