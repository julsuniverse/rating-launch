$(document).ready(function() {
    var lat = "";
    var long = "";

    var old_place_id = null;
    var new_place_id = null;

    $('#location-name-container').hide();
    $('.change-competitor').on('click', function () {
        $('#location-name-container').show();
        ($(this).parents('.competitor')).after($('#location-name-container'));
        old_place_id = $(this).data('place');
    });

    getLocation();

    var results = [];
    var results_no_format = [];
    var select = $('.change-location').select2({
        ajax: {
            url: '/insights/search-competitors',
            data: function() {
                return {
                    term: $(this).data('term'),
                    lat: lat,
                    long: long
                };
            },
            method: 'POST',
            processResults: function (data) {
                results = data;
                console.log('data', data);
                formattedResults = data.map(function(item){
                    if (item.id != 'custom'){
                        item.text = item.name + " at " + item.vicinity;
                    }
                    return item;
                });
                return {
                    results: formattedResults,
                    //results: results,
                };
            }
        },
    });

    $(".change-location").change(function(){
        let bussinessId = $(".change-location option:selected").val();

        let picked = results.filter(function(r){
            return r.id == bussinessId
        })[0];

        new_place_id = picked['place_id'];
        console.log('picked', picked);

        $.ajax({
            url: '/insights/save-competitor',
            method: 'POST',
            data: {
                function() {
                    return {
                        old_place_id: old_place_id,
                        new_place_id: new_place_id,
                    };
                },
            },
            success: function (data) {
                console.log('success', data);
            },
            error: function (data) {
                console.log('error', data);
            }
        });

        /*$("#place_name").text(picked.name);
        $("#place_address").text(picked.address);
        $("#place_icon").attr('src', picked.image);
        $('#place_rating').html(getRatingHtml(picked.rating));

        $("[name=name]").val(picked.name);
        $("[name=full_address]").val(picked.address);
        $("[name=icon]").val(picked.name);
        $("[name=google_rating]").val(picked.rating);*/

    });

    function getRatingHtml(rating) {
        var html_rating = '';
        for(var i =0; i <  Math.round(rating); i++) {
            html_rating += "<i class='mdi mdi-star' style='color: gold'></i>";
        }
        for(var i = 0; i < 5 - Math.round(rating); i++) {
            html_rating += "<i class='mdi mdi-star' style='color: gray'></i>";
        }

        return html_rating;
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        console.log("Lat: " + lat);
        console.log("Long: " + long);
    }

    $('#confirm_button').on('click', function () {
        $('#change_location_form').submit();
    })
});