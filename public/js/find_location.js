$(document).ready(function() {
    var lat = "";
    var long = "";

    getLocation();

    var results = [];
    var select = $('.find-location').select2({
        ajax: {
            url: '/onboarding/search',
            delay: 1000,
            data: function(params){
                return {
                    term: params.term,
                    lat: lat,
                    long: long
                };
            },
            processResults: function (data) {
                results = data.results;
                formattedResults = data.results.map(function(item){
                    if (item.id != 'custom'){
                        item.text = item.text + " at " + item.address;
                    }
                    return item;
                });
                return {
                    results: formattedResults
                };
            }
        }
    });

    $(".find-location").change(function(){
        let bussinessId = $(".find-location option:selected").val();
        console.log(bussinessId);
        if (bussinessId == 'custom'){
            select.select2('destroy');
            $("[name=street]").val("");
            $("[name=city]").val("");
            $("[name=state]").val("");
            $("[name=zip]").val("");
            $("[name=country]").val("");
            $("[name=location_name]").val("");
            $("[name=yelp_url]").val("");
            $("#location_text").removeClass('hidden');
            $(".find-location").addClass('hidden');
            $("#location-detailed-information").removeClass('hidden');
            $("#company-img-container").addClass('hidden');
            $("#company-img-container").css('width', '0');
            //$("#location-name-container").css('width', 'calc(100% - 20px)');
            //$("#location-name-container").css('margin-left', '10px');

        }else{
            let picked = results.filter(function(r){
                return r.id == bussinessId
            })[0];

            $("[name=street]").val(picked.street);
            $("[name=city]").val(picked.city);
            if(picked.state) {
                $("[name=state]").val(picked.state);
            } else {
                $("[name=state]").val("");
            }
            $("[name=country]").val(picked.country);
            $("[name=location_name]").val(picked.text);
            $("[name=places_id]").val(picked.places_id);
            $("#company-img")[0].src = picked.image
            $("#company-img").removeClass('hidden');
        }

    });

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        console.log("Lat: " + lat);
        console.log("Long: " + long);
    }
});

