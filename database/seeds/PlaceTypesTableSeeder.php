<?php

use Illuminate\Database\Seeder;

class PlaceTypesTableSeeder extends Seeder
{
    /** @var array $place_types
     *  @link https://developers.google.com/places/web-service/supported_types
     */
    private $place_types = ['dentist', 'doctor', 'cafe', 'bar', 'restaurant'];

    public function run()
    {
        foreach ($this->place_types as $place_type) {
            DB::table('place_types')->insert(['name' => $place_type]);
        }
    }
}
