<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    private $permissions = [
        [
            'id' => 1,
            'name' => 'Billing'
        ],
        [
            'id' => 2,
            'name' => 'View Location(s)'
        ],
        [
            'id' => 3,
            'name' => 'Edit Location Details'
        ],
        [
            'id' => 4,
            'name' => 'Manage Users'
        ],
        [
            'id' => 5,
            'name' => 'Respond to reviews'
        ],
        [
            'id' => 6,
            'name' => 'Insights Dashbaord'
        ]
    ];
    public function run()
    {
        DB::table('permissions')->insert($this->permissions);
    }
}
