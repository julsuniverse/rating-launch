<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->integer('location_id');
            $table->integer('company_id');
            $table->double('rating');
            $table->text('text');
            $table->boolean('starred')->default(0);
            $table->text('review_source_id');
            $table->text('native_review_id');
            $table->text('user_fullname');
            $table->text('user_image_url');
            $table->double('ambiance')->nullable();
            $table->double('expertise')->nullable();
            $table->double('innovation')->nullable();
            $table->double('reccomendability')->nullable();
            $table->double('team')->nullable();
            $table->double('time')->nullable();
            $table->double('trust')->nullable();
            $table->double('value')->nullable();
            $table->timestamp('native_created_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
