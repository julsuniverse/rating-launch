<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditableLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('editable_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_user_permissions_id');
            $table->unsignedInteger('location_id');

            $table->foreign('company_user_permissions_id')
                ->references('id')
                ->on('company_user_permissions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('editable_locations', function (Blueprint $table) {
            $table->dropForeign(['company_user_permissions_id']);
            $table->dropForeign(['location_id']);
        });

        Schema::dropIfExists('editable_locations');
    }
}
