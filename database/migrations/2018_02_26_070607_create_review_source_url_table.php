<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewSourceUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_source_url', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->integer('review_category_id');
            $table->text('url');
            $table->text('data1')->nullable();
            $table->text('data2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_source_url');
    }
}
