<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkFromLocationsToPlaceTypes extends Migration
{
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->addColumn('integer', 'place_type')->unsigned()->nullable();

            $table->foreign('place_type')
                ->references('id')
                ->on('place_types')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropForeign(['place_type']);
            $table->dropColumn(['place_type']);
        });
    }
}
