<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->integer('company_id');
            $table->integer('review_source_id');
            $table->integer('review_source_url_id');
            $table->text('native_id');
            $table->text('url');
            $table->boolean('is_primary')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_media');
    }
}
