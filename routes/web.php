<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();

Route::get('/cron/reviews', 'CronController@getReviews');
Route::get('/cron/rate_reviews', 'CronController@rateReviews');
Route::get('/cron/daily', 'CronController@daily');

// Kiosk Mode
Route::get('/locations/{location}/reviewer', 'ReviewerController@createForLocation');
Route::post('/locations/{location}/reviewer', 'ReviewerController@storeForLocation');
Route::get('/locations/{location}/reviewer/success/{reviewer}', 'ReviewerController@successForLocation');
Route::get('/locations/{location}/write/{reviewer}', 'WriteReviewController@index');
Route::post('/locations/{location}/write/{reviewer}', 'WriteReviewController@store');
Route::get('/reviewer/{reviewer}/unsubscribe', 'ReviewerController@unsubscribe');

Route::get('/sms', 'CommunicationController@sms');

//for iframe
Route::get('/reviews/{location}/{user}', 'ReviewController@getLocationReviews')->name('getLocationReviews');

Route::group(['middleware' => 'auth'], function () {
	Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::post('/insights/search-competitors', 'InsightController@search');
    Route::post('/insights/save-competitor', 'InsightController@saveCompetitor');
    Route::get('/insights/competitors/{location}', 'InsightController@competitors')->name('competitors');
	Route::resource('/insights', 'InsightController');

	Route::get('/users/{user}/login', 'UserManagementController@supervise');
    Route::get('/users/create', 'UserManagementController@create')->name('users.create');
    Route::resource('/users', 'UserManagementController', ['except' => 'create']);

    Route::get('/team-members/{company}', 'TeamMemberController@index')->name('team-members.index');
    Route::get('/team-members/create/{company?}', 'TeamMemberController@create')->name('team-members.create');
    Route::get('/team-members/{companyUser}/edit', 'TeamMemberController@edit')->name('team-members.edit');
    Route::post('/team-members/{companyUser}', 'TeamMemberController@update')->name('team-members.update');
    Route::resource('/team-members', 'TeamMemberController', ['except' => 'index', 'create', 'edit', 'update']);

    //Route::resource('/permissions', 'PermissionController', ['only' => ['edit', 'update']]);
    Route::get('/permissions/{user}', 'PermissionController@edit')->name('permissions-edit');
    Route::post('/permissions/{user}', 'PermissionController@edit')->name('permissions-update');

	// Dashboard
	Route::get('/dashboard/location/{location}', 'DashboardController@setActiveLocation');
	Route::resource('/', 'DashboardController');
	Route::group(['middleware' => 'admin'], function () {
        Route::get('/super-dashboard', 'SuperDashboardController@index');
    });

    // Reviews
    Route::get('/reviews/reviews', 'ReviewController@getReviews');
	Route::post('/reviews/{review}/star', 'ReviewController@toggleStarred');

	Route::get('/reviews/{review}/{read}', 'ReviewController@show');
	Route::resource('/reviews', 'ReviewController');

	// Locations
    Route::get('/locations/{location}/reviews-widget', 'LocationController@reviewsWidget')->name('reviews-widget');
    //Route::put('/locations/{location}')
    Route::resource('/locations', 'LocationController');

    // Settings
	Route::get('/settings/primaryCard/{card}', 'SettingController@primaryCard');
	Route::post('/settings/saveCard', 'SettingController@saveCard');
	Route::resource('/settings', 'SettingController');

	// Reviews
	Route::get('/reviewers/import', 'ReviewerController@showImport');
	Route::get('/reviewers/{reviewer}/textForReview', 'ReviewerController@textForReview');
	Route::get('/reviewers/{reviewer}/emailForReview', 'ReviewerController@emailForReview');
	Route::get('/reviewers/{reviewer}/newCommunication', 'ReviewerController@newCommunication');
	Route::post('/reviewers/{reviewer}/communication', 'ReviewerController@sendCommunication');
	Route::post('/reviewers/import', 'ReviewerController@import');
	Route::resource('/reviewers', 'ReviewerController');

	// Sentiments
	Route::post('/sentiments/{category}/words', 'SentimentController@addWord');
	Route::put('/sentiments/{category}/words/{word}', 'SentimentController@editWord');
	Route::get('/sentiments/{category}/words/{word}/remove', 'SentimentController@deleteWord');
	Route::get('/sentiments/run', 'SentimentController@run');
	Route::resource('/sentiments', 'SentimentController');

	// Onboarding
	Route::get('/onboarding/search', 'OnboardingController@businessSearch');
	Route::get('/onboarding/coupon_lookup', 'OnboardingController@checkCoupon');
	Route::get('/onboarding/{step}', 'OnboardingController@showStep');
	Route::post('/onboarding/1', 'OnboardingController@saveInfo');
	Route::post('/onboarding/2', 'OnboardingController@addSource');
	Route::post('/onboarding/3', 'OnboardingController@saveCard');
	Route::post('/onboarding/4', 'OnboardingController@termsAccepted');
		
	// Sources
	Route::get('/locations/{location}/sources/fbpages', 'SourceController@getFbPages');
	Route::get('/locations/{location}/sources/googpages', 'SourceController@getGoogPages');
	Route::get('/locations/{location}/sources/yelpplaces', 'SourceController@getYelpPlaces');
	Route::get('/locations/{location}/sources/{source}/delete', 'SourceController@destory');
	Route::get('/oauth/googleredirect', 'SourceController@googleOauth');
    Route::post('/locations/store-media', 'LocationController@storeMedia');
    Route::resource('/locations/{location}/sources', 'SourceController');

    // Contact
    Route::post('/contact', 'ContactController@submit')->name('contact');

    //Notifications
    Route::get('/notifications', 'NotificationController@index')->name('notifications');

    //Potential leads
    Route::put('/leads/confirm', 'LeadController@confirm')->name('leads.confirm');
    Route::get('/leads/import', 'LeadController@import')->name('leads.import');
    Route::post('/leads/upload', 'LeadController@upload')->name('leads.upload');
    Route::get('/leads/check/{lead}', 'LeadController@check')->name('leads.check');
    Route::get('leads/{lead}/edit/{check?}', 'LeadController@edit')->name('leads.edit');
    Route::put('/leads/{lead}/{check?}', 'LeadController@update')->name('leads.update');
    Route::resource('/leads', 'LeadController', ['except' => ['edit', 'update']]);
    //Route::resource('/leads', 'LeadController');

    // Testing :)
    Route::get('/tests/getGoogle', 'TestController@getGoogle');
    Route::get('/tests/getFb', 'TestController@getFbReviews');

});