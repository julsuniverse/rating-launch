<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Location
 *
 * @property int $id
 * @property int|null $company_id
 * @property string $text
 * @property string $street
 * @property string $zip
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $address
 * @property-read mixed $average_rating
 * @property-read mixed $hashed_id
 * @property-read mixed $media_url
 * @property-read mixed $rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LocationMedia[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReviewCount[] $reviewCounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReviewSourceUrl[] $sources
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location withoutTrashed()
 */
	class Location extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LocationMedia
 *
 * @property int $id
 * @property int $location_id
 * @property int|null $company_id
 * @property int $review_source_id
 * @property int $review_source_url_id
 * @property string $native_id
 * @property string $url
 * @property int $is_primary
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereIsPrimary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereNativeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereReviewSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereReviewSourceUrlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocationMedia whereUrl($value)
 */
	class LocationMedia extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereName($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ReviewCount
 *
 * @property int $id
 * @property int $company_id
 * @property int $location_id
 * @property int $review_source_id
 * @property int $count
 * @property float $rating
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\ReviewSource $source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereReviewSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewCount whereUpdatedAt($value)
 */
	class ReviewCount extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Review
 *
 * @property int $id
 * @property string|null $title
 * @property int $location_id
 * @property int|null $company_id
 * @property string $text
 * @property float $rating
 * @property int $starred
 * @property string $review_source_id
 * @property string $native_review_id
 * @property string $user_fullname
 * @property string $user_image_url
 * @property \Carbon\Carbon|null $created_at
 * @property float|null $ambiance
 * @property float|null $expertise
 * @property float|null $innovation
 * @property float|null $reccomendability
 * @property float|null $team
 * @property float|null $time
 * @property float|null $trust
 * @property float|null $value
 * @property string|null $native_created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $read
 * @property-read \App\Models\Company|null $company
 * @property-read mixed $formatted_time
 * @property-read mixed $icon_html
 * @property-read mixed $short_text
 * @property-read \App\Models\Location $location
 * @property-read \App\Models\Reviewer $reviewer
 * @property-read \App\Models\ReviewSource $source
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereAmbiance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereExpertise($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereInnovation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereNativeCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereNativeReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereReccomendability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereReviewSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereStarred($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereTrust($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUserFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUserImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review withoutTrashed()
 */
	class Review extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $address
 * @property-read mixed $media_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $locations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LocationMedia[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReviewCount[] $reviewCounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 */
	class Company extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyUser
 *
 * @property int $id
 * @property int $company_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $subscription
 * @property int $role_id
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUser whereUserId($value)
 */
	class CompanyUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notification
 *
 * @property int $id
 * @property string $text
 * @property string $uri
 * @property int $notification_type_id
 * @property int $read
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification setNotificationRead($review_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereNotificationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUserId($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Plan
 *
 * @property int $id
 * @property string $text
 * @property string $stripe_id
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Plan whereUpdatedAt($value)
 */
	class Plan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ReviewSource
 *
 * @property int $id
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSource whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSource whereUpdatedAt($value)
 */
	class ReviewSource extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Communication
 *
 * @property int $id
 * @property string $method
 * @property string $type
 * @property int $reviewer_id
 * @property string $text
 * @property int|null $sender_id
 * @property int $from_client
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $read
 * @property-read \App\Models\Reviewer $reviewer
 * @property-read \App\Models\User|null $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereFromClient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereReviewerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Communication whereUpdatedAt($value)
 */
	class Communication extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Reviewer
 *
 * @property int $id
 * @property string $name
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $review_id
 * @property int $location_id
 * @property int $was_texted
 * @property int $was_emailed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Communication[] $communications
 * @property-read \App\Models\Location $location
 * @property-read \App\Models\Review|null $review
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereWasEmailed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reviewer whereWasTexted($value)
 */
	class Reviewer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EditableLocation
 *
 * @property int $id
 * @property int $company_user_permissions_id
 * @property int $location_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EditableLocation whereCompanyUserPermissionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EditableLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EditableLocation whereLocationId($value)
 */
	class EditableLocation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Lead
 *
 * @property int $id
 * @property string|null $salutation
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string $email
 * @property string|null $address_1
 * @property string|null $address_2
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $phone
 * @property string|null $place_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead wherePlaceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereSalutation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereZip($value)
 */
	class Lead extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ReviewSourceUrl
 *
 * @property int $id
 * @property int $location_id
 * @property int $review_category_id
 * @property string $url
 * @property string|null $data1
 * @property string|null $data2
 * @property string|null $data3
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $rate_url
 * @property-read \App\Models\ReviewSource $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereData1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereData2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereData3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereReviewCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReviewSourceUrl whereUrl($value)
 */
	class ReviewSourceUrl extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyUserPermission
 *
 * @property int $id
 * @property int $permission_id
 * @property int $company_user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUserPermission whereCompanyUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUserPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyUserPermission wherePermissionId($value)
 */
	class CompanyUserPermission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property string $customer
 * @property string $stripe_id
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription canceled()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription trialing()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereUpdatedAt($value)
 */
	class Subscription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $role_id
 * @property string|null $stripe_id
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Company[] $companies
 * @property-read \App\Models\CompanyUser $companyUser
 * @property-read mixed $gravatar_url
 * @property-read mixed $hashed_id
 * @property-read mixed $locations
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SentimentWord
 *
 * @property int $id
 * @property int $sentiment_category_id
 * @property string $word
 * @property int $score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereSentimentCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentWord whereWord($value)
 */
	class SentimentWord extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SentimentCategory
 *
 * @property int $id
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SentimentWord[] $words
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentCategory whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SentimentCategory whereUpdatedAt($value)
 */
	class SentimentCategory extends \Eloquent {}
}

