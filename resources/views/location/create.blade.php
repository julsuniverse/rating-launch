@extends('layouts.dashboard')

@section('title')
    Add Location
@endsection

@section('content')
    <div class="row">
    	<div class="col-lg-9 col-md-12">
	    	<div class="card-box">
	        	<form class="form-horizontal" role="form" action="/locations" method="post" id="location-create-form">
	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input class="form-control hidden" name="places_id" type="hidden">

                    <div class="row">
                        <div id="company-img-container" class="col-12">
                            <div class="bg-icon bg-icon-primary onboarding-icon m-auto">
                                <img src="" class="hidden onboarding-icon-img" id='company-img'>
                            </div>
                        </div>
                        <div id="location-name-container" class="col-12">
                            <div class="form-group clearfix row m-t-10">
                                <label class="col-3 col-form-label" for="location_name">Location Name</label>
                                <div class="hidden col-9" id="location_text">
                                    <input class="form-control required" name="location_name" type="text" value="{{ old('text') }}">
                                </div>
                                <select class="form-control select2 find-location col-9 " name='yelp_id'></select>
                            </div>
                        </div>
                    </div>

                    <div id='location-detailed-information' class="hidden">

                        <div class="form-group row">
                            <label for="street" class="col-3 col-form-label">Street</label>
                            <div class="col-9">
                                <input class="form-control" name='street' value="{{ old('street') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="city" class="col-3 col-form-label">City</label>
                            <div class="col-9">
                                <input class="form-control" name='city' value="{{ old('city') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="state" class="col-3 col-form-label">State</label>
                            <div class="col-9">
                                <input class="form-control" name='state' value="{{ old('state') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-3 col-form-label">Zip</label>
                            <div class="col-9">
                                <input class="form-control" name='zip' value="{{ old('zip') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country" class="col-3 col-form-label">Country</label>
                            <div class="col-9">
                                <input class="form-control" name='country' value="{{  old('country') ?? 'USA' }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-b-0 row">
                        <div class="offset-3 col-9">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>

			    </form>
			</div>
		</div>
    </div>
    <!-- end row -->

@endsection

@push('scripts')
    <script src="{{ asset('js/find_location.js') }}"></script>
@endpush

