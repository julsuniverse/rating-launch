@extends('layouts.dashboard')

@section('title')
    Locations
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">All Locations
                	<div class="pull-right">
                		<a href="/locations/create" class="pull-right btn btn-success btn-sm" id="btn-archive">Add</a>
                	</div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
	                    <tr>
	                        <th>#</th>
	                        <th>Location Name</th>
	                        <th>Avg Rating</th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($locations as $location)
		                    <tr>
		                        <td>{{ $location->id }}</td>
		                        <td><a href='/locations/{{ $location->id }}'>{{ $location->text }}</a></td>
		                        <td>{{ $location->average_rating }}</td>
		                    </tr>
		                @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection