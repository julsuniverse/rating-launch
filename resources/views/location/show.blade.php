@extends('layouts.dashboard')

@section('title')
    {{ $location->text }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Location Information
                    <div class="pull-right">
                        <a href="{{ route('reviews-widget', ['location' => $location->id]) }}" class="btn btn-secondary waves-effect" style="padding: 4px 11px;"><i class="mdi mdi-comment-multiple-outline"></i> Get Reviews Widget</a>
                        @include('includes.kiosk-button', ['locations'=> collect([$location])])
                    </div>
                </h4>

                <form class="form-horizontal" role="form" action="{{ route('locations.update', ['location' => $location->id]) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group row">
                        <label for="inputEmail3" class="col-3 col-form-label">Name</label>
                        <div class="col-9">
                            <input class="form-control" name='text' value='{{ $location->text }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword3" class="col-3 col-form-label">Street</label>
                        <div class="col-9">
                            <input class="form-control" name='street' value='{{ $location->street }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword4" class="col-3 col-form-label">City</label>
                        <div class="col-9">
                            <input class="form-control" name='city' value='{{ $location->city }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword4" class="col-3 col-form-label">State</label>
                        <div class="col-9">
                            <input class="form-control" name='state' value='{{ $location->state }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword4" class="col-3 col-form-label">ZIP</label>
                        <div class="col-9">
                            <input class="form-control" name='zip' value='{{ $location->zip }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword4" class="col-3 col-form-label">Country</label>
                        <div class="col-9">
                            <input class="form-control" name='country' value='{{ $location->country ?? 'USA' }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword4" class="col-3 col-form-label">Place Type</label>
                        <div class="col-9">
                            <select class="form-control select2" name="place_type" data-placeholder="Select Place Type">
                                <option>Select Place Type...</option>
                                @foreach ($place_types as $place_type)
                                    <option value="{{ $place_type->id }}" @if($place_type->id == $location->place_type) selected @endif>{{ $place_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-b-0 row">
                        <div class="offset-3 col-9">
                            <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-7">
        	<div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Review Sources
                	<div class="pull-right">
                		<a href="/locations/{{ $location->hashed_id }}/sources/create" class="pull-right btn btn-success btn-sm" id="btn-archive">Add</a>
                	</div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
	                    <tr>
	                        <th>#</th>
	                        <th>Type</th>
	                        <th>Link</th>
                            <th style="width: 20px;"></th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($location->sources as $source)
		                    <tr>
		                        <td>{{ $source->id }}</td>
		                        <td><a href='/locations/{{ $location->id }}/sources/{{ $source->id }}'>{{ $source->type->text }}</a></td>
		                        <td><a href="{{ $source->url }}" target="_blank">link</a></td>
                                <td><a href="/locations/{{ $location->id }}/sources/{{ $source->id }}/delete"><i class="fa fa-trash"></i></a></td>
		                    </tr>
		                @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <!-- end row -->

    <div class="row location-media">
        <div class="col-md-12">
            <div class="card-box">

            <div id="save-alert" class="d-none">
                <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                    <strong>Saved</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

                <form method="POST" action="/locations/store-media">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h4 class="text-dark header-title m-t-0 m-b-20">Location media
                        <div class="pull-right">
                            <button type="submit" class="pull-right btn btn-success btn-sm" id="btn-archive">Make primary</button>
                        </div>
                    </h4>

                    <div class="row">
                        @foreach($media as $img)
                            <div class="col text-center m-b-20 one-media">
                                <a href="javascript:void(0)"  data-location="{{ $location->id }}">
                                    <div class="checkbox checkbox-success checkbox-circle checkbox-single media-checkbox">
                                        <input type="checkbox" @if($img->is_primary) checked  @endif data-media="{{ $img->id }}">
                                        <label></label>
                                    </div>
                                    <img src="{{ $img->url }}" >
                                </a>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.one-media a').on('click', function(e) {
                if(!$(this).find('input[type="checkbox"]').is(':checked')) {
                    $(document).find('input[type="checkbox"]').attr('checked', false);
                    $(this).find('input[type="checkbox"]').attr('checked', 'checked');
                } else {
                    $(this).find('input[type="checkbox"]').attr('checked', false);
                }
            });

            $('.location-media form').submit(function (e) {
                e.preventDefault();

                var form = $(this);
                var data = {
                    'media_id' : form.find('input[type="checkbox"]:checked').data('media'),
                    'location_id' : form.find('a').data('location'),
                    '_token': "{{ csrf_token() }}"
                };

                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: data,
                    success: function (response) {
                        $('#save-alert').removeClass('d-none').addClass('d-block');
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });

            });
        });
    </script>
@endpush





