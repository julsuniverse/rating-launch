<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Rating Launch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/favicon.ico">

        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />

        <script src="/js/modernizr.min.js"></script>

    </head>
    <body>
        <div class="wrapper-page">
            @if(count($errors))
                <br>
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger m-b-10">
                        <strong>Oh snap!</strong> {{ $error }}
                    </div>
                @endforeach
            @endif

            <div class="text-center">
                <img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">
            </div>

            <form class="form-horizontal m-t-20" action="register" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-email"></i></span>
                            <input class="form-control" type="text" required="" placeholder="email" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                            <input class="form-control" type="text" required="" placeholder="name" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-key"></i></span>
                            <input class="form-control" type="password" required="" placeholder="Password" name="password">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="mdi mdi-key"></i></span>
                            <input class="form-control" type="password" required="" placeholder="Confirm Password" name="password_confirmation">
                        </div>
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Signup
                        </button>
                    </div>
                </div>

                <div class="form-group row m-t-30">
                    <div class="col-sm-7">
                        
                    </div>
                    <div class="col-sm-5 text-right">
                        <a href="/login" class="text-muted">Login</a>
                    </div>
                </div>
            </form>
        </div>


        <!-- jQuery  -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="/js/jquery.core.js"></script>
        <script src="/js/jquery.app.js"></script>

    </body>
</html>