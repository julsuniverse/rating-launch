<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Rating Launch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/favicon.ico">

        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />

        <script src="/js/modernizr.min.js"></script>

    </head>
    <body>
        <div class="wrapper-page">

            <div class="text-center">
                <img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">
            </div>
            <form class="text-center m-t-20" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Reset Password
                </div>
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} m-b-0">
                    <div class="input-group">
                        <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ $email or old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} m-b-0">
                    <div class="input-group">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} m-b-0}">
                    <div class="input-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif


                    </div>

                </div>

                <button type="submit" class="btn btn-primary waves-effect waves-light btn-custom">
                    Reset Password
                </button>

            </form>
        </div>

    <!-- jQuery  -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/waves.js"></script>
    <script src="/js/jquery.slimscroll.js"></script>
    <script src="/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="/js/jquery.core.js"></script>
    <script src="/js/jquery.app.js"></script>

    </body>
</html>