@extends('layouts.dashboard')

@section('title')
    Template
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">All Users
                	<div class="pull-right">
                		<a href="{{ route('users.create') }}" class="pull-right btn btn-success btn-sm" id="btn-archive">Add User</a>
                	</div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
	                    <tr>
	                        <th>#</th>
	                        <th>Name</th>
	                        <th>Email</th>
	                        <th>Role</th>
	                        <th></th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($users as $user)
		                    <tr>
		                        <td>{{ $user->id }}</td>
		                        <td>{{ $user->name }}</td>
		                        <td>{{ $user->email }}</td>
		                        <td>{{ $user->role_id }}</td>
		                        <td>
		                        	<a href="/users/{{ $user->id }}/login" class="btn btn-primary">
		                        		<i class="fa fa-blind"></i>
		                        	</a>
								</td>
		                    </tr>
		                @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush