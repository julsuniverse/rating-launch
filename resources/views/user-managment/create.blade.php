@extends('layouts.dashboard')

@section('title')
    @isset($company)
        Add Team Member for {{ $company->text }}
    @else
        Add User
    @endisset
@endsection

@section('content')
    <div class="row">
    	<div class="col-lg-9 col-md-12">
	    	<div class="card-box">
	        	<form class="form-horizontal" role="form" action="{{ $company ? route('team-members.store') : action('UserManagementController@store') }}" method="post">
	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@isset($company)
	        			<input type="hidden" name="company" value="{{ $company->id }}">
					@endisset

			        <div class="form-group row">
			            <label for="member-name" class="col-3 col-form-label">Name</label>
			            <div class="col-9">
								<input class="form-control" id="member-name" name='name' value='{{ old('name') }}'>
						</div>
			        </div>

			        <div class="form-group row">
			            <label for="member-email" class="col-3 col-form-label">Email</label>
			            <div class="col-9">
			                <input class="form-control" id="member-email" type="email" name='email' value='{{ old('email') }}'>
			            </div>
			        </div>

			        <div class="form-group row">
			            <label for="member-title" class="col-3 col-form-label">Title</label>
			            <div class="col-9">
			                <input class="form-control" id="member-title" name='title' value='{{ old('title') }}'>
			            </div>
			        </div>

			        <div class="form-group m-b-0 row">
			            <div class="offset-3 col-9">
			                <button type="submit" class="btn btn-success">Add</button>
			            </div>
			        </div>

			    </form>
			</div>
		</div>
    </div>
    <!-- end row -->

@endsection

