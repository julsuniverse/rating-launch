@extends('layouts.dashboard')

@section('title')
    Sentiment Categories
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">All Sentiment Categories
                	<div class="pull-right">
                		<a href="/sentiments/run" class="pull-right btn btn-success btn-sm" id="btn-archive">Run Analysis</a>
                	</div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
	                    <tr>
	                        <th>#</th>
	                        <th>Name</th>
	                        <th>Words</th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($categories as $category)
		                    <tr>
		                        <td>{{ $category->id }}</td>
		                        <td><a href='/sentiments/{{ $category->id }}'>{{ $category->text }}</a></td>
		                        <td>{{ $category->words()->count() }}</td>
		                    </tr>
		                @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush