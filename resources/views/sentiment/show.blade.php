@extends('layouts.dashboard')

@section('title')
    Senitment: {{ $category->text }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Words
                	<div class="pull-right">
	                	<a class="btn btn-success btn-large" href="#" id="add-dict-bttn">Add</a>
	                </div>
                </h4>
                <br>
                <table class="table mb-0 table-responsive" id="dictionary-table">
                    <thead>
	                    <tr>
	                        <th class="hidden-xs">#</th>
	                        <th>Word</th>
	                        <th>Score</th>
	                        <th class="hidden-xs"></th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($category->words as $word)
		                    <tr>
		                        <td class="not-editable">{{ $word->id }}</td>
		                        <td data-type="label" dict-id="{{ $word->id }}">{{ $word->word }}</td>
		                        <td class="hidden-xs" data-type="value" dict-id="{{ $word->id }}">{{ $word->score }}</td>
		                        <td class="not-editable">
                                    <a href="/sentiments/{{ $category->id }}/words/{{ $word->id }}/remove"><i class="fa fa-times" aria-hidden="true"></i></a>
                                </td>
		                    </tr>
		                @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
	<script type="text/javascript">
        $(function(){
            $("#add-dict-bttn").click(function(){
                $.ajax({
                    url: "/sentiments/" + {{ $category->id }} + "/words", 
                    type: "POST",
                    data: {_token: "{{ csrf_token() }}"},
                    success: function(data, status){
                        location.reload();
                    }
                });

                return false;
            });

            //Editable Table
            $("#dictionary-table").editableTableWidget();
            $("#dictionary-table td").on('change', function(evt, newValue) {
                if ($(this).hasClass('not-editable')){ return false; }

                console.log(this);
                let id = $(this).attr('dict-id');
                let type = $(this).attr('data-type');
                console.log("Id: " + id + " Type: " + type + " New Value: " + newValue);

                var data = {type: type, value: newValue, _token: "{{ csrf_token() }}" };
                $.ajax({
                    url: "/sentiments/" + {{ $category->id }} + "/words/" + id, 
                    type: "PUT",
                    data: data,
                    success: function(data, status){
                        // alert("Data: " + data + "\nStatus: " + status);
                    }
                });

                return true;
            });
            $("#dictionary-table td").on('validate', function(evt, newValue) {
                return !$(this).hasClass('not-editable');
            });


        });

    </script>
@endpush