@extends('layouts.dashboard')

@section('title')
    Settings
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="text-center card-box">
                <div class="member-card">
                    <div class="thumb-xl member-thumb m-b-10 center-block">
                        <img src="{{ $user->gravatar_url}}" class="rounded-circle img-thumbnail" alt="profile-image">
                    </div>

                    <div class="">
                        <h5 class="m-b-5">{{ explode(' ', $user->name)[0] }}</h5>
                    </div>


                    <div class="text-left m-t-40">
                        <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">{{ $user->name }}</span></p>

                        <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ $user->email }}</span></p>

                        <p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15">USA</span></p>
                    </div>

                </div>

            </div> <!-- end card-box -->

            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title">Companies</h4>

                <table class="table mb-0 table-responsive">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Locations</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user->companies as $company)
                            <tr>
                                <td>
                                    <a href="{{ route('team-members.index', ['company' => $company]) }}">
                                        {{ $company->text }}
                                    </a>
                                </td>
                                <td>{{ $company->locations()->count() }}</td>
                                <td class="text-center">
                                    <a href="{{ route('team-members.create', ['company' => $company]) }}" class="btn btn-success btn-sm" id="btn-archive">Add Team Member</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div> <!-- end col -->


        <div class="col-lg-8 col-xl-9">
            <div class="">
                <div class="card-box">
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="nav-item">
                            <a href="#settings" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                Settings
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#payment" data-toggle="tab" aria-expanded="true" class="nav-link">
                                Payment
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings" aria-expanded="true">
                            <form role="form">
                                <div class="form-group">
                                    <label for="FullName">Full Name</label>
                                    <input type="text" name='name' value="{{ $user->name }}" id="FullName" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Email">Email</label>
                                    <input type="email" name='email' value="{{ $user->email }}" id="Email" class="form-control">
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" placeholder="6 - 15 Characters" id="Password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="RePassword">Re-Password</label>
                                    <input type="password" placeholder="6 - 15 Characters" id="RePassword" class="form-control">
                                </div>
                                <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Save</button>
                            </form>
                        </div>

                        <div class="tab-pane" id="payment" aria-expanded="true">
                            <h4>Subscriptions</h4>
                            <table class="table mb-0 table-responsive">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Billable Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customer["subscriptions"]["data"] as $subscription)
                                        <tr>
                                            <td>{{ $subscription["items"]["data"][0]["plan"]["nickname"] }}</td>
                                            <td>
                                                @php
                                                    $rawPrice = $subscription["items"]["data"][0]["plan"]["amount"] / 100;
                                                    $coupon = NULL;
                                                    if (!empty($subscription['discount'])){
                                                        $coupon = $subscription['discount']['coupon'];
                                                    }
                                                    $price = \App\Models\Plan::priceWithCoupon($rawPrice, $coupon);
                                                @endphp
                                                ${{ $price }}
                                            </td>
                                            <td>{{ date('m/d/y', $subscription["current_period_end"]) }}</td>
                                            <td>
                                                <a href="#">
                                                    <button class="btn btn-light"><i class="mdi mdi-alert-octagon"></i></button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br><br>

                            <h4>Cards</h4>
                            <table class="table mb-0 table-responsive">
                                <thead>
                                    <tr>
                                        <th>Card Type</th>
                                        <th>Last 4</th>
                                        <th>Expiration</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customer["sources"]["data"] as $cardSource)
                                        <tr 
                                            @if ($customer["default_source"] == $cardSource['id']) 
                                                style="background: #f9f9f9;"
                                            @endif
                                        >
                                            <td>{{ $cardSource["brand"] }}</td>
                                            <td>{{ $cardSource["last4"] }}</td>
                                            <td>{{ $cardSource["exp_month"] }}/{{ $cardSource["exp_year"] }}</td>
                                            <td>
                                                @if ($customer["default_source"] != $cardSource['id'])
                                                    <a href="/settings/primaryCard/{{ $cardSource['id'] }}">
                                                        <button class="btn btn-light"><i class="mdi mdi-star"></i></button>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br><br>

                            <form action="/settings/saveCard" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_test_cpr0O1VRmVEq1haoTU57lzyy"
                                    data-name="Rating Launch"
                                    data-description="Rating Launch"
                                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                    data-label="Add Card"
                                    data-panel-label="Add Card"
                                    data-locale="auto"
                                    data-email="{{ $user->email }}">
                                </script>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- end col -->
    </div>

@endsection

@push('scripts')

@endpush