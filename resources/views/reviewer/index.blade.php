@extends('layouts.dashboard')

@section('title')
    Reviewers
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">All Reviews
                	<div class="pull-right">
                		<div class="btn-group" style="margin-right: 10px; height: 30px;">
                			@include('includes.kiosk-button', ['locations'=> $locations])
                        </div>
                		<a href="/reviewers/import" class="pull-right btn btn-success btn-sm" id="btn-archive">Import Reviewers</a>
                	</div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
	                    <tr>
	                        <th>#</th>
	                        <th>Name</th>
	                        <th>Date Added</th>
	                        <th>Location</th>
	                        <th>Review</th>
	                        <th>Communication Count</th>
	                        <th>Contact</th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@foreach($reviewers as $reviewer)
		                    <tr>
		                        <td>{{ $reviewer->id }}</td>
		                        <td><a href='/reviewers/{{ $reviewer->id }}'>{{ $reviewer->name }}</a></td>
		                        <td>{{ $reviewer->created_at }}</td>
		                        <td>{{ $reviewer->location->text }}</td>
		                        <td>
		                        	@if (!empty($reviewer->review))
		                        		<i class="mdi mdi-star" style="color: {{ $reviewer->review->rating >= 0.2 ? 'gold' : 'grey'}};"></i>
						                <i class="mdi mdi-star" style="color: {{ $reviewer->review->rating >= 0.4 ? 'gold' : 'grey'}};"></i>
						                <i class="mdi mdi-star" style="color: {{ $reviewer->review->rating >= 0.6 ? 'gold' : 'grey'}};"></i>
						                <i class="mdi mdi-star" style="color: {{ $reviewer->review->rating >= 0.8 ? 'gold' : 'grey'}};"></i>
						                <i class="mdi mdi-star" style="color: {{ $reviewer->review->rating >= 1.0 ? 'gold' : 'grey'}};"></i>
		                        	@else
		                        		no review
		                        	@endif
		                        </td>
		                        <td>
		                        	{{ $reviewer->communications()->count() }}
		                        </td>
		                        <td>
		                        	@if (!empty($reviewer->phone))
		                        		<a href="/reviewers/{{ $reviewer->id }}/textForReview"><button class="btn btn-light"><i class="mdi mdi-cellphone"></i></button></a>
		                        	@endif

		                        	@if (!empty($reviewer->email))
		                        		<a href="/reviewers/{{ $reviewer->id }}/emailForReview"><button class="btn btn-light"><i class="mdi mdi-email"></i></button></a>
		                        	@endif
		                        </td>
		                    </tr>
		                @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush