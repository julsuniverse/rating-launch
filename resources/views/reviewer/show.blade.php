@extends('layouts.dashboard')

@section('title')
    {{ $reviewer->name }}
@endsection

@section('content')
    <div class="row">
        @foreach ($comItems as $item)
            <div class="col-sm-12">
                <div class="card-box m-t-10" style="{{ $item['type'] == 'review' ? 'background-color: #EEEEEE;' : '' }}">
                    <div class="media m-b-30">
                        @if ($item['sender_image_url'] != "")
                            <img class="d-flex mr-3 rounded-circle thumb-sm" src="{{ $item['sender_image_url'] }}" alt="Sender Profile Picture">
                        @endif
                        <div class="media-body">
                            <span class="media-meta pull-right">
                                @if ($item["date"]->diffInDays(Carbon\Carbon::now()) <= 1)
                                    {{ $item["date"]->format('g:i a') }}
                                @else
                                    {{ $item["date"]->format('F j, Y, g:i a') }}
                                @endif
                            </span>
                            
                            @if ($item["from_reviewer"])
                                <h4 class="text-primary font-16 m-0">
                                    <b>{{ $item["sender_name"] }}</b>
                                </h4>
                            @else
                                <h4 class="text-muted font-16 m-0">
                                    {{ $item["sender_name"] }}
                                </h4>
                            @endif
                            <small class="text-muted">From: {{ $item["from"] }}</small>
                        </div>
                    </div>

                    @if ($item["type"] == "review")
                        <i class="mdi mdi-star" style="color: {{ $item['rating'] >= 0.2 ? 'gold' : 'grey'}};"></i>
                        <i class="mdi mdi-star" style="color: {{ $item['rating'] >= 0.4 ? 'gold' : 'grey'}};"></i>
                        <i class="mdi mdi-star" style="color: {{ $item['rating'] >= 0.6 ? 'gold' : 'grey'}};"></i>
                        <i class="mdi mdi-star" style="color: {{ $item['rating'] >= 0.8 ? 'gold' : 'grey'}};"></i>
                        <i class="mdi mdi-star" style="color: {{ $item['rating'] >= 1.0 ? 'gold' : 'grey'}};"></i>
                        <br/><br/>
                    @endif

                    {!! $item["text"] !!}
                </div>
            </div>
        @endforeach
    </div>


    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" role="form" action="/reviewers/{{ $reviewer->id }}/communication" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="communication_type" value="sms">
                <input type="hidden" name="message_type" value="general">

                <div class="media m-b-0">
                    <a href="#" class="pull-left">
                        <img alt="" src="{{ Auth::user()->gravatar_url }}" class="media-object thumb-sm img-circle">
                    </a>
                    <div class="media-body">
                        <div class="card-box">
                            <div class="form-group row">
                                <div class="col-12">
                                    <textarea class="form-control" name="message" style="height: 180px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary waves-effect waves-light w-md m-b-30">
                        <i class="fa fa-mobile-phone"></i>&nbsp;&nbsp;Send Via Text 
                    </button>
                    <button type="submit" class="btn btn-secondary waves-effect waves-light w-md m-b-30" disabled="">
                        <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Send Via Email
                    </button>
                </div>

            </form>
        </div>
    </div>
    <!-- End row -->

@endsection

@push('scripts')

@endpush