@extends('layouts.dashboard')

@section('title')
    Create Communication
@endsection

@section('content')
    <div class="row">
    	<div class="col-lg-9 col-md-12">
	    	<div class="card-box">
	    		<h3>To {{ $reviewer->name }}</h3>
	        	<form class="form-horizontal" role="form" action="/reviewers/{{ $reviewer->id }}/communication" method="post">
	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">

			        <div class="form-group row">
			            <label for="inputEmail3" class="col-3 col-form-label">Message: </label>
			            <div class="col-9">
			                <textarea class="form-control" name="message" {{ $communicationType == 2 ? 'disabled' : '' }}>{{ $prefilledText ?? "" }}</textarea>
			            </div>
			        </div>

			        <div class="form-group row">
			            <label for="inputEmail3" class="col-3 col-form-label">Communication Type: </label>
			            <div class="col-9">
			                <select class="form-control" name="communication_type">
                                <option value="sms"  {{ $communicationType == 1 ? 'selected' : '' }}>SMS Message</option>
                                <option value="email"  {{ $communicationType == 2 ? 'selected' : '' }}>Email</option>
                            </select>
			            </div>
			        </div>

			        <div class="form-group row">
			            <label for="inputEmail3" class="col-3 col-form-label">Message Type: </label>
			            <div class="col-9">
			                <select class="form-control" name="message_type">
			                	<option value="general" {{ $messageType == 1 ? 'selected' : '' }}>General</option>
                                <option value="request_review" {{ $messageType == 2 ? 'selected' : '' }}>Request Review</option>
                            </select>
			            </div>
			        </div>


			        <div class="form-group m-b-0 row">
			            <div class="offset-3 col-9">
			                <button type="submit" class="btn btn-success">Send</button>
			            </div>
			        </div>

			    </form>
			</div>
		</div>
    </div>

@endsection

@push('scripts')
	<script type="text/javascript">
		$("[name=communication_type]").change(function(){
            let option = $("[name=communication_type] option:selected").val();
            console.log(option);
            if (option == "email"){
            	$("[name=message]").attr("disabled", "true");
            }else{
            	$("[name=message]").removeAttr("disabled");
            }
        });
	</script>
@endpush