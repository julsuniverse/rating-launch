 <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>{{ $location->text }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/favicon.ico">


        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />

        <link href="/css/bootstrap-stars.css" rel="stylesheet" type="text/css" />
        <link href="/css/css-stars.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome-stars-o.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome-stars.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

        <script src="/js/modernizr.min.js"></script>

    </head>

    <body>
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-md-8">

                    @if(count($errors))
                        <br>
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger m-b-10">
                                <strong>Oh snap!</strong> {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    @if(session('success'))
                        <br>
                        <div class="alert alert-success m-b-10">
                            <strong>Yay!</strong> {{ session('success') }}
                        </div>
                    @endif

                    <div class="" style="margin-top: 20px;">
                        <div class="row justify-content-md-center">
                            <div class="col-xs-3">
                                <div style="height: 60px; width: 60px; background: white; border-radius: 30px; text-align: center;">
                                    <i class="mdi mdi-home-modern" style="margin-top: 14px; font-size: 30px;"></i>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row justify-content-md-center" style="text-align: center;">
                            <div class="col-xs-12" style="">
                                <p style="font-size: 40px;">
                                    {{ $location->text }}
                                </p>
                                <br/><br/>
                                <p style="font-size: 60px;">
                                    Success, {{ $reviewer->name }}!<br/>
                                    <span style="font-size: 15px;">
                                        We will follow-up shortly to review your experience today!
                                    </span>
                                </p>
                                <a href="/locations/{{ $location->hashed_id }}/reviewer" class="btn btn-secondary waves-effect">
                                    Add another reviewer
                                </a>
                            </div>
                        </div>
                    </div>

                    <div style="position: fixed; right: 20px; bottom: 20px;">
                        <a href="/locations/{{ $location->hashed_id }}/reviewer" style="color: grey;">
                            <i class="mdi mdi-home" style="margin-top: 14px; font-size: 30px;"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>



        <!-- jQuery  -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/js/mindmup-editabletable.js"></script>
        <script src="/js/jquery.barrating.min.js"></script>

        <script type="text/javascript">
           $(function() {
              $('#rating-stars').barrating({
                theme: 'fontawesome-stars'
              });
           });
        </script>
    </body>
</html>