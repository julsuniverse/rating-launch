@extends('layouts.dashboard')

@section('title')
    Import Reviewers
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-12">
            <div class="card-box">
                <form class="form-horizontal" role="form" action="/reviewers/import" method="post" enctype="multipart/form-data">
	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">

	        		<p>Please upload a comma seperated spreadsheet formated by name, phone, email.</p>

			        <div class="form-group row">
			            <label for="inputEmail3" class="col-3 col-form-label">File</label>
			            <div class="col-9">
			                <input class="form-control" name='file' type="file" accept=".csv">
			            </div>
			        </div>

			        <div class="form-group m-b-0 row">
			            <div class="offset-3 col-9">
			                <button type="submit" class="btn btn-success">Import</button>
			            </div>
			        </div>

			    </form>

            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush