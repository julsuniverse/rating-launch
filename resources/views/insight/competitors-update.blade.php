@extends('layouts.dashboard')

@section('title')
    Change Competitors
@endsection

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0">Competitors</h4>

                <div class="competitors">
                    @foreach($competitors as $competitor)
                        <div class="competitor @isset($competitor['my_location']) my-location @endif">
                            <div class="competitors-icon pull-left">
                                <img src="{{ $competitor['icon'] }}" >
                            </div>
                            <div class="competitor-name">
                                {{ $competitor['name'] }}
                                @if(!isset($competitor['my_location']))
                                    <a data-place="{{ $competitor['place_id'] }}"
                                       class="change-competitor"
                                       href="javascript:void(0)">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                @endif
                            </div>
                            <div>
                                @isset($competitor['rating'])
                                    <div>
                                        @for($i = 0; $i < round($competitor['rating'], 0); $i++)
                                            <i class="mdi mdi-star" style="color: gold"></i>
                                        @endfor

                                        @for($i = 0; $i < 5 - round($competitor['rating'], 0); $i++)
                                            <i class="mdi mdi-star" style="color: grey"></i>
                                        @endfor
                                        <span> ({{ $competitor['rating'] }}) </span>
                                    </div>
                                @else
                                    <div>
                                        @for($i = 0; $i < 5; $i++)
                                            <i class="mdi mdi-star" style="color: grey"></i>
                                        @endfor
                                        <span> ({{ $competitor['rating'] }}) </span>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>


                    @endforeach

                    <div id="location-name-container" class="col-12">
                        <div class="form-group clearfix row m-t-10">
                            <label class="col-3 col-form-label" for="location_name">Select competitor</label>
                            <select class="form-control select2 change-location col-9" style="width: 80%"
                                    data-term="{{ $location->placeType->name }}"></select>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end row -->
@endsection

@push('scripts')
    <script src="{{ asset('js/change_competitor.js') }}"></script>
    <!--<script src="{{ asset('js/change_location.js') }}"></script> -->
@endpush