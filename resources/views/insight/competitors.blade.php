<div class="row">
    <div class="col-md-4">
        <div class="card-box widget-bg-color-icon">
            <h4>
                Your competitors
                <a href="{{ route('competitors', ['location' => $location]) }}" class="pull-right btn btn-sm btn-secondary waves-effect">
                    Change
                </a>
            </h4>
            <div class="competitors">
                @foreach($competitors as $competitor)
                    <div class="competitor @isset($competitor['my_location']) my-location @endif">
                        <div class="competitors-icon pull-left">
                            <img src="{{ $competitor['icon'] }}" >
                        </div>
                        <div class="competitor-name">{{ $competitor['name'] }}</div>
                        <div>
                            @isset($competitor['rating'])
                                <div>
                                    @for($i = 0; $i < round($competitor['rating'], 0); $i++)
                                        <i class="mdi mdi-star" style="color: gold"></i>
                                    @endfor

                                    @for($i = 0; $i < 5 - round($competitor['rating'], 0); $i++)
                                        <i class="mdi mdi-star" style="color: grey"></i>
                                    @endfor
                                    <span> ({{ $competitor['rating'] }}) </span>
                                </div>
                            @else
                                <div>
                                    @for($i = 0; $i < 5; $i++)
                                        <i class="mdi mdi-star" style="color: grey"></i>
                                    @endfor
                                    <span> ({{ $competitor['rating'] }}) </span>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="clearfix"></div>
                @endforeach
            </div>
        </div>
    </div>
</div>
