@extends('layouts.dashboard')

@section('title')
    Insights
@endsection

@section('breadcrums')
    <div class="pull-right">
        <button type="button" class="btn btn-secondary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" style="padding: 4px 11px;">
            {{ $location->text ?? 'All Locations' }} <span class="caret"></span>
        </button>
        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item" href="/dashboard/location/0">{{ $location->text }}</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Shout Outs

                </h4>
            </div>
        </div>

        <div class="col-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Opportunities

                </h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    @include('insight.competitors')

@endsection