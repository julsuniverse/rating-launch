@extends('layouts.dashboard')

@section('title')
    Dashboard
@endsection

@section('breadcrums')
    <div class="pull-right">
        <button type="button" class="btn btn-secondary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" style="padding: 4px 11px;">
            {{ $location->text ?? 'All Locations' }} <span class="caret"></span>
        </button>
        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item" href="/dashboard/location/0">All Locations</a>
            @foreach($company->locations as $l)
                <a class="dropdown-item" href="/dashboard/location/{{ $l->id }}">{{ $l->text }}</a>
            @endforeach
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-box widget-bg-color-icon">
                        <div class="bg-icon bg-icon-primary" style="margin-left: auto; margin-right: auto;">
                            @if (empty($location))
                                <img style="height: 68px; border-radius: 35px; margin: auto;" src="{{ $company->media_url }}">
                            @else
                                <img style="height: 68px; border-radius: 35px; margin: auto;" src="{{ $location->media_url }}">
                            @endif
                        </div>
                        <h2 style="text-align: center; margin-top: 5px;"> {{ $location->text ?? $company->text }} </h2>
                        <p style="text-align: center;">{{ $location->address ?? $company->address }}</p>
                        <p style="text-align: center; font-size: 30px; margin-bottom: -7px;">
                            <i class="mdi mdi-star" style="color: {{ count($reviews) == 0 || $rating >= 0.2 ? 'gold' : 'grey' }};"></i>
                            <i class="mdi mdi-star" style="color: {{ count($reviews) == 0 || $rating >= 0.4 ? 'gold' : 'grey' }};"></i>
                            <i class="mdi mdi-star" style="color: {{ count($reviews) == 0 || $rating >= 0.6 ? 'gold' : 'grey' }};"></i>
                            <i class="mdi mdi-star" style="color: {{ count($reviews) == 0 || $rating >= 0.8 ? 'gold' : 'grey' }};"></i>
                            <i class="mdi mdi-star" style="color: {{ count($reviews) == 0 || $rating >= 1.0 ? 'gold' : 'grey' }};"></i>
                        </p>

                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card-box" style="height: 244px; overflow: hidden;">
                        @if(count($reviews) > 0)
                            Number of Reviews
                            <div id="line-graph" class="ct-chart ct-golden-section" style="max-height: 200px;"></div>
                        @else
                            <div class="alert alert-danger">
                                <p>Be sure to get new reviews and keep this chart growing, up and to the right!</p>
                                <p>Click here to ask your customers for reviews.</p>
                                @if (empty($location))
                                    @include('includes.kiosk-button', ['locations'=> $company->locations])
                                @else
                                    @include('includes.kiosk-button', ['locations'=> collect([$location])])
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>Reputation Monitor</b></h4>
                @if(count($reviews) > 0)
                    <div class="inbox-widget nicescroll" tabindex="5000" style="overflow: scroll; height: 700px;">
                        @include('review.reviews', ['reviews'=> $reviews])
                    </div>
                @else
                    <div class="alert alert-danger text-center">
                        <h4>No reviews yet?</h4>
                        <p>Connect your accounts to start populating reviews.</p>

                        @if($active_location_id)
                            <a href="/locations/{{ $active_location_id }}/sources/create" class="btn btn-secondary waves-effect text-left text-light">Connect Your Accounts</a>
                        @else
                            <a href="/locations/{{ $company->locations->first()->hashed_id }}/sources/create" class="btn btn-secondary waves-effect text-left text-light">Connect Your Accounts</a>
                        @endif
                    </div>

                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Perception Map</h4>

                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
        <!-- <div class="col-md-3">
            <div class="card-box">
                <div id="bubble-graph"></div>
            </div>

            <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Perception Map</h4>

                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div> -->

@endsection

@push('scripts')
    <script src="/plugins/chartist/dist/chartist.min.js"></script>
    <script src="/plugins/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.js"></script>

    <script>
        $(function(){
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'radar',
                data: {
                    labels: {!! json_encode($categories->map(function($c){ return $c->text; })) !!},
                    datasets: [{
                        label: "Your Score",
                        data: [
                            @foreach ($categories as $category)
                                {{ $reviews->sum(strtolower($category->text)) / max($reviews->count(), 1) }},
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)'
                        ],
                        borderWidth: 1
                    },
                    // {
                    //     label: "Competition Score",
                    //     data: [
                    //         @foreach ($categories as $category)
                    //             0.25,
                    //         @endforeach
                    //     ],
                    //     backgroundColor: [
                    //         'rgba(54, 162, 235, 0.2)'
                    //     ],
                    //     borderWidth: 1
                    // }
                    ]
                },
                options: {

                }
            });

            @if ($sourceIds->count() > 0)
                new Chartist.Line('#line-graph', {
                    labels: [
                        @foreach ($perSourcePerDay[$sourceIds[0]] as $d => $c)
                            @if(strtotime($d) <= 86400)
                                @for($i = 4; $i > 0; $i--)
                                    '{{ date('m/d/y', strtotime($d) - $i * 86400) }}',
                                @endfor
                            @endif
                            '{{ $d }}',
                        @endforeach
                    ],
                    series: [
                        @foreach ($perSourcePerDay as $s => $dCounts)
                            [
                                @foreach ($dCounts as $d => $counts)
                                    @if(strtotime($d) <= 86400)
                                        @for($i = 0; $i < 5; $i++)
                                            {{ $counts->sum('count') ?? 0 }},
                                        @endfor
                                    @else
                                        {meta: '{{( $counts[0]->source->text ?? "" )}}', value: '{{ $counts->sum('count') ?? 0 }}'},
                                    @endif
                                @endforeach
                            ],
                        @endforeach
                    ],
                },
                {
                    // high: 3,
                    // low: -3,
                    showArea: true,
                        showLine: false,
                    showPoint: true,
                    fullWidth: true,
                    chartPadding: {
                        right: 50
                    },
                    axisX: {
                    showLabel: true,
                        showGrid: false
                },
                    plugins: [
                        Chartist.plugins.tooltip({

                        })
                    ]
                });
            @endif
        });


        (function() {

          // Fake JSON data
          var json = {"countries_msg_vol": {
            "CA": 170, "US": 393, "BB": 120
          }};

            // D3 Bubble Chart

            var diameter = 300;

            var svg = d3.select('#bubble-graph').append('svg')
                            .attr('width', diameter)
                            .attr('height', diameter);

            var bubble = d3.layout.pack()
                        .size([diameter, diameter])
                        .value(function(d) {return d.size;})
                 // .sort(function(a, b) {
                        //  return -(a.value - b.value)
                        // })
                        .padding(4);

          // generate data with calculated layout values
          var nodes = bubble.nodes(processData(json))
                                .filter(function(d) { return !d.children; }); // filter out the outer bubble

          var vis = svg.selectAll('circle')
                            .data(nodes);

          vis.enter().append('circle')
                    .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
                    .attr('r', function(d) { return d.r; })
                    .attr('class', function(d) { return d.className; });

          function processData(data) {
            var obj = data.countries_msg_vol;

            var newDataSet = [];

            for(var prop in obj) {
              newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
            }
            return {children: newDataSet};
          }

        })();
    </script>
@endpush