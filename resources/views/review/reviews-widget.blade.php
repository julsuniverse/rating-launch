@extends('layouts.dashboard')

@section('title')
    Reviews Widget
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <div class="card-box">
                    <h4>Copy this code</h4>
                    <textarea style="width: 100%" rows="5"><iframe src="{{ route('getLocationReviews', ['location' => $location, 'user' =>  Auth::user()->hashed_id]) }}" width="100%" height="300px" frameborder="0"></iframe></textarea>
                </div>
            </div>
            <div class="col-6">
                <div class="card-box">
                    <h4>Sample</h4>
                    <iframe src="{{ route('getLocationReviews', ['location' => $location, 'user' => Auth::user()->hashed_id]) }}" width="100%" height="300px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>

@endsection

@push('scripts')

@endpush