@foreach ($reviews as $review)
    <tr class="{{ !$review->read ? 'unread' : '' }}">
        <td class="mail-select">
            <a class="review-starred {{ $review->starred ? 'text-warning' : 'text-muted' }}" href="#" data-id="{{ $review->id }}">
                <i class="fa fa-star m-r-15"></i>
            </a>
            <span data-toggle="tooltip" data-placement="top" title="High Team Perception">
                <i class="fa fa-circle m-l-5 text-primary"></i>
            </span>
        </td>
        <td>
            <a href="/reviews/{{ $review->id }}" class="email-name">
                {!! $review->icon_html !!}
                {{ $review->user_fullname }}
            </a>
        </td>
        <td class="hidden-xs">
            <a href="/reviews/{{ $review->id }}" class="email-msg">
                {!! $review->short_text !!}
            </a>
        </td>
        <td style="width: 120px;">
            <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.2 ? 'gold' : 'grey'}};"></i>
            <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.4 ? 'gold' : 'grey'}};"></i>
            <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.6 ? 'gold' : 'grey'}};"></i>
            <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.8 ? 'gold' : 'grey'}};"></i>
            <i class="mdi mdi-star" style="color: {{ $review->rating >= 1.0 ? 'gold' : 'grey'}};"></i>
        </td>
        <td class="text-right" style="width: 150px;">
            {{ $review->formatted_time }}
        </td>
    </tr>
@endforeach

<script type="text/javascript">
    $(function(){
        $(".review-starred").click(function(){
            let obj = $(this);
            let reviewId = obj.attr('data-id');
            $.ajax({
                url: '/reviews/'+reviewId+'/star',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}'
                },
                success: function(data) {
                    if (data.starred){
                        obj.addClass('text-warning');
                        obj.removeClass('text-muted');
                    }else{
                        obj.removeClass('text-warning');
                        obj.addClass('text-muted');
                    }
                },
            });
        });

    });

</script>