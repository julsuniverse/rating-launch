@foreach ($reviews as $review)
    @if(!isset($iframe))
    <a href="/reviews/{{ $review->id }}">
    @endif
        <div class="inbox-item m-t-10">
            <div class="inbox-item-img"><img src="{{ $review->user_image_url }}" class="rounded-circle" alt=""></div>
            <p class="inbox-item-author">
            	{{ $review->user_fullname }} &nbsp;&nbsp;
                {!! $review->icon_html !!}
                <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.2 ? 'gold' : 'grey'}};"></i>
                <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.4 ? 'gold' : 'grey'}};"></i>
                <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.6 ? 'gold' : 'grey'}};"></i>
                <i class="mdi mdi-star" style="color: {{ $review->rating >= 0.8 ? 'gold' : 'grey'}};"></i>
                <i class="mdi mdi-star" style="color: {{ $review->rating >= 1.0 ? 'gold' : 'grey'}};"></i>
            </p>
            <p class="inbox-item-text">{!! $review->text !!}</p>
            <p class="inbox-item-date">
                {{ $review->formatted_time }}
            </p>
        </div>
    @if(!isset($iframe))
    </a>
    @endif
@endforeach