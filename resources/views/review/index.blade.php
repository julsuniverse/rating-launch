@extends('layouts.dashboard')

@section('title')
    Reviews
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="mdi mdi-pencil-box text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark m-t-10"><b class="counter">{{ $reviews->count() }}</b></h3>
                    <p class="text-muted mb-0">Life Time Reviews</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="mdi mdi-star text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark m-t-10"><b class="counter">{{ number_format($reviews->sum('rating') / max($reviews->count(), 1) * 5, 1) }} / 5</b></h3>
                    <p class="text-muted mb-0">Average Rating</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon">
                    [ Graph ]
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <!-- Left sidebar -->
        <div class="col-xl-2 col-lg-3">

            <div class="p-20">

                <div class="list-group mail-list  m-t-20">
                    <a href="#" class="list-group-item b-0 primary-filter active" data-key='inbox'>
                        <i class="fa fa-download m-r-10"></i>All Reviews 
                        @if ($unread > 0)
                            <b class="ml-1">({{ $unread }})</b>
                        @endif
                    </a>
                    <a href="#" class="list-group-item b-0 primary-filter" data-key='starred'>
                        <i class="fa fa-star-o m-r-10"></i>Starred
                    </a>
                    <a href="#" class="list-group-item b-0 primary-filter" data-key='needs_response'>
                        <i class="fa fa-file-text-o m-r-10"></i>Needs Response
                        @if ($needsResponse > 0)
                            <b class="ml-1">({{ $needsResponse }})</b>
                        @endif
                    </a>
                    <a href="#" class="list-group-item b-0 primary-filter" data-key='archive'>
                        <i class="fa  fa-archive m-r-10"></i>Archived
                    </a>
                </div>

                <h4 class="font-18 m-t-40">Perception</h4>

                <div class="list-group b-0 mail-list">
                    @foreach ($categories as $category)
                        <a href="#" class="list-group-item b-0 secondary-filter" data-key="{{ $category->id }}"><span class="fa fa-circle text-info m-r-10"></span>{{ $category->text }}</a>
                    @endforeach
                </div>

            </div>

        </div>
        <!-- End Left sidebar -->

        <!-- Right Sidebar -->
        <div class="col-xl-10 col-lg-7">
            <div class="row">
                <div class="col-lg-12">
                    <div class="btn-toolbar m-t-20" role="toolbar">
                        <div class="btn-group ml-1">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false" id='platform-bttn'>
                                Platform
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                @foreach ($sources as $source)
                                    <a class="dropdown-item top-filter" data-key="platform" data-val="{{ $source->id }}" href="#">{{ ucfirst($source->text) }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="btn-group ml-1">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false" id='stars-bttn'>
                                Stars
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                @for($i = 1; $i <= 5; $i++)
                                    <a class="dropdown-item top-filter" href="#" data-key="stars" data-val="{{ $i }}">
                                        <i class="mdi mdi-star" style="color: {{ $i >= 1 ? 'gold' : 'grey'}};"></i>
                                        <i class="mdi mdi-star" style="color: {{ $i >= 2 ? 'gold' : 'grey'}};"></i>
                                        <i class="mdi mdi-star" style="color: {{ $i >= 3 ? 'gold' : 'grey'}};"></i>
                                        <i class="mdi mdi-star" style="color: {{ $i >= 4 ? 'gold' : 'grey'}};"></i>
                                        <i class="mdi mdi-star" style="color: {{ $i >= 5 ? 'gold' : 'grey'}};"></i>
                                    </a>
                                @endfor
                            </div>
                        </div>
                        <div class="btn-group ml-1">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false" id='state-bttn'>
                                State
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item top-filter" href="#" data-key="state" data-val="read">Read</a>
                                <a class="dropdown-item top-filter" href="#" data-key="state" data-val="unread">Unread</a>
                            </div>
                        </div>
                        
                        <div class="btn-group ml-3">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false" id='location-bttn'>
                                Location
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                @foreach($locations as $location)
                                    <a class="dropdown-item top-filter" href="#" data-key="location" data-val="{{ $location->id }}">{{ $location->text }}</a>
                                @endforeach
                            </div>
                        </div>

                        <div class="btn-group ml-1" style="position: absolute; right: 15px;">
                            <button type="button" class="btn btn-secondary waves-effect waves-light" id='clear-bttn'>
                                Clear
                            </button>
                        </div>
                    </div>
                </div>
            </div> <!-- End row -->

            <div class="card-box p-1 m-t-20">
                <div class="panel-body p-0">
                    <div class="table-responsive">
                        <table class="table table-hover mails m-0">
                            <tbody id=reviews-container>
                              

                            </tbody>
                        </table>
                    </div>

                </div> <!-- panel body -->
            </div> <!-- panel -->

            <div class="row m-b-20">
                <div class="col-7">
                    Showing <span id='showing-start'></span> - <span id='showing-end'></span> of <span id='showing-total'></span>
                </div>
                <div class="col-5">
                    <div class="btn-group pull-right">
                        <button type="button" id="page-minus" class="btn btn-sm btn-secondary waves-effect"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" id="page-plus" class="btn btn-sm btn-secondary waves-effect"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>

        </div> <!-- end Col-9 -->

    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        var filter = {};
        var hasMoreReviews = false;

        $(function(){
            filter = defaultFilterOptions();

            $(".filter-check-box").change(function(){
                let id = $(this).attr('id');
                $(".filter-check-box").each(function(i){
                    if ($(this).attr('id') != id){
                        $(".filter-check-box")[i].checked = false;
                    }
                });

                pullReviews();
                pullReviews();
            });

            $(".perception-check-box").change(function(){
                pullReviews();
                pullReviews();
            });

            $("#location-select").change(function(){
                pullReviews();
                pullReviews();
            });

            $(".primary-filter").click(function(){
                filter.primary = $(this).attr('data-key');
                updateFilterUI();
                pullReviews();
            });

            $(".secondary-filter").click(function(){
                var val = $(this).attr('data-key');
                if (val == filter.secondary){
                    val = '';
                }
                filter.secondary = val;
                updateFilterUI();
                pullReviews();
            });

            $(".top-filter").click(function(){
                var key = $(this).attr('data-key');
                var val = $(this).attr('data-val');
                if (val == filter[key]){
                    val = '';
                    $("#" + key + "-bttn").html(capitalizeFirstLetter(key)+" ");
                }else{
                    $("#" + key + "-bttn").html($(this).html());
                }

                filter[key] = val;
                pullReviews();
            });

            $("#page-minus").click(function(){
                if (filter.page > 1){
                    filter.page -= 1;
                    pullReviews();
                }
            });

            $("#page-plus").click(function(){
                if (hasMoreReviews){
                    filter.page += 1;
                    pullReviews();
                }
            });

            $("#clear-bttn").click(function (){
                filter = defaultFilterOptions();
                updateFilterUI();
                pullReviews();
                $("#platform-bttn").html("Platform");
                $("#stars-bttn").html("Stars");
                $("#state-bttn").html("State");
                $("#location-bttn").html("Location");
            });

            pullReviews();
        });

        function pullReviews(){
            $.ajax({
                url: '/reviews/reviews',
                type: 'GET',
                data: filter,
                success: function(data) {
                    $("#reviews-container").html(data.html);
                    $("#showing-total").html(data.total);

                    let start = (filter.page-1) * data.results_per + 1;
                    $("#showing-start").html(start);
                    $("#showing-end").html(start + data.count - 1);
                    hasMoreReviews = data.count == data.results_per;

                    $('[data-toggle="tooltip"]').tooltip();
                },
            });
        }

        function updateFilterUI(){
            console.log(filter);

            $(".primary-filter").each(function(){
                let obj = $(this);
                if (obj.attr('data-key') == filter.primary){
                    obj.addClass('active');
                }else{
                    obj.removeClass('active');
                }
            });

            $(".secondary-filter").each(function(){
                let obj = $(this);
                if (obj.attr('data-key') == filter.secondary){
                    obj.addClass('active-tag');
                }else{
                    obj.removeClass('active-tag');
                }
            });
        }

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function defaultFilterOptions(){
            return {
                primary: 'inbox',
                secondary: '',
                location: '*',
                platform: '',
                stars: '',
                state: '',
                page: 1
            };
        }
    </script>

@endpush