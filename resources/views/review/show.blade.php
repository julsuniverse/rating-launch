@extends('layouts.dashboard')

@section('title')
    Review: {{ $review->user_fullname }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
        	<p> {{ $review->text }} </p>
            <br>
            <p>Location: <a href="/locations/{{ $review->location_id }}">{{ $review->location->text }}</a></p>
        </div>

        <div class="col-md-3">
        	<!-- <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Perception Map</h4>

                <canvas id="myChart" width="400" height="400"></canvas>
            </div> -->
        </div>
    </div>
    <!-- end row -->

@endsection

@push('scripts')
	<script>
        $(function(){
            // var ctx = document.getElementById("myChart").getContext('2d');
            // var myChart = new Chart(ctx, {
            //     type: 'radar',
            //     data: {
            //         labels: {!! json_encode($categories->map(function($c){ return $c->text; })) !!},
            //         datasets: [{
            //             label: "Your Score",
            //             data: [
            //                 @foreach ($categories as $category)
            //                     {{ $review->{strtolower($category->text)} }},
            //                 @endforeach
            //             ],
            //             backgroundColor: [
            //                 'rgba(255, 99, 132, 0.2)'
            //             ],
            //             borderWidth: 1
            //         }]
            //     },
            //     options: {
                    
            //     }
            // });
        });
    </script>
@endpush