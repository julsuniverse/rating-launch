<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Rating Launch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="/images/favicon.ico">

    @yield('head-meta')

    <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body style="padding-bottom: 0">
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Reputation Monitor</b></h4>
                    @if(count($reviews) > 0)
                        <div class="inbox-widget" tabindex="5000">
                            @include('review.reviews', [
                                'reviews'=> $reviews,
                                'iframe' => true
                            ])
                        </div>
                    @else
                        <div class="alert alert-danger text-center">
                            <h4>No reviews yet?</h4>
                            <p>Connect your accounts to start populating reviews.</p>
                            <a href="/locations" class="btn btn-secondary waves-effect text-left text-light">Connect Your Accounts</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>

