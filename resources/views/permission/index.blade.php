@extends('layouts.dashboard')

@section('title')
    Edit User Permissions
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Edit permissions for: {{ $user->name }}
                    <div class="pull-right">
                        <a href="{{ route('permissions-update', $user) }}" class="pull-right btn btn-success btn-sm" id="btn-archive">Save</a>
                    </div>
                </h4>
                <br>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection