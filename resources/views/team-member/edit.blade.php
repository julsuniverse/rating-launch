@extends('layouts.dashboard')

@section('title')
    Manage Permissions for {{ $user->name }}
@endsection

@section('breadcrums')
    <div class="btn-group pull-right">
        <ol class="breadcrumb hide-phone p-0 m-0">
            <li class="breadcrumb-item"><a href="/"><i class="fa fa-home"></i></a></li>
            <?php $cumulative_path = "" ?>
            @foreach (explode('/', Request::path()) as $path)
                <?php $cumulative_path .= "/$path" ?>
                @if (!ctype_digit($path))
                    @if (!$loop->last && $path != "")
                        <li class="breadcrumb-item active"><a href="{{ strtolower($cumulative_path) . '/' . $companyUser->company_id }}">{{ strtoupper($path) }}</a></li>
                    @elseif ($path != "" || $loop->index == 0)
                        <li class="breadcrumb-item active">{{ strtoupper($path) }}</li>
                    @endif
                @endif
            @endforeach
        </ol>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0">
                    Manage Permissions for {{ $user->name }}
                </h4>
                <p>
                    All users will have access to the main dashboard and ability to see all reviews.
                <p>
                    Please select the following permissions you would like to add for this user.
                </p>

                <form action="{{ route('team-members.update', ['companyUser' => $companyUser]) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @foreach($permissions as $permission)
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-input"
                                   name="{{ $permission->id }}" @if(in_array($permission->id, $userPermissions)) {{ "checked" }} @endif
                                   data-plugin="switchery" data-color="#00b19d" data-size="small">
                            <label class="form-check-label">{{ $permission->name }}</label>
                        </div>
                        @if($permission->id == 2)
                            <div class="form-group" id="locations_block">
                                <select name="locations[]" class="select2 select2-multiple form-control col-6"
                                        id="locations" multiple="multiple" multiple data-placeholder="Choose locations">
                                @foreach($locations as $location)
                                        <option value="{{ $location->id }}" @if(in_array($location->id, $editableLocations)) {{ "selected" }} @endif>
                                            {{ $location->text }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    @endforeach
                    <button type="submit" class="btn btn-success m-t-10">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" href="{{ asset('plugins/switchery/switchery.min.css') }}">
    <script src="{{ asset('js/permissions.js') }}"></script>
    <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.core.js') }}"></script>
@endpush