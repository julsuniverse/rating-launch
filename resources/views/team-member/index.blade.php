@extends('layouts.dashboard')

@section('title')
    Team Members of {{ $company->text }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">All Team Members
                    <div class="pull-right">
                        <a href="{{ route('team-members.create', ['company' => $company]) }}" class="pull-right btn btn-success btn-sm" id="btn-archive">Add Team Member</a>
                    </div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Permissions</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $users)
                        <tr>
                            <td>{{ $users->id }}</td>
                            <td>{{ $users->name }}</td>
                            <td>{{ $users->email }}</td>
                            <td>{{ $users->companyUser->role_id }}</td>
                            <td>
                                <ul>
                                    @foreach($users->companyUser->permissions as $permission)
                                        <li>
                                            {{ $permission->name }}
                                        </li>
                                    @endforeach
                                </ul>

                            </td>
                            <td>
                                <a href="{{ route('team-members.edit', ['companyUser' => $users->companyUser]) }}" class="btn btn-primary">
                                    <i class="fa fa-cog"></i> Manage permissions
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection