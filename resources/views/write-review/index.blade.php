<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>{{ $location->text }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/favicon.ico">

        <meta property="og:title" content="{{ $location->text }}" />
        <meta property="og:image" content="{{ $location->media_url ?? 'https://app.ratinglaunch.com/images/logo_dark.png'}}" />

        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />

        <link href="/css/bootstrap-stars.css" rel="stylesheet" type="text/css" />
        <link href="/css/css-stars.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome-stars-o.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome-stars.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

        <script src="/js/modernizr.min.js"></script>

    </head>

    <body>
    	<div class="container-fluid write-review">
            @if(count($errors))
                <br>
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger m-b-10">
                        <strong>Oh snap!</strong> {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(session('success'))
                <br>
                <div class="alert alert-success m-b-10">
                    <strong>Yay!</strong> {{ session('success') }}
                </div>
            @endif

            <h1> How Did We Do </h1>
    		<div class="row location-info">
    			<div class="col-xs-3" style="margin-top: 10px;">
    				<div style="height: 40px; width: 40px; background: white; border-radius: 20px; text-align: center;">
	                    <img style="height: 38px; border-radius: 19px; margin: auto;" src="{{ $location->media_url }}">
	                </div>
    			</div>
    			<div class="col-xs-8" style="width: 70%; margin-top: 11px;">
					<p style="line-height: 17px;">
						{{ $location->text }}
						<br/>
						<span style="font-size: 12px;">{{ $location->address }}</span>
					</p>
    			</div>
    		</div>

            @if (!$alreadyReviewed)
        		<div class="row">
        			<form class="form-horizontal" role="form" action="/locations/{{ $location->hashed_id }}/write/{{ $reviewer->id }}" method="post" id="location-create-form" style="width: 100%;">
    	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <select id="rating-stars" class="form-control" name="rating" {{ $alreadyReviewed ? 'disabled' : '' }}>
                          <option value="1" {{ $rating == 1 ? 'selected' : '' }}>1</option>
                          <option value="2" {{ $rating == 2 ? 'selected' : '' }}>2</option>
                          <option value="3" {{ $rating == 3 ? 'selected' : '' }}>3</option>
                          <option value="4" {{ $rating == 4 ? 'selected' : '' }}>4</option>
                          <option value="5" {{ $rating == 5 ? 'selected' : '' }}>5</option>
                        </select>

    			        <textarea placeholder="Please tell us about your experience in a couple of words." class="form-control" name='review' style="margin: 10px; height: 200px; width: calc(100% - 20px);" {{ $alreadyReviewed ? 'disabled' : '' }}>{{ $alreadyReviewed ? $reviewer->review->text : '' }}</textarea>

                        <input type="submit" class="btn btn-success" style="width: calc(100% - 20px); margin: 0px 10px;"/>
    			    </form>
        		</div>
            @else
                <div class="row rating-info">
                    <form class="form-horizontal" style="width: 100%;">
                        <h2>Your Review:</h2> 
                        <select id="rating-stars" class="form-control" name="rating">
                          <option value="1" {{ $rating == 1 ? 'selected' : '' }}>1</option>
                          <option value="2" {{ $rating == 2 ? 'selected' : '' }}>2</option>
                          <option value="3" {{ $rating == 3 ? 'selected' : '' }}>3</option>
                          <option value="4" {{ $rating == 4 ? 'selected' : '' }}>4</option>
                          <option value="5" {{ $rating == 5 ? 'selected' : '' }}>5</option>
                        </select>
                        <p>
                            {{ $reviewer->review->text ?? '' }}
                            {{ $rating }}
                        </p>
                    </form>
                </div>
            @endif

            @if ($alreadyReviewed)
                @if ($location->sources->count() > 0)
                    @if ($reviewer->review->rating >= $location->rating)
                        <p style="text-align: center; margin: 0 15px;">We've copied your <strong>AWESOME</strong> review to your clipboard.</p>
                        <br>
                        <p style="text-align: center; margin: 0 15px;">
                            <strong style="font: italic bold 16px/30px Georgia, serif;">Share it with the WORLD</strong><br>
                            Simply <span style="text-decoration-line: underline;">paste</span> the review into any of the sites below! No need to retype anything, just paste!
                        </p>

                        <br/>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="row justify-content-md-center" style="text-align: center;">
                                    @foreach ($location->sources as $source)
                                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                                            <a href="#" data-url="{{ $source->rate_url }}" class="social-bttn" data-url="{{ $source->url }}">
                                                @if ($source->review_category_id == 1)
                                                    <div class="yelp-share-bttn">
                                                        <button class="btn btn-primary yelp-share-bttn-icon"><i class="mdi mdi-yelp"></i></button>
                                                        <div class="yelp-share-text"> &nbsp; Share on Yelp </div>
                                                    </div>
                                                @elseif ($source->review_category_id == 2)
                                                    <button class="btn btn-primary"><i class="mdi mdi-foursquare"></i></button>
                                                @elseif ($source->review_category_id == 3)
                                                    <div class="fb-share-bttn">
                                                        <button class="btn btn-primary fb-share-bttn-icon"><i class="mdi mdi-facebook"></i></button>
                                                        <div class="fb-share-text"> &nbsp; Share on Facebook </div>
                                                    </div>
                                                @elseif ($source->review_category_id == 6)
                                                    <div class="goog-share-bttn">
                                                        <button class="btn btn-primary goog-share-bttn-icon"><i class="mdi mdi-google"></i></button>
                                                        <div class="goog-share-text"> &nbsp; Share on Google </div>
                                                    </div>
                                                @endif
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    @endif
                @endif
            @endif
    	</div>



        <!-- jQuery  -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/js/mindmup-editabletable.js"></script>
        <script src="/js/jquery.barrating.min.js"></script>

        <script type="text/javascript">
           $(function() {
              $('#rating-stars').barrating({
                theme: 'fontawesome-stars'
              });

              $(".social-bttn").click(function(){
                copyToClipboard("{{ $reviewer->review->text ?? '' }}");
                let url = $(this).attr('data-url');
                // alert('Your review had been copied to your clipboard!');
                var win = window.open(url, '_blank');
              });
           });

           function copyToClipboard(text) {
                  // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var origSelectionStart, origSelectionEnd;

                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = text;

                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);
                
                // copy the selection
                var succeed;
                try {
                      succeed = document.execCommand("copy");
                } catch(e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }
                

                target.textContent = "";

                return succeed;
            }
        </script>
    </body>
</html>