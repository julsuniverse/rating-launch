@extends('layouts.dashboard')

@section('title')
    Potential Leads
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Potential leads overview
                    <div class="pull-right">
                        <a href="{{ route('leads.create') }}" class="btn btn-success btn-sm m-r-5" id="btn-archive">
                            <span class="fa fa-user-plus"></span>
                            Create
                        </a>
                        <a href="{{ route('leads.import') }}" class="btn btn-success btn-sm" id="btn-archive">
                            <span class="fa fa-upload"></span>
                            Import
                        </a>
                    </div>
                </h4>
                <br>

                <table class="table mb-0 table-responsive">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Company name</th>
                        <th>email</th>
                        <th>Address 1</th>
                        <th>Address 2</th>
                        <th>Place Type</th>
                        <th></th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($counter = 1)
                        @foreach($leads as $lead)
                            <tr>
                                <td>{{ $counter }}</td>
                                <td>{{ $lead->salutation }} {{ $lead->name }}</td>
                                <td>{{ $lead->company_name }}</td>
                                <td>{{ $lead->email }}</td>
                                <td>{{ $lead->address_1 }}</td>
                                <td>{{ $lead->address_2 }}</td>
                                <td>{{ $lead->place_type }}</td>
                                <td>
                                    <a href="{{ route('leads.show', ['lead' => $lead]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('leads.edit', ['lead' => $lead]) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                                <td>
                                    @if($lead->checked)
                                        <a href="{{ route('leads.check', ['lead' => $lead]) }}">
                                            <span class="badge badge-success">
                                            <i class="fa fa-check"></i>
                                            Confirmed
                                        </span>
                                        </a>
                                    @else
                                        <a href="{{ route('leads.check', ['lead' => $lead]) }}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-warning"></i> Confirm location
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @php($counter++)
                        @endforeach
                    </tbody>
                </table>

                    {{ $leads->links() }}

            </div>
        </div>
    </div>
    <!-- end row -->
@endsection