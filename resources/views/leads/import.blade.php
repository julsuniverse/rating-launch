@extends('layouts.dashboard')

@section('title')
    Potential Leads
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Import Leads</h4>
                <br>

                <form action="{{ route('leads.upload') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <span class="fa fa-upload"></span>
                            CSV Upload
                        </label>
                        <div class="col-10">
                            <input type="file" class="form-control" name="file">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mb-2">Import</button>
                </form>

            </div>
        </div>
    </div>
    <!-- end row -->
@endsection