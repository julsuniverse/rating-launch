@extends('layouts.dashboard')

@section('title')
    Information check
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Information check</h4>
                <br>

                <div class="row">
                    <div class="col-6">
                        <h2>Imported data</h2>
                        <p class="{{ $lead->salutation ? '' : 'text-danger' }}">
                            <b>Salutation:</b> {{ $lead->salutation ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->name ? '' : 'text-danger' }}">
                            <b>Name:</b> {{ $lead->name ?? 'Missing'  }}
                        </p>
                        <p class="{{ $lead->company_name ? '' : 'text-danger' }}">
                            <b>Company name:</b> {{ $lead->company_name ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->email ? '' : 'text-danger' }}">
                            <b>Email:</b> {{ $lead->email }}
                        </p>
                        <p class="{{ $lead->address_1 ? '' : 'text-danger' }}">
                            <b>Address 1:</b> {{ $lead->address_1 ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->address_2 ? '' : 'text-danger' }}">
                            <b>Address 2:</b> {{ $lead->address_2 ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->place_type ? '' : 'text-danger' }}">
                            <b>Place Type:</b> {{ $lead->place_type ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->city ? '' : 'text-danger' }}">
                            <b>City:</b> {{ $lead->city ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->state ? '' : 'text-danger' }}">
                            <b>State:</b> {{ $lead->state ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->country ? '' : 'text-danger' }}">
                            <b>Country:</b> {{ $lead->country ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->zip ? '' : 'text-danger' }}">
                            <b>Zip:</b> {{ $lead->zip ?? 'Missing' }}
                        </p>
                        <p class="{{ $lead->phone ? '' : 'text-danger' }}">
                            <b>Phone:</b> {{ $lead->phone ?? 'Missing' }}
                        </p>

                        <a href="{{ route('leads.edit', ['lead' => $lead, 'check' => 1]) }}" class="btn btn-success btn-sm">
                            <span class="fa fa-pencil"></span>
                            Edit
                        </a>
                    </div>
                    <div class="col-6 text-center">
                        <h2>Matched location</h2>
                        @isset($place)
                            @if($place === 0)
                                <div class="alert alert-danger">
                                    <strong>
                                        Location not found!
                                    </strong>
                                    Try to add more company information or search manually.
                                </div>
                            @else
                                <div class="card-box widget-bg-color-icon">
                                    <div class="place-icon-div">
                                        @isset($place['icon'])
                                            <img id="place_icon" src="{{ $place['icon'] }}" alt="{{ $place['name'] }}" >
                                        @else
                                            <i class="mdi mdi-home-modern" style="margin-top: 14px; font-size: 53px; color: #999"></i>
                                        @endif
                                    </div>

                                    <h2 id="place_name" style="text-align: center; margin-top: 5px;"> {{ $place['name'] }} </h2>
                                    <p id="place_address" class="text-center"> {{ $place['formatted_address'] }} </p>

                                    @isset($place['rating'])
                                        <p id="place_rating" class="text-center" style="font-size: 30px;">
                                            @for($i = 0; $i < round($place['rating'], 0); $i++)
                                                <i class="mdi mdi-star" style="color: gold"></i>
                                            @endfor

                                            @for($i = 0; $i < 5 - round($place['rating'], 0); $i++)
                                                <i class="mdi mdi-star" style="color: grey"></i>
                                            @endfor
                                        </p>
                                    @else
                                        <p id="place_rating" class="text-center" style="font-size: 30px;">
                                            @for($i = 0; $i < 5; $i++)
                                                <i class="mdi mdi-star" style="color: grey"></i>
                                            @endfor
                                        </p>
                                    @endif

                                    <div class="text-center">
                                        Found on <i class="fa fa-google"></i>
                                    </div>
                                </div>

                                <a href="javascript:void(0)" id="change_location" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Find another location</a>

                                <div id="location-name-container" class="col-12">
                                    <div class="form-group clearfix row m-t-10">
                                        <label class="col-3 col-form-label" for="location_name">Location Name</label>
                                        <div class="hidden col-9" id="location_text">
                                            <input class="form-control required" name="location_name" type="text" value="{{ old('text') }}">
                                        </div>
                                        <select class="form-control select2 change-location col-9 " name='yelp_id'></select>
                                    </div>
                                </div>

                                <form role="form" action="{{ route('leads.confirm') }}" method="post" id="change_location_form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name='lead_id' value="{{ $lead->id }}">
                                    <input type="hidden" name='name' value="{{ isset($place['name']) ? $place['name'] : null }}">
                                    <input type="hidden" name='full_address' value="{{ isset($place['formatted_address']) ? $place['formatted_address'] : null }}">
                                    <input type="hidden" name="icon" value="{{ isset($place['icon']) ? $place['icon'] : null}}">
                                    <input type="hidden" name="google_rating" value="{{ isset($place['rating']) ? $place['rating'] : null }}">
                                    <input type="hidden" name="google_place_id" value="{{ isset($place['place_id']) ? $place['place_id'] : null }}">
                                    <input type="hidden" name="lead" value="{{ $lead }}">
                                </form>
                            @endif
                        @else
                            <div class="alert alert-danger">
                                <strong>
                                    Too few information to find a location.
                                </strong>
                                Please, add more company information.
                            </div>
                        @endif
                    </div>
                    @isset($place)
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary" id="confirm_button">Confirm!</button>
                        </div>
                    @else
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary" id="confirm_button" disabled style="cursor: not-allowed;">Confirm!</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection

@push('scripts')
    <script src="{{ asset('js/change_location.js') }}"></script>
@endpush