@extends('layouts.dashboard')

@section('title')
    Potential Leads Edit
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Potential lead <b>{{ $lead->salutation }} {{ $lead->first_name }} {{ $lead->last_name }}</b>
                    <div class="pull-right">
                        <a href="{{ route('leads.edit', ['lead' => $lead]) }}" class="btn btn-success btn-sm m-r-5" id="btn-archive">
                            <span class="fa fa-pencil"></span>
                            Edit
                        </a>
                    </div>
                </h4>
                <br>

                <p><b>Salutation:</b> {{ $lead->salutation }}</p>
                <p><b>First name:</b> {{ $lead->first_name }}</p>
                <p><b>Last name:</b> {{ $lead->last_name }}</p>
                <p><b>Email:</b> {{ $lead->email }}</p>
                <p><b>Address 1:</b> {{ $lead->address_1 }}</p>
                <p><b>Address 2:</b> {{ $lead->address_2 }}</p>
                <p><b>Place Type:</b> {{ $lead->place_type }}</p>
                <p><b>City:</b> {{ $lead->city }}</p>
                <p><b>State:</b> {{ $lead->state }}</p>
                <p><b>Zip:</b> {{ $lead->zip }}</p>
                <p><b>Phone:</b> {{ $lead->phone }}</p>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection