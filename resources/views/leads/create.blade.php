@extends('layouts.dashboard')

@section('title')
    Potential Leads
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Create Leads</h4>
                <br>

                <form action="{{ route('leads.store') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Salutation</label>
                        <div class="col-10">
                            <input name="salutation" type="text" class="form-control" value=" {{ old('salutation') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Full Name</label>
                        <div class="col-10">
                            <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Company Name</label>
                        <div class="col-10">
                            <input name="company_name" type="text" class="form-control" value="{{ old('company_name') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Email</label>
                        <div class="col-10">
                            <input name="email" type="email" class="form-control" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Address 1</label>
                        <div class="col-10">
                            <input name="address_1" type="text" class="form-control" value="{{ old('address_1') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Address 2</label>
                        <div class="col-10">
                            <input name="address_2" type="text" class="form-control" value="{{ old('address_2') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">City</label>
                        <div class="col-10">
                            <input name="city" type="text" class="form-control" value="{{ old('city') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">State</label>
                        <div class="col-10">
                            <input name="state" type="text" class="form-control" value="{{ old('state') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Country</label>
                        <div class="col-10">
                            <input name="country" type="text" class="form-control" value="{{ old('country') ?? 'US' }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Zip</label>
                        <div class="col-10">
                            <input name="zip" type="text" class="form-control" value="{{ old('zip') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Phone</label>
                        <div class="col-10">
                            <input name="phone" type="tel" class="form-control" value="{{ old('phone') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Type of place</label>
                        <div class="col-10">
                            <input name="place_type" type="text" class="form-control" value="{{ old('place_type') }}">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mb-2">Save</button>
                </form>

            </div>
        </div>
    </div>
    <!-- end row -->
@endsection