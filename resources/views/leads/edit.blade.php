@extends('layouts.dashboard')

@section('title')
    Potential Leads
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0">Edit Lead {{ $lead->salutation }} {{ $lead->first_name }} {{ $lead->last_name }}</h4>
                <br>

                <form action="{{ route('leads.update', ['lead' => $lead, 'check' => $check]) }}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Salutation</label>
                        <div class="col-10">
                            <input name="salutation" type="text" class="form-control" value=" {{ $lead->salutation }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Name</label>
                        <div class="col-10">
                            <input name="name" type="text" class="form-control" value="{{ $lead->name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Company Name</label>
                        <div class="col-10">
                            <input name="company_name" type="text" class="form-control" value="{{ $lead->company_name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Email</label>
                        <div class="col-10">
                            <input name="email" type="email" class="form-control" value="{{ $lead->email }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Address 1</label>
                        <div class="col-10">
                            <input name="address_1" type="text" class="form-control" value="{{ $lead->address_1}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Address 2</label>
                        <div class="col-10">
                            <input name="address_2" type="text" class="form-control" value="{{ $lead->address_2 }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">City</label>
                        <div class="col-10">
                            <input name="city" type="text" class="form-control" value="{{ $lead->city }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">State</label>
                        <div class="col-10">
                            <input name="state" type="text" class="form-control" value="{{ $lead->state }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Country</label>
                        <div class="col-10">
                            <input name="state" type="text" class="form-control" value="{{ $lead->country }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Zip</label>
                        <div class="col-10">
                            <input name="zip" type="text" class="form-control" value="{{ $lead->zip }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Phone</label>
                        <div class="col-10">
                            <input name="phone" type="tel" class="form-control" value="{{ $lead->phone }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Type of place</label>
                        <div class="col-10">
                            <input name="place_type" type="text" class="form-control" value="{{ $lead->place_type }}">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mb-2">Save</button>
                </form>

            </div>
        </div>
    </div>
    <!-- end row -->
@endsection