@extends('email.layout')

@section('title')
    Is the
    {{ $rating }} {{ $stars }} rating on {{ $platform }} true?
@endsection

@section('content-title')
    <h1>Is the {{ $rating }} {{ $stars }} rating on {{ $platform }} true?</h1>
@endsection

@section('content-text')
    <h3>Dear {{ $lead->salutation }} {{ $lead->name }},</h3>

    <p>I was just looking at dental practices in the {{ $lead->city }} area and saw that your practice
        just received a {{ $rating }} star review on {{ $platform }}.</p>

    <p>Like me, many people use Google to search for new practices when they move to an
    area or are looking for new providers. The value of a new patient can be substantial
    for your practice. A bad review can cost you thousands of dollars in revenue each year.</p>

    <p>With {{ $company }}, you can increase your positive reviews, burying the ones
    that are no good. Plus, by leveraging artificial intelligence, the platform can predict
    how you stack against your local competitors and provide areas in which your patients say you can improve.</p>

    <p>{{ $company }} is exponentially cheaper than the cost of even one missed new patient.</p>

    <p><a href="{{ $link }}">Click here</a> to start your free trial today. We believe in transparency, so there are no secrets and no hooks, you can cancel anytime.</p>
@endsection