@extends('layouts.dashboard')

@section('title')
    Super-Dashboard
@endsection

@section('content')

        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-warning pull-left">
                            <i class="mdi  mdi mdi-star text-success"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter">{{ $reviews }}</b></h3>
                            <p class="text-muted mb-0">Total Reviews</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-primary pull-left">
                            <i class="mdi  mdi mdi-account-multiple text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter"> {{ $subscribe }}</b></h3>
                            <p class="text-muted mb-0">Current Subscribe (Paid)</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 ">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="mdi mdi mdi-timer-sand text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter">{{ $trial }}</b></h3>
                            <p class="text-muted mb-0">People in Trial</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <!-- end row -->

            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-success pull-left">
                            <i class="mdi  mdi mdi-check text-success"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter">{{ round($subscribe / max($all, 1) * 100, 2)}}%</b></h3>
                            <p class="text-muted mb-0">Conversion Rate</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-danger pull-left">
                            <i class="mdi  mdi mdi-alert-circle-outline text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter"> {{ round($canceled / max($all, 1) * 100, 2)}}%</b></h3>
                            <p class="text-muted mb-0">Churn Rate</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-info pull-left">
                            <i class="mdi  mdi mdi-currency-usd text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark m-t-10"><b class="counter">{{ round($trial * 100 / max($trial + $subscribe, 1), 2) }}%</b></h3>
                            <p class="text-muted mb-0">Trial to Paid</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-md-8">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">New subscribers vs. Cancellations</h4>

                        <div id="new-subs" class="ct-chart ct-golden-section" style="max-height: 200px;"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Paying vs. Non Paying</h4>

                        <div id="paying" class="ct-chart ct-golden-section" style="max-height: 200px;"></div>
                    </div>
                </div>
            </div>

        </div> <!-- end container -->

@endsection

@push('scripts')
    <script src="/plugins/chartist/dist/chartist.min.js"></script>
    <script src="/plugins/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.js"></script>

    <script>
        @php
            $today = new \DateTime();
            $today = $today->format('M');
        @endphp
        $(function(){
            var subscribers = 'Subscribers';
            var cancellations = 'Cancellations';
            new Chartist.Line('#new-subs', {
                labels: [
                    '{{ $three_months_ago->format('F') }}',
                    '{{ $two_months_ago->format('F') }}',
                    '{{ $one_month_ago->format('F') }}',
                    '{{ $this_month->format('F') }}'
                ],
                series: [
                    [
                        {meta: subscribers, value: '{{ $three_m_paid }}'},
                        {meta: subscribers, value: '{{ $two_m_paid }}'},
                        {meta: subscribers, value: '{{ $one_m_paid }}'},
                        {meta: subscribers, value: '{{ $this_m_paid }}'}
                    ],
                    [
                        {meta: cancellations, value: '{{ $three_m_canceled }}'},
                        {meta: cancellations, value: '{{ $two_m_canceled }}'},
                        {meta: cancellations, value: '{{ $one_m_canceled }}'},
                        {meta: cancellations, value: '{{ $this_m_canceled }}'}
                    ]
                ]
                },
                {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    },
                    lineSmooth: Chartist.Interpolation.simple({
                        divisor: 6
                    }),
                    axisX: {
                        showLabel: true,
                        showGrid: false
                    },
                    plugins: [
                        Chartist.plugins.tooltip()
                    ]
                });

            // PIE CHART
            var data;
            @if ($trial > 0 && $subscribe > 0)
                data = {
                        labels: ['Paying', 'Non Paying'],
                        series: [{{ $subscribe }}, {{ $trial }}]
                    };
            @elseif ($trial == 0)
                data = {
                    labels: ['Paying'],
                    series: [{{ $subscribe }}]
                };
            @else
                data = {
                labels: ['Non Paying'],
                series: [{{ $trial }}]
            };
            @endif


            var options = {
                labelInterpolationFnc: function(value) {
                    return value[0]
                }
            };

            var responsiveOptions = [
                ['screen and (min-width: 640px)', {
                    chartPadding: 30,
                    labelOffset: 100,
                    labelDirection: 'explode',
                    labelInterpolationFnc: function(value) {
                        return value;
                    }
                }],
                ['screen and (min-width: 1024px)', {
                    labelOffset: 50,
                    chartPadding: 20
                }]
            ];

            new Chartist.Pie('#paying', data, options, responsiveOptions);

        });

    </script>
@endpush