 <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Rating Launch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/images/favicon.ico">


        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/plugins/jquery.steps/css/jquery.steps.css" />

        <script src="/js/jquery.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.11/d3.min.js"></script>
    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <a class="logo">
                            <!-- <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                            <span class="logo-large"><i class="mdi mdi-radar"></i> Rating Launch</span> -->
                            <img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">
                        </a>
                        <!-- Image Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--<img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">-->
                        <!--<img src="/images/logo_sm.png" alt="" height="24" class="logo-sm">-->
                        <!--</a>-->

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">

                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                            

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="{{ Auth::user()->gravatar_url }}" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                    
                                    <a href="/logout" class="dropdown-item notify-item">
                                        <i class="mdi mdi-logout"></i> <span>Logout</span>
                                    </a>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">
                @if(count($errors))
                    <br>
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger m-b-10">
                            <strong>Oh snap!</strong> {{ $error }}
                        </div>
                    @endforeach
                @endif

                @if(session('success'))
                    <br>
                    <div class="alert alert-success m-b-10">
                        <strong>Yay!</strong> {{ session('success') }}
                    </div>
                @endif

                <div class="row justify-content-md-center">
                    <div class="col-md-10">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b>Onboarding</b></h4>
                            <p class="text-muted m-b-30 font-13">
                                Please give us a little information about your self to help us jump start you.
                            </p>

                            <div role="application" class="wizard clearfix" id="steps-uid-0">
                               <div class="steps clearfix">
                                  <ul role="tablist">
                                    <li role="tab" class="{{ $step == 1 ? 'first current' : 'done' }}" aria-disabled="false" aria-selected="false">
                                        <a id="steps-uid-0-t-0" aria-controls="steps-uid-0-p-0">
                                            <span class="number">1.</span> Info
                                        </a>
                                    </li>

                                    <li role="tab" class="{{ $step == 2 ? 'first current' : 'done' }}" aria-disabled="false" aria-selected="true"><a id="steps-uid-0-t-1" aria-controls="steps-uid-0-p-1"><span class="number">2.</span> Connect Your Accounts</a></li>

                                     <li role="tab" class="{{ $step == 3 ? 'first current' : 'done' }}" aria-disabled="false" aria-selected="true"><a id="steps-uid-0-t-1" aria-controls="steps-uid-0-p-1"><span class="number">3.</span> Payment</a></li>

                                     <!-- <li role="tab" class="{{ $step == 4 ? 'first current' : 'done' }}" aria-disabled="false" aria-selected="false"><a id="steps-uid-0-t-3" aria-controls="steps-uid-0-p-3"><span class="number">4.</span> Launch</a></li> -->
                                  </ul>
                               </div>

                               <div class="content clearfix" style="overflow: scroll; height: 800px;">

                                  <h3 id="steps-uid-0-h-0" tabindex="-1" class="title current"> Info</h3>
                                  <section id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="display: {{ $step == 1 ? 'block' : 'none' }};">
                                    <form method="post" action="/onboarding/1">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input class="form-control hidden" name="places_id" type="hidden">

                                        <br>
                                        <h4>Location Info</h4>
                                        <div class='row'>
                                            <div id="company-img-container" style="width: 110px;">
                                                <div class="bg-icon bg-icon-primary onboarding-icon">
                                                    <!-- <i class="mdi mdi-home-modern"></i> -->
                                                    <img src="" class="hidden onboarding-icon-img" id='company-img'>
                                                </div>
                                            </div>
                                            <div id="location-name-container" style="width: calc(100% - 110px);">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="userName">Location Name</label>
                                                    <div class="hidden" id="location_text">
                                                       <input class="form-control required" name="location_name" type="text">
                                                    </div>
                                                    <select class="form-control select2 find-location" name='yelp_id'></select>
                                                </div>
                                            </div>
                                        </div>

                                        <div id='location-detailed-information' class="hidden">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Street Address</label>
                                                        <div class="">
                                                           <input class="form-control required" name="street" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">City</label>
                                                        <div class="">
                                                           <input class="form-control required" name="city" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">State</label>
                                                        <div class="">
                                                           <input class="form-control required" name="state" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Postal Code</label>
                                                        <div class="">
                                                           <input class="form-control required" name="zip" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Country</label>
                                                        <div class="">
                                                           <input class="form-control required" name="country" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <input type="submit" value="Save" class="btn btn-success"/>
                                        <br><br>
                                    </form>
                                  </section>

                                  <h3 id="steps-uid-0-h-3" tabindex="-1" class="title">Connect Your Accounts</h3>
                                  <section id="steps-uid-0-p-3" role="tabpanel" aria-labelledby="steps-uid-0-h-3" class="body" aria-hidden="true" style="display: {{ $step == 2 ? 'block' : 'none' }};">
                                     <div class="form-group clearfix">
                                        <div class="col-lg-12">
                                            @if ($step == 2)
                                                @include('source.add-source', ['location'=> Auth::user()->locations->first(), 'redirect'=> '/onboarding/2'])
                                            @endif
                                        </div>
                                     </div>
                                  </section>

                                  <h3 id="steps-uid-0-h-1" tabindex="-1" class="title">Payment</h3>
                                  <section id="steps-uid-0-p-1" role="tabpanel" aria-labelledby="steps-uid-0-h-1" class="body" aria-hidden="true" style="display: {{ $step == 3 ? 'block' : 'none' }};">
                                     
                                    <form method="post" action="/onboarding/3" id='payment-form'>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="plan_id" value="1">

                                        <h4>Billing Info</h4>
                                        <p>Almost there, the only thing left is to add your credit card to pay.</p>
                                        @if ($step == 3)
                                            <div id="card-errors" role="alert"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Billing Name</label>
                                                        <div class="">
                                                           <input class="form-control required" name="cardholder-name" type="text" value="{{ $user->name }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Street Address</label>
                                                        <div class="">
                                                           <input class="form-control required" name="street" type="text" value="{{ $user->locations->first()->street ?? ''}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">City</label>
                                                        <div class="">
                                                           <input class="form-control required" name="city" type="text" value="{{ $user->locations->first()->city ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">State</label>
                                                        <div class="">
                                                           <input class="form-control required" name="state" type="text" value="{{ $user->locations->first()->state ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Postal Code</label>
                                                        <div class="">
                                                           <input class="form-control required" name="zip" type="text" value="{{ $user->locations->first()->ip ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="userName">Country</label>
                                                        <div class="">
                                                           <input class="form-control required" name="country" type="text" value="{{ $user->locations->first()->country ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <br>
                                        <h4>Payment Info</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="userName">Card Number</label>
                                                    <div id='card-number'>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="userName">Expiration</label>
                                                    <div id='card-expiry'>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="userName">CVC</label>
                                                    <div id='card-cvc'>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <h4>Coupon Code</h4>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-4 col-sm-6">
                                                <div class="form-group clearfix">
                                                    <!-- <label class="control-label " for="userName">Card Number</label> -->
                                                    <div class="">
                                                       <input class="form-control" name="coupon" type="text">
                                                       <i id='coupon-validation' class="fa fa-check input-fa hidden"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <input id='pay-bttn' type="submit" value="Pay $134.00" class="btn btn-success"/>
                                        <input type="hidden" value="" name='stripeToken'/>
                                    </form>

                                  </section>

                                  <h3 id="steps-uid-0-h-3" tabindex="-1" class="title">Launch</h3>
                                  <section id="steps-uid-0-p-3" role="tabpanel" aria-labelledby="steps-uid-0-h-3" class="body" aria-hidden="true" style="display: {{ $step == 4 ? 'block' : 'none' }};">
                                     <div class="form-group clearfix">
                                        <div class="col-lg-12">
                                            <div class="termsOfUse">
                                                The standard Lorem Ipsum passage, used since the 1500s<br/>
                                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."<br/><br/>

                                                Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC<br/>
                                                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"<br/><br/>

                                                1914 translation by H. Rackham<br/>
                                                "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"<br/>

                                                Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC<br/>
                                                "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."<br/><br/>

                                                1914 translation by H. Rackham<br/>
                                                "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."<br/><br/>

                                            </div>
                                            <form method="POST" action="/onboarding/4">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                               <div class="checkbox checkbox-primary">
                                                  <input id="checkbox-h" type="checkbox" name="terms_of_use">
                                                  <label for="checkbox-h">
                                                    I agree with the Terms and Conditions.
                                                  </label>
                                               </div>

                                               @if ($step == 4)
                                                    <button class="btn btn-primary" type="submit"> Agree</button>
                                                @endif
                                            </form>
                                        </div>
                                     </div>
                                  </section>
                               </div>
                               @if ($step == 2)
                                   <div class="actions clearfix">
                                      <ul role="menu" aria-label="Pagination">
                                         <li aria-hidden="false" aria-disabled="false" class="" style=""><a href="/onboarding/{{ $step + 1 }}" role="menuitem">Next</a></li>
                                      </ul>
                                   </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        {{ date('Y') }} © RatingLaunch
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/js/mindmup-editabletable.js"></script>
        <script src="/plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/plugins/jquery-validation/js/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js" type="text/javascript"></script>
        <script src="https://js.stripe.com/v3/"></script>
        <script src="{{ asset('js/find_location.js') }}"></script>

        <script type="text/javascript">
            // FB Source

            $(function(){
                @if ($step == 1)
                    //getLocation();
                @elseif ($step == 2)

                @elseif ($step == 3)
                    var style = {
                      base: {
                        color: '#32325d',
                        lineHeight: '18px',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                          color: '#aab7c4'
                        }
                      },
                      invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                      }
                    };

                    var stripe = Stripe("{{ env('STRIPE_PUB') }}");
                    var elements = stripe.elements();

                    // Create an instance of the card Element.
                    var cardNum = elements.create('cardNumber', {style: style});
                    cardNum.mount('#card-number');

                    var cardExpiry = elements.create('cardExpiry', {style: style});
                    cardExpiry.mount('#card-expiry');

                    var cardCvc = elements.create('cardCvc', {style: style});
                    cardCvc.mount('#card-cvc');

                    var form = document.getElementById('payment-form');
                    form.addEventListener('submit', function(event) {
                      event.preventDefault();

                      stripe.createToken(cardNum).then(function(result) {
                        if (result.error) {
                          var errorElement = document.getElementById('card-errors');
                          errorElement.textContent = result.error.message;
                        } else {
                            console.log(result.token.id);
                            $("[name=stripeToken]").val(result.token.id);
                            $("#payment-form").submit();
                        }
                      });
                    });

                    $("[name=coupon]").on('input', function(){
                        let val = $(this).val();

                        $.ajax({
                            url: '/onboarding/coupon_lookup',
                            type: 'GET',
                            data: {
                                'code': val,
                                'plan': $("[name=plan_id]").val()
                            },
                            success: function(response) {
                                $("#pay-bttn").val("Pay $"+response.price);
                                if (response.found){
                                    $("#coupon-validation").css('color', 'green');
                                    $("#coupon-validation").addClass("fa-check");
                                    $("#coupon-validation").removeClass("fa-times");
                                }else{
                                    $("#coupon-validation").css('color', 'red');
                                    $("#coupon-validation").removeClass("fa-check");
                                    $("#coupon-validation").addClass("fa-times");
                                }
                            },
                        });

                        if (val == ""){
                            $("#coupon-validation").addClass("hidden");
                        }else{
                            $("#coupon-validation").removeClass("hidden");
                        }
                    });
                @endif

            });
        </script>
    </body>
</html>