@extends('layouts.dashboard')

@section('title')
    Add Location Source
@endsection

@section('content')
    <div class="row justify-content-center">
    	<div class="col-lg-9 col-md-12">
	    	<div class="card-box">
				@include('source.add-source', [
                        'location'=> $location,
                        'redirect'=> '/locations/'.$location->id,
                        'source_google' => $source_google,
                        'source_facebook' => $source_facebook,
                        'source_yelp' => $source_yelp,
					])
	    	</div>
		</div>
    </div>

@endsection