<h2 class="text-center">
    Connect Your Accounts
</h2>
<table class="table">
    <thead>
        <tr>
            <th>
                SITE
            </th>
            <th class="text-center">
                API CONNECTION
            </th>
            <th class="text-center">
                <span class="mdi mdi-open-in-new text-center d-block"></span>
                COLLECT
            </th>
            <th class="text-center">
                <span class="mdi mdi-eye text-center d-block"></span>
                VIEW
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-middle">
                <img src="{{ asset('images/sources/google.png') }}" alt="Google" />
            </td>
            <td class="text-center align-middle">
                @isset($source_google)
                    <h3 class="text-success">Connected to Google</h3>
                    <a href="/locations/{{  $location->id }}/sources/{{ $source_google->id }}/delete" class="disconnect">disconnect</a>
                @else
                    @if ($googleLoggedIn)
                        <form class="form-horizontal d-block" role="form" action="/locations/{{ $location->id }}/sources" method="post" id="location-create-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="source_id" value="6">
                            <input type="hidden" name="redirect_uri" value="{{ $redirect }}">

                            <div class="form-group row" id='goog-page-field'>
                                <label for="inputEmail3" class="col-3 col-form-label">Location</label>
                                <div class="col-9">
                                    <select class="form-control" name="goog_page_info" id="goog-page">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-b-0 row">
                                <div class="offset-3">
                                    <button type="submit" class="btn btn-outline-dark pointer">Add</button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="form-group row d-block" id='fb-field'>
                            <a href="{!! $googleUrl !!}">
                                <div class="goog-share-bttn">
                                    <button class="btn btn-primary goog-share-bttn-icon"><i class="mdi mdi-google"></i></button>
                                    <div class="goog-share-text"> &nbsp; Google </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endif
            </td>
            <td class="text-center source-check">
                <i class="mdi mdi-check-circle text-success"></i>
            </td>
            <td class="text-center source-check">
            </td>
        </tr>
        <tr>
            <td class="align-middle">
                <img src="{{ asset('images/sources/facebook.png') }}" alt="Facebook" />
            </td>
            <td class="text-center align-middle">
                @isset($source_facebook)
                    <h3 class="text-success">Connected to Facebook</h3>
                    <a href="/locations/{{  $location->id }}/sources/{{ $source_facebook->id }}/delete" class="disconnect">disconnect</a>
                @else
                    <form class="form-horizontal d-block" role="form" action="/locations/{{ $location->id }}/sources" method="post" id="location-create-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="source_id" value="3">
                        <input type="hidden" name="redirect_uri" value="{{ $redirect }}">

                        <div class="form-group row d-block" id='fb-field'>
                            <div class="fb-login-button " data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" data-scope="manage_pages,public_profile,pages_show_list,business_management" onlogin="checkLoginState"></div>
                        </div>

                        <div class="form-group row hidden" id='fb-page-field'>
                            <label for="inputEmail3" class="col-3 col-form-label">Page</label>
                            <div class="col-9">
                                <select class="form-control" name="fb_page_info" id="fb-page">

                                </select>
                            </div>
                        </div>
                    </form>
                @endif

            </td>
            <td class="text-center source-check">
                @isset($source_facebook)
                    <i class="mdi mdi-check-circle text-success"></i>
                @endisset
            </td>
            <td class="text-center source-check">
                @isset($source_facebook_view)
                    <i class="mdi mdi-check-circle text-success"></i>
                @endisset
            </td>
        </tr>
        <tr>
            <td class="align-middle">
                <img src="{{ asset('images/sources/yelp.png') }}" alt="Yelp" />
            </td>
            <td class="text-center align-middle">
                @isset($source_yelp)
                    <h3 class="text-success">Connected to Yelp</h3>
                    <a href="/locations/{{ $location->id }}/sources/{{ $source_yelp->id }}/delete" class="disconnect">disconnect</a>
                @else
                    <button type="button" class="btn-yelp btn btn-outline-dark pointer" data-toggle="modal" data-target="#yelpModal">
                        <i class="fa fa-yelp"></i>
                        Yelp
                    </button>
                @endif
            </td>
            <td class="text-center source-check">
                <i class="mdi mdi-check-circle text-success"></i>
            </td>
            <td class="text-center source-check">
                <i class="mdi mdi-check-circle text-success"></i>
            </td>
        </tr>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="yelpModal" tabindex="-1" role="dialog" aria-labelledby="yelpModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Connect to Yelp</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal d-block" role="form" action="/locations/{{ $location->id }}/sources" method="post" id="location-create-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="source_id" value="1">
                    <input type="hidden" name="redirect_uri" value="{{ $redirect }}">
                    <input type="hidden" name="yelp_url" value="">

                    <div class="form-group row" id='goog-page-field'>
                        <label class="col-12 col-form-label">Search by Business Name</label>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-9">
                                    <select class="form-control" name='yelp_id' id='yelp-select' style="width: 100%"></select>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control" name='yelp_zip' value='' placeholder="Zip (optional)" style="height: 28px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 m-t-15">
                            <button type="submit" class="btn btn-outline-dark pointer">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    // -------------
    // Facebook
    // -------------
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            console.log(response.status);
            console.log(response.authResponse.accessToken);
            if (response.status == "connected"){
                $.ajax({
                    url: '/locations/_/sources/fbpages',
                    type: 'GET',
                    data: {
                        'access_token': response.authResponse.accessToken
                    },
                    success: function(response) {
                        console.log(response);
                        $("#fb-page").html = "";
                        response.data.forEach(function(item){
                            $("#fb-page").append('<option value="' + item.id +',' + item.access_token +'">' + item.name + '</option>');
                        });

                        $("#fb-field").addClass("hidden");
                        $("#fb-page-field").removeClass("hidden");

                        FB.api("/me/permissions",
                            function (response) {
                              if (response && !response.error) {
                                
                              }
                            });
                    },
                });
            }
        });
    }


    window.fbAsyncInit = function() {
        FB.init({
            appId      : "{{ env('FB_PUB') }}",
            cookie     : true,
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();   

    };

    (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // -------------
    // Google
    // -------------
    @if ($googleLoggedIn)
        $(function(){
            $.ajax({
                url: '/locations/_/sources/googpages',
                type: 'GET',
                success: function(response) {
                    console.log(response);
                    response.accounts.forEach(function(account){
                        console.log(account);
                        $("#goog-page").append('<option disabled>' + account.name + '</option>');
                        account.locations.forEach(function(location){
                            console.log(location);
                            $("#goog-page").append('<option value="' + location.code + '">&nbsp;&nbsp;' + location.name + '</option>');
                        });
                    });
                },
            });
        });
    @endif

    // -------------
    // Yelp
    // -------------
    $(function(){
        var results = [];
        var lat = "";
        var long = "";

        var select = $('#yelp-select').select2({
            ajax: {
                url: '/locations/_/sources/yelpplaces',
                delay: 1000,
                data: function(params){
                    return {
                        term: params.term,
                        lat: lat,
                        long: long,
                        zip: $('[name=yelp_zip]').val()
                    };
                },
                processResults: function (data) {
                    results = data.results
                    return {
                        results: data.results
                    };
                }
            }
        });

        $("#yelp-select").change(function(){
            let bussinessId = $("#yelp-select option:selected").val();
            console.log(bussinessId);
            let picked = results.filter(function(r){
                return r.id == bussinessId
            })[0];
            $("[name=yelp_url]").val(picked.url);

        });

        getLocation();
    });
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            console.log("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        console.log("Lat: " + lat);
        console.log("Long: " + long);
    }
</script>





