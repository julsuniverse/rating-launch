<table class="table mb-0 table-responsive">
    <thead>
        <tr>
            <th>Type</th>
            <th>Link</th>
            <th style="width: 20px;"></th>
        </tr>
    </thead>
    <tbody>
    	@foreach($sources as $source)
            <tr>
                <td>{{ $source->type->text }}</td>
                <td><a href="{{ $source->url }}" target="_blank">{{ $source->url }}</a></td>
                <td><a href="/locations/{{ $location->id }}/sources/{{ $source->id }}/delete"><i class="fa fa-trash"></i></a></td>
            </tr>
        @endforeach

    </tbody>
</table>