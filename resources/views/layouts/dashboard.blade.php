 <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Rating Launch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rating Launch - Smart Review Collection and Tracking" name="description" />
        <meta content="Rating Launch" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="/images/favicon.ico">

        @yield('head-meta')

        <!-- App css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/css/style.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="/plugins/chartist/dist/css/chartist.css">
        <link rel="stylesheet" href="/plugins/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css" />

        <script src="/js/jquery.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.11/d3.min.js"></script>
    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <a href="/" class="logo">
                            <!-- <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                            <span class="logo-large"><i class="mdi mdi-radar"></i> Rating Launch</span> -->
                            <img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">
                        </a>
                        <!-- Image Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--<img src="/images/logo_dark.png" alt="" height="24" class="logo-lg">-->
                        <!--<img src="/images/logo_sm.png" alt="" height="24" class="logo-sm">-->
                        <!--</a>-->

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">

                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell noti-icon"></i>
                                    <span class="badge badge-pink noti-icon-badge">{{ $notifications_count }}</span>
                                </a>
                                @if(!empty($notifications))
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg" aria-labelledby="Preview">

                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5 class="font-16"><span class="badge badge-danger float-right">{{ $notifications_count }}</span>Notifications</h5>
                                        </div>

                                        @foreach($notifications as $notification)
                                            <!-- item-->
                                            <a href="{{ $notification->uri }}" class="dropdown-item notify-item">
                                                <div class="notify-icon bg-success"><i class="mdi mdi-comment-account"></i></div>
                                                <p class="notify-details">{{ $notification->text }}<small class="text-muted">{{ $notification->created_at->format('m/d/Y') }}</small></p>
                                            </a>
                                        @endforeach

                                        @if($notifications_count > 0)
                                            <!-- All-->
                                            <a href="{{ route('notifications') }}" class="dropdown-item notify-item notify-all">
                                                View All
                                            </a>
                                        @endif
                                    </div>
                                @endif
                            </li>

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="{{ Auth::user()->gravatar_url }}" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                    <!-- item-->
                                    <a href="/locations" class="dropdown-item notify-item">
                                        <i class="mdi mdi-home-map-marker"></i> <span>Locations</span>
                                    </a>

                                    <!-- item-->
                                    <a href="/settings" class="dropdown-item notify-item">
                                        <i class="mdi mdi-settings"></i> <span>Settings</span>
                                    </a>

                                    @if (Auth::user()->role_id == 1)
                                        <!-- item-->
                                        <a href="/sentiments" class="dropdown-item notify-item">
                                            <i class="mdi mdi-emoticon-happy"></i> <span>Sentiments</span>
                                        </a>
                                    @endif

                                    <!-- item-->
                                    <a href="/logout" class="dropdown-item notify-item">
                                        <i class="mdi mdi-logout"></i> <span>Logout</span>
                                    </a>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu {{Request::segment(1) == '/' ? 'active' : null}}">
                                <a href="/"><i class="ti-home"></i>Home</a>
                            </li>

                            @if (Auth::user()->role_id == 1)
                                <li class="has-submenu {{Request::segment(1) == 'users' ? 'active' : null}}">
                                    <a href="/users"><i class="fa fa-user-o"></i>Users</a>
                                </li>

                                <li class="has-submenu {{Request::segment(1) == 'leads' ? 'active' : null}}">
                                    <a href="{{ route('leads.index') }}"><i class="fa  fa-user-plus"></i>Leads</a>
                                </li>
                            @else
                                <li class="has-submenu {{Request::segment(1) == 'reviews' ? 'active' : null}}">
                                    <a href="/reviews"><i class="ti-star"></i>Reviews</a>
                                </li>

                                <li class="has-submenu {{Request::segment(1) == 'insights' ? 'active' : null}}">
                                    <a href="/insights"><i class="fa fa-binoculars"></i>Insights</a>
                                </li>

                                <li class="has-submenu {{Request::segment(1) == 'reviewers' ? 'active' : null}}">
                                    <a href="/reviewers"><i class="fa fa-user-o"></i>Reviewers</a>
                                </li>

                            @endif
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">
                @if(count($errors))
                    <br><br>
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger m-b-10">
                            <strong>Oh snap!</strong> {{ $error }}
                        </div>
                    @endforeach
                @endif

                @if(session('success'))
                    <br><br>
                    <div class="alert alert-success m-b-10">
                        <strong>Yay!</strong> {{ session('success') }}
                    </div>
                @endif

                    <!-- Page-Title -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                @section('breadcrums')
                                    <div class="btn-group pull-right">
                                        <ol class="breadcrumb hide-phone p-0 m-0">
                                            <li class="breadcrumb-item"><a href="/"><i class="fa  fa-home"></i></a></li>
                                            <?php $cumulative_path = "" ?>
                                            @foreach (explode('/', Request::path()) as $path)
                                                <?php $cumulative_path .= "/$path" ?>
                                                @if (!ctype_digit($path))
                                                    @if (!$loop->last && $path != "")
                                                        <li class="breadcrumb-item active"><a href="{{ strtolower($cumulative_path) }}">{{ strtoupper($path) }}</a></li>
                                                    @elseif ($path != "" || $loop->index == 0)
                                                        <li class="breadcrumb-item active">{{ strtoupper($path) }}</li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </ol>
                                    </div>
                                @show
                                <h4 class="page-title">@yield('title')</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title end breadcrumb -->


                @yield('content')

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        @include('includes.contact-modal')
        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        {{ date('Y') }} © RatingLaunch
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/js/jquery.app.js"></script>
        <script src="/js/mindmup-editabletable.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js" type="text/javascript"></script>
        
        <!-- circliful Chart -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>

        @stack('scripts')
    </body>
</html>