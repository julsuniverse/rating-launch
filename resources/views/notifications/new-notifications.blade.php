@extends('layouts.dashboard')

@section('title')
    New Notifications
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card-box widget-icon">
                <h4 class="m-t-0 m-b-20 header-title"><b>New Reviews</b></h4>

                <div class="inbox-widget">
                    @foreach($notifications as $notification)
                        <a href="{{ $notification->uri }}">
                            <div class="inbox-item">
                                <div class="bg-icon bg-icon-danger pull-left m-r-15">
                                    <i class="mdi mdi-comment-account text-success"></i>
                                </div>
                                <p class="inbox-item-author">{{ $notification->text }}</p>
                                <p class="inbox-item-date">{{ $notification->created_at->format('m/d/Y') }}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>

        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection