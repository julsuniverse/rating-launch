@if($locations->count() == 1)
    <a href="/locations/{{ $locations[0]->hashed_id }}/reviewer" target="_blank" class="btn btn-secondary waves-effect" style="padding: 4px 11px;">
            Kiosk
    </a>
@else
    <button type="button" class="btn btn-secondary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" style="padding: 4px 11px;">
        Kiosk Mode <span class="caret"></span>
    </button>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
        @foreach($locations as $location)
            <a class="dropdown-item" href="/locations/{{ $location->hashed_id }}/reviewer" target="_blank">{{ $location->text }}</a>
        @endforeach
    </div>
@endif