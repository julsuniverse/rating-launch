<?php

namespace App\Providers;

use App\ViewComposers\GetNotificationsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot(){
        // Using class based composers...
        View::composer(
            'source.add-source', 'App\ViewComposers\AddSourceComposer'
        );
        View::composer('layouts.dashboard', GetNotificationsComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(){
        
    }
}