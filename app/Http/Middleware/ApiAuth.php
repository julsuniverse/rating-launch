<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use App\Models\Token;
use Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('X-Api-Token'))){
            return response()->json(["error" => "no token found in header"]);
        }

        $token = Token::where('token', $request->header('X-Api-Token'))->get()->first();
        if (empty($token)){
            return response()->json(["error" => "no token found in db"]);
        }

        Auth::loginUsingId($token->user_id);

        return $next($request);
    }

}