<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Services\CompetitorsService;
use GooglePlaces;
use Illuminate\Http\Request;
use SKAgarwal\GoogleApi\Exceptions\GooglePlacesApiException;

class InsightController extends Controller{

    private $competitorsService;

    public function __construct(
        CompetitorsService $competitorsService
    )
    {
        $this->competitorsService = $competitorsService;
    }

    function index()
    {
        $location = \Auth::user()->locations;
        $location = $location->first();
        try {
            $competitors = $this->competitorsService->getCompetitors($location);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
        return view('insight.index')->with([
            'location' => $location,
            'competitors' => $competitors,
        ]);
    }

    function competitors(Location $location)
    {
        try {
            $competitors = $this->competitorsService->getCompetitors($location);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        return view('insight.competitors-update')->with([
            'location' => $location,
            'competitors' => $competitors,
        ]);
    }

    function search(Request $request)
    {
        try {
            $competitors = GooglePlaces::nearbySearch($request->lat. ',' .$request->long, 50000, ['type' => $request->term]);
            if ($competitors['status'] == 'OK') {
                return $competitors['results'];
            }
        } catch (GooglePlacesApiException $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }

    function saveCompetitor(Request $request) {
        //
    }
}
