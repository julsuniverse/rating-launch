<?php

namespace App\Http\Controllers;

use App\Mail\ContactEmail;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'comment' => 'required'
        ]);

        \Mail::to(config('mail.admin_email'))->send(new ContactEmail($request->all()));

        return redirect()->back()->withSuccess('Your message have been sent!');
    }
}
