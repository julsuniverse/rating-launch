<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\SentimentCategory;
use App\Models\SentimentWord;

use App\Jobs\RateReviews;

class SentimentController extends Controller{

    function index(){
    	$categories = SentimentCategory::all();
        return view('sentiment.index', ['categories'=> $categories]);
    }

    function show($catId){
    	$category = SentimentCategory::find($catId);
    	return view('sentiment.show', ['category'=> $category]);
    }
    
    function run(){
		RateReviews::dispatch();
		return redirect('/sentiments')->withSuccess("Reviews being analyzed!");
    }

    function editWord($catId, $wordId, Request $request){
    	$word = SentimentWord::find($wordId);
    	
    	if ($request->type == "label"){
    		$word->word = $request->value;
    	}else if ($request->type == "value"){
    		$word->score = $request->value;
    	}
    	$word->save();

    	return response()->json(["success"=> "true", "word"=> $word]);
    }

    function addWord($catId){
    	$category = SentimentCategory::find($catId);

    	$word = new SentimentWord;
    	$word->word = "--";
    	$word->score = 0;
    	$word->sentiment_category_id = $catId;
    	$word->save();

    	return response()->json(["success"=> "true", "word"=> $word]);
    }

    function deleteWord($catId, $wordId){
    	$word = SentimentWord::find($wordId);
    	$word->delete();

    	return redirect('/sentiments/'.$catId);
    }
}
