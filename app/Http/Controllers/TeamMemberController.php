<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMemberEmail;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\CompanyUserPermission;
use App\Models\EditableLocation;
use App\Models\Location;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;

class TeamMemberController extends Controller
{
    public function index(Company $company)
    {
        $companyUsers = CompanyUser::with('permissions')
            ->where(['company_id' => $company->id, 'role_id' => 4])
            ->get();
        $users = User::whereIn('id', $companyUsers->pluck('user_id'))->with(['companyUser' => function($query) {
            return $query->with('permissions');
        }])->get();

        return view('team-member.index')->with([
            'company' => $company,
            'users' => $users
        ]);
    }

    public function create(Company $company)
    {
        return view('user-managment.create')->with('company', $company);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'title' => 'required|max:255'
        ]);

        $password = str_random(8);

        try {
            $user = \DB::transaction(function () use ($request, $password) {
                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($password),
                    'role' => 4 //team member
                ]);

                CompanyUser::create([
                    'company_id' => $request->company,
                    'user_id' => $user->id,
                    'role_id' => 4
                ]);

                return $user;
            });
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors(['Something went wrong']);
        }

        \Mail::to($user->email)->send(new WelcomeMemberEmail($password));
        return redirect()->route('users.index')->withSuccess('New team member has been registered!');
    }

    public function edit(CompanyUser $companyUser)
    {
        $permissions = Permission::all();
        $locations = Location::where('company_id', $companyUser->company_id)->get();

        $userPermissions = CompanyUserPermission::select(['id', 'permission_id'])
            ->where('company_user_id', $companyUser->id)
            ->get();

        $editableLocations = EditableLocation::select('location_id')
            ->whereIn('company_user_permissions_id', $userPermissions->pluck('id')->toArray())
            ->pluck('location_id')
            ->toArray();

        return view('team-member.edit')->with([
            'permissions' => $permissions,
            'user' => $companyUser->user,
            'companyUser' => $companyUser,
            'locations' => $locations,
            'userPermissions' => $userPermissions->pluck('permission_id')->toArray(),
            'editableLocations' => $editableLocations,
        ]);
    }

    public function update(Request $request, CompanyUser $companyUser)
    {
        $permissions = $request->except('_token', 'locations');

        if($request->locations) {
            $locations = $request->locations;
        }

        try {
            CompanyUserPermission::where('company_user_id', $companyUser->id)->delete();
            foreach($permissions as $key => $value) {
                $companyUserPermission = CompanyUserPermission::create([
                    'permission_id' => $key,
                    'company_user_id' => $companyUser->id
                ]);

                if($key == 2) {
                    if(isset($locations)) {
                        foreach ($locations as $location) {
                            EditableLocation::create([
                                'company_user_permissions_id' => $companyUserPermission->id,
                                'location_id' => (int)$location
                            ]);
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors(['Something went wrong']);
        }


        return redirect()->route('team-members.index', ['company'=> $companyUser->company_id])->withSuccess('Permissions were updated!');
    }
}
