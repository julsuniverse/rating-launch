<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Models\User;
use App\Models\Company;
use App\Models\Location;
use App\Models\SentimentCategory;
use App\Models\ReviewCount;

use Carbon\Carbon;

class DashboardController extends Controller{

    function index(Request $request){
        if (Auth::user()->role_id == 1){
            return redirect('/super-dashboard');
        }

    	$company = Auth::user()->companies->first();
        if (empty($company)){
            return redirect('/onboarding/1');
        }
    	$categories = SentimentCategory::all();

        $activeLocationId = $request->cookie('active_location');
        $rating = 1;
        if ($activeLocationId != 0 && !empty($activeLocationId)){
            $location = Location::find($activeLocationId);
            $activeLocationId = $location->hashed_id;
            $reviews = $location->reviews->sortBy('native_created_at')->reverse();
            $counts = $location->reviewCounts()->where('created_at', '>', Carbon::now()->subDays(5))->get()->load('source');
            if ($reviews->count() > 0){
                $rating = $reviews->sum('rating') / $reviews->count();
            }else{
                $rating = 1;
            }
        }else{
            $reviews = $company->reviews->sortBy('native_created_at')->reverse();
            $counts = $company->reviewCounts()->where('created_at', '>', Carbon::now()->subDays(5))->get()->load('source');
            $location = NULL;
            if ($reviews->count() > 0){
                $rating = $reviews->sum('rating') / $reviews->count();
            }else{
                $rating = 1;
            }
        }

        // Get and format data for review count graphs, while zeroing out empty days
        $perSourcePerDay = array();
        $sourceIds = collect();
        if (!empty($counts->first())){
            $oldest = $counts->sortBy('created_at')->first()->created_at;
            $sourceIds = $counts->groupBy('review_source_id')->keys();

            foreach ($sourceIds as $sourceId) {
                $perSourcePerDay[$sourceId] = array();
            }

            for ($i = 0; $i <= $oldest->diffInDays(Carbon::now()); $i++){
                $day = $oldest->copy()->addDays($i)->format('m/d/y');
                $filtered = $counts->filter(function($c) use ($day){
                    return $c->created_at->format('m/d/y') == $day;
                });
                //dd($filtered);
                foreach ($sourceIds as $sourceId) {
                    $perSourcePerDay[$sourceId][$day] = $filtered->filter(function($c) use ($sourceId){
                        return $c->review_source_id == $sourceId;
                    })->values(); // Not using a groupBy here so that we get an empty collection if there are no objects
                }
            }
        } // END data formating

        return view('dashboard.index', [
            'company'=> $company,
            'reviews'=> $reviews,
            'categories'=> $categories,
            'perSourcePerDay'=>$perSourcePerDay,
            'sourceIds'=>$sourceIds,
            'location'=> $location,
            'rating'=> $rating,
            'active_location_id' => $activeLocationId,
        ]);
    }

    function setActiveLocation($locationId){
        $cookie = cookie('active_location', $locationId, 60*6);
        return redirect('/')->cookie($cookie);
    }
    
}
