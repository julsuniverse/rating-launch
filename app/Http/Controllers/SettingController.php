<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;

class SettingController extends Controller{

    function index(){
    	\Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

    	$user = Auth::user();
        $user->createStripeCustomer();
    	$customer = \Stripe\Customer::retrieve($user->stripe_id);
        // return $customer;

        return view('setting.index', ['user'=> $user, 'customer'=>$customer]);
    }

    function saveCard(Request $request){
    	\Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

    	$user = Auth::user();
    	$customer = \Stripe\Customer::retrieve($user->stripe_id);
		$customer->sources->create(array("source" => $request->stripeToken));

		return redirect('/settings');
    }

    function primaryCard($cardId){
    	\Stripe\Stripe::setApiKey(env('STRIPE_SEC'));
    	$user = Auth::user();
    	$customer = \Stripe\Customer::retrieve($user->stripe_id);
    	$customer->default_source = $cardId;
		$customer->save();

		return redirect('/settings');
    }
    
}
