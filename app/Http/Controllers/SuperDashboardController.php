<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\Subscription;
use App\Services\SubscriptionCounter;
use Illuminate\Http\Request;

class SuperDashboardController extends Controller
{
    private $subscriptionCounter;

    public function __construct(SubscriptionCounter $subscriptionCounter)
    {
        $this->subscriptionCounter = $subscriptionCounter;
    }

    function index()
    {
        $reviews = Review::all()->count();

        $all = Subscription::all()->count();
        $subscribe = Subscription::active()->count();
        $trial = Subscription::trialing()->count();
        $canceled = Subscription::canceled()->count();

        $three_m_paid = $this->subscriptionCounter->three_m_paid;
        $two_m_paid = $this->subscriptionCounter->two_m_paid;
        $one_m_paid = $this->subscriptionCounter->one_m_paid;
        $this_m_paid = $this->subscriptionCounter->this_m_paid;

        $three_m_canceled = $this->subscriptionCounter->three_m_canceled;
        $two_m_canceled = $this->subscriptionCounter->two_m_canceled;
        $one_m_canceled = $this->subscriptionCounter->one_m_canceled;
        $this_m_canceled = $this->subscriptionCounter->this_m_canceled;

        return view('super-dashboard.index', [
            'all' => $all,
            'reviews' => $reviews,
            'subscribe' => $subscribe,
            'trial' => $trial,
            'canceled' => $canceled,
            'three_m_paid' => $three_m_paid,
            'two_m_paid' => $two_m_paid,
            'one_m_paid' => $one_m_paid,
            'this_m_paid' => $this_m_paid,
            'three_m_canceled' => $three_m_canceled,
            'two_m_canceled' => $two_m_canceled,
            'one_m_canceled' => $one_m_canceled,
            'this_m_canceled' => $this_m_canceled,
            'this_month' => $this->subscriptionCounter->this_month,
            'three_months_ago' => $this->subscriptionCounter->three_months_ago,
            'two_months_ago' => $this->subscriptionCounter->two_months_ago,
            'one_month_ago' => $this->subscriptionCounter->one_month_ago,
         ]);
    }
}
