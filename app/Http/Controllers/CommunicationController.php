<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Reviewer;
use App\Models\Review;
use App\Models\Communication;

use Twilio\Twiml;

class CommunicationController extends Controller{

    function sms(Request $request){
        $from = str_replace("+1", "", $request->From);
        $message = $request->Body;

        $reviewer = Reviewer::where('phone', $from)->get()->first();
        if (!empty($reviewer)){
            $lastCom = $reviewer->communications->last();
            $wasReviewRequest = false;
            if (!empty($lastCom)){
                $wasReviewRequest = $lastCom->type == "request_review";
            }

            $communication = new Communication;
            $communication->method = "sms";
            $communication->type = $wasReviewRequest ? "request_review" : "_";
            $communication->reviewer_id = $reviewer->id;
            $communication->text = $message;
            $communication->from_client = 1;
            $communication->save();

            // if ($wasReviewRequest){
            //     $review = $reviewer->review;
            //     if (empty($review)){
            //         $review = new Review;
            //         $review->location_id = $reviewer->location_id;
            //         $review->company_id = $review->location->company_id;
            //         $review->text = $message;
            //         $review->rating = 4;
            //         $review->review_source_id = 5;
            //         $review->user_fullname = $reviewer->name;
            //         $review->user_image_url = "/images/users/hidden_user.jpg";
            //         $review->native_review_id = "{inhouse}";
            //         $review->save();

            //         $reviewer->review_id = $review->id;
            //         $reviewer->save();
            //     }else{
            //         $review = $reviewer->review;
            //         $review->text .= "<br><br>" . $message;
            //         $review->save();
            //     }
            // }

            $response = new Twiml();
            $response->message(
                "Thanks For your feedback!"
            );
            return $response;
        }else{
            $response = new Twiml();
            $response->message(
                "Number $from not recognised"
            );
            return $response;
        }
    }
    
}
