<?php

namespace App\Http\Controllers;

use App\Models\LocationMedia;
use App\Models\PlaceType;
use App\Models\TagType;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;

class LocationController extends Controller{

    function index(){
    	$locations = Auth::user()->locations;
        return view('location.index', ['locations'=> $locations]);
    }

    function show($locationId){
    	$location = Location::find($locationId);
        $media = LocationMedia::where('location_id', $locationId)->get();
        $place_types = PlaceType::all();

        return view('location.show', [
    	    'location'=> $location,
            'media' => $media,
            'place_types' => $place_types,
        ]);
    }

    function create(){
    	return view('location.create');
    }

    function store(Request $request){
        $this->validate($request, [
            'location_name' => 'required',
            'places_id' => 'required_without:street,city,state,zip,country',
            'street' => 'required_without:places_id',
            'city' => 'required_without:places_id',
            'state' => 'required_without:places_id',
            'zip' => 'required_without:places_id',
            'country' => 'required_without:places_id',
        ]);

        $location = new Location;
        $location->text = $request->location_name;
        $location->street = $request->street;
        $location->city = $request->city;
        $location->state = isset($request->state) ? $request->state : '';
        $location->zip = isset($request->zip) ? $request->zip : '';
        $location->country = $request->country;
        $location->company_id = Auth::user()->companies->first()->id;
        $location->save();

        return redirect('/locations');
    }

    function storeMedia(Request $request)
    {
        $this->validate($request, [
            'location_id' => 'required|integer',
            'media_id' => 'required|integer',
        ]);

        $medias = LocationMedia::where('location_id', $request->location_id)->get();
        foreach ($medias as $media) {
            if($media->id != $request->media_id) {
                $media->is_primary = 0;
            } else {
                $media->is_primary = 1;
            }
            $media->save();
        }

        return response()->json('Saved', 200);
    }

    function reviewsWidget(int $location)
    {
        $location = Location::find($location)->hashed_id;
        return view('review.reviews-widget')
            ->with(['location' => $location]);
    }

    function update(Request $request, $location)
    {
        //dd($request->except(['_token', '_method']));
        $this->validate($request, [
            'text' => 'required|string',
            'street' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'zip' => 'required|string',
            'country' => 'required|string',
            'place_type' => 'required|int'
        ]);

        Location::where(['id' => $location])->update($request->except(['_token', '_method']));

        return redirect(route('locations.show', ['location' => $location]));
    }
    
}
