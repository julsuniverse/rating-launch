<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMemberEmail;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class UserManagementController extends Controller{

    function index(){
    	$users = User::all();
        return view('user-managment.index', ['users'=> $users]);
    }

    function supervise($userId){
    	if (Auth::user()->role_id == 1){
    		Auth::loginUsingId($userId);
    	}

    	return redirect('/');
    }

    function create()
    {
        return view('user-managment.create');
    }

    function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'title' => 'required|max:255'
        ]);

        $password = str_random(8);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password),
        ]);

        if ($user) {
            \Mail::to($user->email)->send(new WelcomeMemberEmail($password));
            return redirect()->route('users.index')->withSuccess('New team member has been registered!');
        }

        return redirect()->back()->withErrors(['Something went wrong']);
    }
}
