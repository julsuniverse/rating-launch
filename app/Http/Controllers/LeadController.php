<?php

namespace App\Http\Controllers;

use App\Adapter\LocationAdapter;
use App\Http\Requests\StoreLeads;
use App\Http\Requests\UploadLeads;
use App\Models\Lead;
use App\Models\User;
use App\Services\Locations\FindGooglePlaceService;
use App\Services\Locations\FindYelpPlaceService;
use App\Services\Locations\YelpClientService;
use App\Services\ParseFacebook;
use GooglePlaces;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use SKAgarwal\GoogleApi\Exceptions\GooglePlacesApiException;

class LeadController extends Controller
{
    private $googlePlaceService;
    public $fb_service;
    private $locationAdapter;

    public function __construct(
        FindGooglePlaceService $googlePlaceService,
        ParseFacebook $parseFacebook,
        LocationAdapter $locationAdapter
    )
    {
        $this->googlePlaceService = $googlePlaceService;
        $this->fb_service = $parseFacebook;
        $this->locationAdapter = $locationAdapter;
    }

    public function index()
    {
        $leads = Lead::orderBy('id', 'DESC')->paginate(20);
        return view('leads.index')->with([
            'leads' => $leads,
        ]);
    }

    public function create()
    {
        return view('leads.create');
    }

    public function store(StoreLeads $request)
    {
        try {
            $lead = Lead::create($request->input());
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors(['Something went wrong']);
        }
        return redirect()->route('leads.check', ['lead' => $lead])->withSuccess('New lead was added!');
    }

    public function import()
    {
        return view('leads.import');
    }

    public function upload(UploadLeads $request)
    {
        $path = $request->file('file')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get()->toArray();

        if (count($data) > 0) {
            foreach ($data as $row) {
                try {
                    $user = User::where(['email' => $row['email']])->first();
                    if(!$user) {
                        Lead::create([
                            'name' => $row['name'],
                            'email' => $row['email'],
                            'company_name' => $row['company_name'],
                            'address_1' => $row['shipping_address_1'],
                            'address_2' => $row['shipping_address_2'],
                            'city' => $row['shipping_city'],
                            'state' => $row['shipping_stateprovince'],
                            'zip' => $row['shipping_zip'],
                        ]);
                    } else {
                        continue;
                    }

                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                }
            }
        } else {
            return redirect()->back()->withErrors(['File is empty!']);
        }

        return redirect()->route('leads.index')->withSuccess('Leads were successfully imported!');
    }

    public function show(Lead $lead)
    {
        return view('leads.show')->with([
            'lead' => $lead
        ]);
    }

    public function edit(Lead $lead, $check = false)
    {
        return view('leads.edit')->with([
            'lead' => $lead,
            'check' => $check,
        ]);
    }

    public function update(StoreLeads $request, Lead $lead, $check = false)
    {
        try {
            Lead::where('id', $lead->id)->update($request->except('_method', '_token'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors(['Something went wrong']);
        }
        if(!$check) {
            return redirect()->route('leads.index')->withSuccess('Lead was updated!');
        } else {
            return redirect()->route('leads.check', ['lead' => $lead])->withSuccess('Lead was updated!');
        }
    }

    public function check(Lead $lead)
    {
        if($lead->google_place_id) {
            $place = $this->googlePlaceService->findById($lead->google_place_id);
        } else {
           $place = $this->googlePlaceService->find($this->locationAdapter->adaptLead($lead));
        }

        return view('leads.check')->with([
            'lead' => $lead,
            'place' => $place,
        ]);
    }

    public function confirm(Request $request)
    {
        try {
            Lead::where('id', $request->lead_id)->update([
                'address_1' => $request->full_address,
                'company_name' => $request->name,
                'google_rating' => $request->google_rating,
                'google_place_id' => $request->google_place_id,
                'yelp_id' => FindYelpPlaceService::getBusinessId($request),
                'fb_id' => $this->fb_service->getPlaceId($request),
                'checked' => 1,
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors(['Something went wrong']);
        }

        return redirect()->route('leads.index')->withSuccess('Location was confirmed!');
    }
}
