<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Subscription;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;
use App\Models\Review;
use App\Models\ReviewSource;
use App\Models\SentimentCategory;

class ReviewController extends Controller{

    function index(){
    	$company = Auth::user()->companies->first();
    	$reviews = $company->reviews->sortBy('native_created_at')->reverse();
    	$categories = SentimentCategory::all();
        $locations = $company->locations;
        $sources = ReviewSource::all();

        $unread = $reviews->filter(function($r){ return !$r->read; })->count();
        $needsResponse = $company->reviews()
            ->whereHas('reviewer', function($q){
                $q->whereHas('communications', function($q){
                    $q->where('read', 0);
                });
            })
            ->count();

        return view('review.index', [
            'reviews'=> $reviews, 
            'categories'=> $categories, 
            'locations'=> $locations, 
            'sources'=> $sources,
            'unread'=> $unread,
            'needsResponse'=> $needsResponse
        ]);
    }

    function show($reviewId, $read = false)
    {
        $review = Review::find($reviewId);
        $categories = SentimentCategory::all();

        if($read) {
            Notification::setNotificationRead($reviewId);
        }

        if (!$review->read) {
            $review->read = true;
            $review->save();
        }

        return view('review.show', ['review' => $review, 'categories' => $categories]);
    }

    function getReviews(Request $request){
        $company = Auth::user()->companies->first();

        $reviews;
        if ($request->primary == "needs_response"){
            $reviews = $company->reviews()->whereHas('reviewer', function($q){
                $q->whereHas('communications', function($q){
                    $q->where('read', 0);
                });
            });
        }else{
            $reviews = $company->reviews();
        }

        if ($request->primary == "starred"){
            $reviews = $reviews->where('starred', '1');
        }

        if ($request->primary == "archive"){
            $reviews = $reviews->whereNotNull('deleted_at');
        }

        if ($request->location != "*"){
            $reviews = $reviews->where('location_id', $request->location);
        }
        
        if ($request->platform != ""){
            $reviews = $reviews->where('review_source_id', $request->platform);
        }

        if ($request->stars != ""){
            $reviews = $reviews->where('rating', $request->stars);
        }

        $reviews = $reviews->orderBy('native_created_at', 'desc')->paginate(25);

        $pageJson = $reviews->toArray();

        $html = view('review.reviews-inbox', ['reviews'=> $reviews])->render();

        return response()->json([
            'html'=> $html, 
            'total'=> $pageJson["total"], 
            'results_per'=> $pageJson["per_page"],
            'count'=> $reviews->count()
        ]);
    }


    function getLocationReviews(string $location, string $user)
    {
        $user = User::findByHash($user);
        $subscription_status = Subscription::select('status')->where('customer', $user->stripe_id)->first()->status;

        if($subscription_status != ('active' || 'trial')) {
            return view('review.reviews-iframe-error')->render();
        } else {
            $location = Location::findForHash($location);
            $reviews = Review::where(['location_id' => $location->id])->orderBy('native_created_at', 'desc')->paginate(25);
            return view('review.reviews-iframe', ['reviews'=> $reviews])->render();
        }
    }

    function toggleStarred($reviewId){
        $review = Review::find($reviewId);
        $review->starred = !$review->starred;
        $review->save();

        return response()->json(['starred'=> $review->starred]);
    }
    
}
