<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\ReviewSource;
use App\Services\Locations\YelpService;
use Cookie;

use App\Http\Controllers\CronController;

class SourceController extends Controller{

    function index($locationId){
        return view('blank');
    }

    function show($locationId, $sourceId){
        return view('blank');
    }

    function create($locationId){
    	$location = Location::findForHash($locationId)->load('sources');
    	$sources = ReviewSource::where('id', '!=', 5)->get();

    	$source_google = $location->sources->where('review_category_id', 6)->first();
    	$source_facebook = $location->sources->where('review_category_id', 3)->first();
    	$source_yelp = $location->sources->where('review_category_id', 1)->first();

        $source_google_view = $source_google ? Review::where('location_id', $source_google->location_id)->first() : $source_google;
        $source_facebook_view = $source_facebook ? Review::where('location_id', $source_facebook->location_id)->first() : $source_facebook;
    	$source_yelp_view = $source_yelp ? Review::where('location_id', $source_yelp->location_id)->first() : $source_yelp;

        return view('source.create', [
            'location'=> $location,
            'sources'=> $sources,
            'source_google' => $source_google,
            'source_facebook' => $source_facebook,
            'source_yelp' => $source_yelp,
            'source_google_view' => $source_google_view,
            'source_facebook_view' => $source_facebook_view,
            'source_yelp_view' => $source_yelp_view,
        ]);
    }

    function store(Request $request, $locationId){
    	$this->validate($request, [
            'source_id' => 'required',
        ]);

    	$location = Location::find($locationId);

    	$sourceUrl = new ReviewSourceUrl;
    	$sourceUrl->location_id = $location->id;
    	$sourceUrl->review_category_id = $request->source_id;
        if ($request->source_id == 1){ // Yelp
            if ($request->has('yelp_url') && $request->has('yelp_id')){
                $sourceUrl->url = $request->yelp_url;
                $sourceUrl->data1 = $request->yelp_id;
            }else{
                return back()->withErrors(["Missing Yelp Url or Yelp Id"]);
            }
        }elseif ($request->source_id == 3){ // FB
            if ($request->has('fb_page_info')){
                $parts = explode(",", $request->fb_page_info);
                $sourceUrl->url = "https://www.facebook.com/pg/" . $parts[0];
                $sourceUrl->data1 = $parts[0];
                $sourceUrl->data2 = $parts[1];
            }else{
                return back()->withErrors(["Missing Facebook Page"]);
            }
        }elseif ($request->source_id == 6){ // Google
            if ($request->has('goog_page_info')){
                if (empty($request->cookie('google_refresh'))){
                    return back()->withErrors(["Missing Google Login"]);
                }

                $sourceUrl->data1 = $request->goog_page_info;
                $sourceUrl->data2 = $request->cookie('google_refresh');

                $client = new \Google_Client();
                $client->setAuthConfig(env('GOOGLE_BUSSINESS_PATH'));
                $client->refreshToken($request->cookie('google_refresh'));

                require app_path('/Services/Google_Service_MyBusiness.php'); //Fuck google
                $bussiness = new \Google_Service_MyBusiness($client);
                $gLoc = $bussiness->accounts_locations->get($sourceUrl->data1);
                
                $sourceUrl->url = $gLoc["metadata"]["mapsUrl"];
                $sourceUrl->data3 = explode("=", $gLoc["metadata"]["newReviewUrl"])[1];

            }else{
                return back()->withErrors(["Missing Google Location"]);
            }
        }
    	$sourceUrl->save();

        CronController::getReviewsForLocations([$location]);
        CronController::pullMediaForLocations([$location]);

    	return redirect($request->redirect_uri);
    }
    
    function destory($locationId, $sourceId){
        $source = ReviewSourceUrl::find($sourceId);
        $source->delete();

        return back()->withSuccess('Source Deleted');
    }

    function getFbPages(Request $request){
        $short = $request->access_token;
        $url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=417049545417697&client_secret=70ff88e46805a40b565d2f1a57c959b2&fb_exchange_token=$short";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        $long_access = $obj->access_token;

        $url = "https://graph.facebook.com/me/accounts?access_token=$long_access";
        $json = file_get_contents($url);
        $obj = json_decode($json, true);

        return $obj;
    }

    function getGoogPages(Request $request){
        if (!empty($request->cookie('google_refresh'))){
            $client = new \Google_Client();
            $client->setAuthConfig(env('GOOGLE_BUSSINESS_PATH'));
            $client->refreshToken($request->cookie('google_refresh'));

            require app_path('/Services/Google_Service_MyBusiness.php'); //Fuck google
            $bussiness = new \Google_Service_MyBusiness($client);

            $accounts = collect();
            foreach ($bussiness->accounts->listAccounts() as $gAccount) {
                $code = $gAccount["name"];

                $gLocations = $bussiness->accounts_locations->listAccountsLocations($code)["locations"];
                $locations = collect();
                foreach ($gLocations as $gLocation) {
                    $locations->push([
                        "name"=> $gLocation["locationName"],
                        "code"=> $gLocation["name"]
                    ]);
                }

                $accounts->push([
                    "name"=> $gAccount["accountName"],
                    "code"=> $code,
                    "locations"=> $locations
                ]);
            }

            return response()->json(['success'=> 'true', 'accounts'=> $accounts]);
        }
        return response()->json(['success'=> 'false'], 403);
    }

    function googleOauth(Request $request){
        $client = new \Google_Client();
        $client->setAuthConfig(env('GOOGLE_BUSSINESS_PATH'));
        $token = $client->fetchAccessTokenWithAuthCode($request->code);

        if (empty($token["access_token"])){
            return $token;
        }
        
        $accessToken = $token["access_token"];
        $refresh = $token["refresh_token"];

        Cookie::queue("google_token", $accessToken, 60);
        Cookie::queue("google_refresh", $refresh, 180);

        return redirect($request->state);
    }

    function getYelpPlaces(Request $request){
        $term = $request->input('term');
        if (empty($term) || $term == ""){
            $term = "food";
        }
        $results = YelpService::searchFor($term, $request);
        return response()->json(['results'=>$results]);
    }
}
