<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Reviewer;
use App\Models\Location;
use App\Models\Communication;

use App\Services\CommunicationService;

use Carbon\Carbon;

class ReviewerController extends Controller{

    function index(){
        $user = Auth::user();
    	$locations = $user->locations;
        $locIds = $locations->map(function($l){ return $l->id; });
        $reviewers = Reviewer::whereIn('location_id', $locIds)->get()->load('communications', 'review', 'location');
        return view('reviewer.index', ['reviewers'=> $reviewers, "locations"=> $locations]);
    }

    function show($reviewerId){
    	$reviewer = Reviewer::find($reviewerId);

        $comItems = collect();
        foreach ($reviewer->communications as $comm) {
            $item = array();
            $item["type"] = "communication";
            $item["date"] = $comm->created_at;
            if ($comm->from_client){
                $item["sender_name"] = $reviewer->name;
                $item["sender_image_url"] = "";
                $item["from"] = $comm->method == "sms" ? $reviewer->phone : $reviewer->email;
            }else{
                $item["sender_name"] = $comm->sender->name;
                $item["sender_image_url"] = $comm->sender->gravatar_url;
                $item["from"] = $comm->method == "sms" ? "‭+1 (213) 354-8464‬" : "no-reply@ratinglaunch.com";
            }
            $item["from_reviewer"] = $comm->from_client;
            $item["method"] = $comm->method;
            $item["comm_type"] = $comm->type;
            $item["text"] = $comm->text;
            $comItems->push($item);

            if (!$comm->read){
                $comm->read = 1;
                $comm->save();
            }
        }

        $review = $reviewer->review;
        if (!empty($review)){
            $item = array();
            $item["type"] = "review";
            $item["date"] = $review->created_at;
            $item["sender_name"] = $reviewer->name;
            $item["from_reviewer"] = 1;
            $item["method"] = "web form";
            $item["text"] = $review->text;
            $item["rating"] = $review->rating;
            $item["sender_image_url"] = "";
            $item["from"] = "online";
            $comItems->push($item);
        }

        $comItems = $comItems->sortBy(function($item){
            return $item["date"];
        });

    	return view('reviewer.show', ['reviewer'=> $reviewer, 'comItems'=> $comItems]);
    }

    function showImport(){
    	return view('reviewer.import');
    }

    function textForReview($reviewerId){
        $reviewer = Reviewer::find($reviewerId);
        $url = config('app.url')."/locations/".$reviewer->location->hashed_id."/write/".$reviewer->id;    
        $prefill = "We hope you had a good experience, please let us know what you thought! " . $url;
        return view('reviewer.communication', ['prefilledText'=> $prefill, 'reviewer'=> $reviewer, 'messageType'=> 2, 'communicationType'=> 1]);
    }

    function emailForReview($reviewerId){
    	$reviewer = Reviewer::find($reviewerId);
        $url = "https://app.ratinglaunch.com/locations/".$reviewer->location->hashed_id."/write/".$reviewer->id;    
    	return view('reviewer.communication', ['reviewer'=> $reviewer, 'messageType'=> 2, 'communicationType'=> 2]);
    }

    function newCommunication($reviewerId){
    	$reviewer = Reviewer::find($reviewerId);

    	return view('reviewer.communication', ['reviewer'=> $reviewer, 'messageType'=> 1, 'communicationType'=> 1]);
    }

    function sendCommunication($reviewerId, Request $request){
    	$this->validate($request, [
            'message_type' => 'required',
            'communication_type' => 'required',
        ]);

        $reviewer = Reviewer::find($reviewerId);

        $communication = new Communication;
        $communication->method = $request->communication_type;
        $communication->type = $request->message_type;
        $communication->reviewer_id = $reviewerId;
        $communication->text = $request->has('message') ? $request->message : "Email Sent";
        $communication->sender_id = Auth::user()->id ?? 54;
        $communication->from_client = 0;
        $communication->save();

        CommunicationService::sendCommunication($communication);

        return redirect('/reviewers/'.$reviewerId);
    }

    function import(Request $request){
    	$this->validate($request, [
            'file' => 'required'
        ]);

    	$user = Auth::user();

    	$handle = fopen($request->file->path(), "r");
		$header = true;
		$cnt = 0;
		while ($csvLine = fgetcsv($handle, 1000, ",")) {
		    if ($header) {
		        $header = false;
		    } else if (count($csvLine) == 3){
		        $reviewer = new Reviewer;
		        $reviewer->name = $csvLine[0];
		        $reviewer->phone = $csvLine[1];
		        $reviewer->email = $csvLine[2];
		        $reviewer->location_id = $user->locations->first()->id;
		        $reviewer->save();
		        $cnt += 1;
		    }
		}

		return redirect('/reviewers')->withSuccess("$cnt Reviewers Imported!");
    }

    function createForLocation($locationId){
        $location = Location::findForHash($locationId);
        return view('reviewer.create', ['location'=> $location]);
    }

    function successForLocation($locationId, $reviewerId){
        $location = Location::findForHash($locationId);
        $reviewer = Reviewer::find($reviewerId);
        return view('reviewer.create-success', ['location'=> $location, 'reviewer'=> $reviewer]);
    }

    function storeForLocation($locationId, Request $request){
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required'
        ]);

        $location = Location::findForHash($locationId);

        $reviewer = new Reviewer;
        $reviewer->name = $request->first_name . " " . $request->last_name;
        $reviewer->phone = $request->phone;
        $reviewer->email = $request->email;
        $reviewer->location_id = $location->id;
        $reviewer->save();

        $url = config('app.url')."/locations/".$reviewer->location->hashed_id."/write/".$reviewer->id;    
        
        $prefill = "We hope you had a good experience, please let us know what you thought! " . $url;
        $communication = new Communication;
        $communication->method = "sms";
        $communication->type = "request_review";
        $communication->reviewer_id = $reviewer->id;
        $communication->text = $prefill;
        $communication->sender_id = Auth::id();
        $communication->from_client = 0;
        $communication->save();

        if ($request->has('phone') && !empty($request->phone)){
            CommunicationService::sendCommunication($communication);
        }else{
            $communication->method = "email";
            $communication->save();
            CommunicationService::sendCommunication($communication);
        }

        return redirect('/locations/' . $location->hashed_id . '/reviewer/success/'.$reviewer->id);
    }

    function unsubscribe($reviewerId){
        return "You Have Been Successfully Unsubscribed";
    }
    
}
