<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PHPHtmlParser\Dom;

use App\Models\User;
use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\Review;
use App\Models\SentimentCategory;

use App\Jobs\ProcessYelpReviews;
use App\Jobs\ProcessGoogleReviews;
use App\Jobs\ProcessFacebookReviews;
use App\Jobs\ProcessTripAdvisorReviews;
use App\Jobs\RateReviews;

use Illuminate\Queue\QueueManager;

class TestController extends Controller{

    function getYelpReviews(){
        $location = Location::find(1);
        $source = ReviewSourceUrl::find(1);
        ProcessYelpReviews::dispatch($location, $source);
        
        return "done";
    }

    function getGoogle(QueueManager $queueManager){
    	$location = Location::find(3);
    	$source = ReviewSourceUrl::find(67);
        
        $defaultDriver = $queueManager->getDefaultDriver();

        $queueManager->setDefaultDriver('sync');

        \Queue::push(new ProcessGoogleReviews($location, $source));

        $queueManager->setDefaultDriver($defaultDriver);

    	return "done";
    }

    function getFbReviews(QueueManager $queueManager){
        $location = Location::find(27);
        $source = ReviewSourceUrl::find(39);

        $defaultDriver = $queueManager->getDefaultDriver();

        $queueManager->setDefaultDriver('sync');

        \Queue::push(new ProcessFacebookReviews($location, $source));

        $queueManager->setDefaultDriver($defaultDriver);
        
        return "done";
    }
    
    function rateReviews(){
    	RateReviews::dispatch();

    	return "success";
    }
}


