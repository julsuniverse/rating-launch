<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessUsersWithoutAccounts;
use App\Jobs\ProcessSubscriptions;
use App\Jobs\ProcessUsersWithoutReviews;
use App\Models\Location;
use App\Models\LocationMedia;
use App\Jobs\ProcessYelpReviews;
use App\Jobs\ProcessYelpMedia;
use App\Jobs\ProcessFacebookReviews;
use App\Jobs\ProcessGoogleReviews;
use App\Jobs\RateReviews;
use App\Jobs\TotalReviewCounts;

class CronController extends Controller{

    function getReviews(){
    	$locations = Location::all();
    	CronController::getReviewsForLocations($locations);

    	return response()->json(["success"=>"true", "message"=>"job queued for pulling and rating reviews"]);
    }

    function rateReviews(){
		RateReviews::dispatch();
		return response()->json(["success"=>"true", "message"=>"job queued"]);
    }

    function pullMedia(){
        $locations = Location::all();
        CronController::pullMediaForLocations($locations);
        return response()->json(["success"=>"true", "message"=>"job queued"]);
    }

    function daily(){
        $this->pullMedia();
        $this->pullSubscriptions();
        $this->emailUsersWithoutReviews();
        $this->checkNewUsers();

        return response()->json(["success"=>"true", "message"=>"job queued"]);
    }

    function pullSubscriptions()
    {
        ProcessSubscriptions::dispatch();
    }

    function emailUsersWithoutReviews()
    {
        ProcessUsersWithoutReviews::dispatch();
    }

    function checkNewUsers()
    {
        ProcessUsersWithoutAccounts::dispatch();
    }

    static function pullMediaForLocations($locations){
        foreach ($locations as $location) {
            LocationMedia::where('location_id', $location->id)->delete();
            foreach ($location->sources as $source) {
                if ($source->review_category_id == 1){
                    ProcessYelpMedia::dispatch($location, $source);
                }
            }
        }
    }

    static function getReviewsForLocations($locations){
        foreach ($locations as $location) {
            foreach ($location->sources as $source) {
                if ($source->review_category_id == 1){
                    ProcessYelpReviews::dispatch($location, $source);
                }elseif ($source->review_category_id == 3){
                    ProcessFacebookReviews::dispatch($location, $source);
                }elseif ($source->review_category_id == 6){
                    ProcessGoogleReviews::dispatch($location, $source);
                }
            }
            TotalReviewCounts::dispatch($location);
        }

        RateReviews::dispatch();
    }

}
