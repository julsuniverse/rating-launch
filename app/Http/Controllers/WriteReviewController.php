<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;
use App\Models\Review;
use App\Models\Reviewer;
use App\Models\SentimentCategory;

use Carbon\Carbon;

class WriteReviewController extends Controller{

    function index($locationId, $reviewerId, Request $request){
        $location = Location::findForHash($locationId);
        $rating = $request->has('rating') ? intval($request->rating) : 5;
        $reviewer = Reviewer::find($reviewerId);
        $alreadyReviewed = $reviewer->review_id != NULL;
        if ($alreadyReviewed){
            $rating = intval($reviewer->review->rating * 5);
        }

        return view('write-review.index', ['location'=> $location, 'reviewer'=> $reviewer, 'rating'=> $rating, 'alreadyReviewed'=> $alreadyReviewed]);
    }

    function store(Request $request, $locationId, $reviewerId){
    	$this->validate($request, [
            'review' => 'required',
            'rating' => 'required'
        ]);

    	$location = Location::findForHash($locationId);
        $reviewer = Reviewer::find($reviewerId);

		$review = new Review;
		$review->location_id = $location->id;
		$review->company_id = $location->company_id;
		$review->text = $request->review;
		$review->rating = intval($request->rating) / 5;
		$review->review_source_id = 5;
		$review->user_fullname = $reviewer->name;;
		$review->user_image_url = "/images/users/hidden_user.jpg";
		$review->native_review_id = "{inhouse}";
        $review->native_created_at = Carbon::now();
		$review->save();

        $reviewer->review_id = $review->id;
        $reviewer->save();

    	return redirect('/locations/' . $locationId . '/write/'.$reviewerId)->withSuccess("Feedback Saved!");
    }

}
