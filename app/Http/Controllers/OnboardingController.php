<?php

namespace App\Http\Controllers;

use App\Models\CompanyUser;
use Illuminate\Http\Request;

use Auth;
use LocationSearch;

use App\Models\User;
use App\Models\Location;
use App\Models\Plan;
use App\Models\Company;
use App\Models\Review;
use App\Models\SentimentCategory;
use App\Models\ReviewSourceUrl;

use App\Services\Locations\LocationSearchService;
use App\Http\Controllers\CronController;

class OnboardingController extends Controller{

    function showStep($step){
        $user = Auth::user();

        if (empty($user->stripe_id)){
            $user->createStripeCustomer();
        }

        return view('onboarding.show', ['step'=> $step, 'user'=> $user]);
    }
    
    function saveInfo(Request $request){
        $this->validate($request, [
            'location_name' => 'required',
            'places_id' => 'required_without:street,city,state,zip,country',
            'street' => 'required_without:places_id',
            'city' => 'required_without:places_id',
            'state' => 'required_without:places_id',
            'zip' => 'required_without:places_id',
            'country' => 'required_without:places_id',
        ]);

        $company = new Company;
        $company->text = $request->location_name;
        $company->save();
        Auth::user()->companies()->attach($company);

        $location = new Location;
        if ($request->has('places_id')){
            $data = LocationSearchService::getLocationForPlace($request->places_id);
            if (empty($data)){
                $company->delete();
                return redirect('/onboarding/1')->withErrors(['Internal Error, Invalid Google Place Id']);
            }
            $company->text = $data["text"];
            $company->save();

            $location->text = $data["text"];
            $location->street = $data["street"];
            $location->city = $data["city"];
            $location->state = isset($data["state"]) ? $data["state"] : '';
            $location->zip = isset($data["zip"]) ? $data["zip"] : '';
            $location->country = $data["country"];
            $location->company_id = $company->id;
            $location->save();
        }else{
            $location->text = $request->location_name;
            $location->street = $request->street;
            $location->city = $request->city;
            $location->state = $request->state;
            $location->zip = $request->zip;
            $location->country = $request->country;
            $location->company_id = $company->id;
            $location->save();
        }

        return redirect('/onboarding/2')->with(['companyId' => $company->id]);
    }

    function addSource(Request $request){
        $this->validate($request, [
            'source_id' => 'required',
        ]);

        $location = Auth::user()->locations->first();

        $sourceUrl = new ReviewSourceUrl;
        $sourceUrl->location_id = $location->id;
        $sourceUrl->review_category_id = $request->source_id;
        if ($request->source_id == 1){
            if ($request->has('url') && !empty($request->url)){
                $sourceUrl->url = $request->url;
            }else{
                return redirect('/onboarding/2')->withErrors(['Missing Yelp Url']);
            }
        }else if ($request->source_id == 3){
            if ($request->has('fb_page_info') && !empty($request->fb_page_info)){
                $parts = explode(",", $request->fb_page_info);
                $sourceUrl->url = "https://www.facebook.com/pg/" . $parts[0];
                $sourceUrl->data1 = $parts[0];
                $sourceUrl->data2 = $parts[1];
            }else{
                return redirect('/onboarding/2')->withErrors(['Missing Facebook Page']);
            }
        }
        $sourceUrl->save();

        return redirect('/onboarding/2');
    }

    function saveCard(Request $request){
        \Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

        $user = Auth::user();

        // Add Card
        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->sources->create(array("source" => $request->stripeToken));

        // Make Card Primary
        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->default_source = $customer->sources->data[0]->id;
        $customer->save(); 

        // Pay
        $coupon = $request->coupon;
        try {
            $subscription = \Stripe\Subscription::create([
                'customer' => $user->stripe_id,
                'items' => [['plan' => 'basic-plan']],
                'coupon' => $coupon
            ]);

            CompanyUser::where([
                'user_id' => $user->id,
                'company_id' => $user->companies()->orderBy('id', 'desc')->first()->id
            ])
                ->update(['subscription' => $subscription->id]);

        } catch (\Stripe\Error\Base $e) {
          return redirect('/onboarding/3')->withErrors([$e->getMessage()]);
        } catch (Exception $e) {
            return redirect('/onboarding/3')->withErrors(["Internal Server Error"]);
        }

        $locations = Auth::user()->locations;
        CronController::getReviewsForLocations($locations);

        return redirect('/')->withSuccess('We are currently pulling your reviews!');
    }

    function termsAccepted(Request $request){
        $this->validate($request, [
            'terms_of_use' => 'required',
        ]);

        $locations = Auth::user()->locations;
        CronController::getReviewsForLocations($locations);

        return redirect('/')->withSuccess('We are currently pulling your reviews!');
    }

    function businessSearch(Request $request){
        $term = $request->input('term');
        if (empty($term) || $term == ""){
            $term = "food";
        }
        $results = LocationSearchService::searchFor($term, $request);
        return response()->json(['results'=>$results]);
    }

    function checkCoupon(Request $request){
        $this->validate($request, [
            'code' => 'required',
            'plan' => 'required',
        ]);

        $plan = Plan::find($request->plan);
        $price = $plan->priceForCoupon($request->code);

        return response()->json(['price'=> strval(number_format($price, 2)), 'found'=> $price != $plan->price]);
    }
}
