<?php

namespace App\Http\Controllers;

use App\Models\Notification;

class NotificationController
{
    function index()
    {
        $notifications = Notification::where([
            'user_id' => \Auth::id(),
            'read' => 0
        ])->orderBy('id', 'DESC')->get();

        return view('notifications.new-notifications')->with([
            'notifications' => $notifications,
        ]);
    }
}