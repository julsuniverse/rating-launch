<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreLeads extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id_to_ignore = isset($this->lead->id) ? $this->lead->id : null;
        return [
            'salutation' => 'max:255',
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'email' => 'email|max:255|unique:leads,id,'.$id_to_ignore . '|unique:users,email',
            'address_1' => 'max:255',
            'address_2' => 'max:255',
            'city' => 'max:255',
            'state' => 'max:255',
            'zip' => 'max:255',
            'phone' => 'max:255',
            'place_type' => 'max:255',
            'country' => 'max:2',

        ];
    }
}
