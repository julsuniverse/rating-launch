<?php

namespace App\Listeners;

use App\Events\CheckNewReviews;
use App\Jobs\CreateNotifications;

class CreateReviewNotification
{
    public function handle(CheckNewReviews $event)
    {
        CreateNotifications::dispatch($event->review);
    }
}
