<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;

class ParseFacebook
{
    public $css_class;

    public $token;
    public $baseUrl;


    public function __construct()
    {
        $this->token = env('FB_PUB') . '|' . env('FB_SEC');
        $this->baseUrl = 'https://graph.facebook.com/';
    }

    public function getBody($url)
    {
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException $e) {
            \Log::error($e->getMessage());
            return null;
        }

        $body = $res->getBody();

        return $body;
    }

    public function makeUrl($request)
    {
        $str = $request->name . ' ' . $request->full_address;
        $url = str_replace('#', '', $str);
        return $url;
    }

    /**
     * @param $request
     * @return mixed|null|\Psr\Http\Message\StreamInterface
     * @url https://developers.facebook.com/docs/places/web/search
     */
    public function getPlaces($request)
    {
        $url = $this->makeUrl($request);

        $query = urldecode(http_build_query([
           'type' => 'place',
           'q' => strtolower($url),
           'access_token' => $this->token
        ]));

        $url = $this->baseUrl . 'search?' . $query;
        $places = $this->getBody($url);
        $places = json_decode($places, true);

        return $places;
    }

    public function getPlaceId($request)
    {
        $places = $this->getPlaces($request);

        if(!empty($places['data'])) {
            $place_id = $places['data']['0']['id'];
            return $place_id;
        }
        else {
            return null;
        }
    }

    /**
     * @param $place_id
     * @return mixed|null|\Psr\Http\Message\StreamInterface
     * @url https://developers.facebook.com/docs/places/web/place-information
     */
    public function getRating($place_id)
    {
        $query = urldecode(http_build_query([
            'access_token' => $this->token,
            'fields' => 'overall_star_rating'
        ]));
        $url = $this->baseUrl . 'v3.0/' . $place_id . '?' . $query;
        $rating = $this->getBody($url);

        if(!empty($rating)) {
            $rating = json_decode($rating, true);
            $rating = $rating['overall_star_rating'];

            return $rating;
        }

        return null;
    }



}