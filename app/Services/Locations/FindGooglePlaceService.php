<?php

namespace App\Services\Locations;

use App\Adapter\LocationAdapter;
use GooglePlaces;
use SKAgarwal\GoogleApi\Exceptions\GooglePlacesApiException;

/**
 * Class FindGooglePlaceService
 * @package App\Services\Locations
 */
class FindGooglePlaceService
{
    /**
     * @param LocationAdapter $location
     * @return bool|string
     */
    public function composeSearch(LocationAdapter $location)
    {
        if (!empty($location->company_name) && empty($location->address)) {
            $search = $location->company_name;
        } else if (!empty($location->address) && empty($location->company_name)) {
            $search = $location->address;
        } else if (!empty($location->company_name) && !empty($location->address)) {
            $search = $location->company_name . ' ' . $location->address;
        } else {
            $search = false;
        }

        return $search;
    }

    /**
     * @param LocationAdapter $location
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Support\Collection|int
     * @link https://developers.google.com/places/web-service/search#FindPlaceRequests
     */
    public function find(LocationAdapter $location)
    {
        $search = $this->composeSearch($location);

        try {
            if(!empty($search)) {
                $place = GooglePlaces::textSearch($search);

                if($place['status'] == 'OK') {
                    $place = $place['results'][0];
                } else if($place['status'] == 'ZERO_RESULTS') {
                    $place = 0;
                }
            } else {
                $place = 0;
            }
        } catch (GooglePlacesApiException $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        return $place;
    }

    /**
     * @param $place_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Support\Collection|int|mixed
     * @link https://developers.google.com/places/web-service/details
     */
    public function findById($place_id)
    {
        try {
            $place = GooglePlaces::placeDetails($place_id);
        } catch (GooglePlacesApiException $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        if($place['status'] == 'OK') {
            $place = $place['result'];
        } else if($place['status'] == 'ZERO_RESULTS') {
            $place = 0;
        }

        return $place;
    }

    /**
     * @param LocationAdapter $location
     * @return \Illuminate\Support\Collection|mixed
     * @link https://developers.google.com/places/web-service/search#PlaceSearchRequests
     */
    public function findCompetitors(LocationAdapter $location, $place_type)
    {
        try {
            $nearbyLocations = GooglePlaces::nearbySearch($this->getGeographyLocation($location), 50000, ['type' => $place_type]);
        } catch (GooglePlacesApiException $e) {
            \Log::error($e->getMessage());
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
        if ($nearbyLocations['status'] == 'OK') {
            $nearbyLocations = $nearbyLocations['results'];
            $nearbyLocations = $nearbyLocations->slice(0, 5);
            return $nearbyLocations;
        } else {
            return 0;
        }

    }

    public function getGeographyLocation(LocationAdapter $location)
    {
        $place = $this->find($location);
        return $place['geometry']['location']['lat'] . ',' . $this->lng = $place['geometry']['location']['lng'];
    }
}