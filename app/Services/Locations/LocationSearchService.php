<?php 

namespace App\Services\Locations;

use Illuminate\Http\Request;

use LocationSearch;

class LocationSearchService{
	
	static function searchFor($term, $request){
		$loc = LocationSearch::get($request->ip());
        if (!empty($loc)){
            $curl = curl_init();

            $term = urlencode($term);
            $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".$term."&key=".env('GOOGLE_API');
            if ($request->has('lat') && $request->has('long')){
                $url .= "&location=". $request->lat . "," . $request->long;
            }else{
                $url .= "&location=". $loc->latitude . "," . $loc->longitude;
            }

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer 5W66_o0nUzm1KJqUAt4snQK4V0vHRkU6D085fhyb7ALYp2dMFVSJyJMOTEAsRXcy5sooyf-Ut8CHYt6XE1rB4d3-zMHjGBrZ1MiLOnEgfZcojSNv0P3wsm9LDsOTWnYx",
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if (!$err) {
                $ret = json_decode($response, true);
                if (empty($ret['results'])){
                    return [[
                        "id"=> 'custom',
                        "text"=> 'Don\'t See Your Business?'
                    ]];
                }

                $locations = collect($ret['results']);
                $addressResults = $locations->filter(function($b){
                    return in_array("premise", $b['types']) || in_array("street_address", $b['types']);
                });

                $businesses = $locations->reject(function($b){
                    return in_array("premise", $b['types']) || in_array("street_address", $b['types']);
                });

                foreach ($addressResults as $address) {
                	$bussinessForLocations = Self::getLocationFor($address["geometry"]["location"]["lat"], $address["geometry"]["location"]["lng"]);
                	$businesses = $businesses->merge($bussinessForLocations);
                }
                // return $businesses;
                $mappedBussinessed = Self::mapBussinessFieldsForLocations($businesses);

                if (!empty($mappedBussinessed)){
                    return $mappedBussinessed; 
                }
            }
        }

        return [[
            "id"=> 'custom',
            "text"=> 'Don\'t See Your Business?'
        ]];

	}

	static function getLocationFor($lat, $long){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$lat.",".$long."&rankby=distance&key=".env("GOOGLE_API"),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET"
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if (!$err) {
		 	$ret = json_decode($response, true);
		 	if (empty($ret['results'])){
		 		return NULL;
		 	}

		 	return collect($ret['results'])->chunk(10)->first();
		}

		return NULL;
	}

	static function getLocationForPlace($placeId){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/details/json?key=".env("GOOGLE_API")."&placeid=".$placeId,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET"
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if (!$err) {
		 	$ret = json_decode($response, true);
		 	if (empty($ret['result'])){
		 		return NULL;
		 	}

			$bussinesses = collect([$ret['result']]);
			$mapped = Self::mapBussinessFieldsForLocations($bussinesses);
			return $mapped->first();
		}

		return NULL;
	}

	static function mapBussinessFieldsForLocations($locations){
        $businesses = $locations->map(function($b){

            $data = [
                "id"=> $b['id'],
                "text"=> $b['name'],
                "lat"=> $b["geometry"]["location"]["lat"],
                "long"=> $b["geometry"]["location"]["lng"],
                "places_id"=> $b['place_id'],
                "url"=> "https://www.google.com/maps/place/?q=place_id:".$b['place_id'],
                "image"=> $b['icon'],
                "rating" => $rating ?? 0
            ];

            if (!empty($b["formatted_address"])){
	            $addressParts = explode(", ", $b["formatted_address"]);
                $data["city"] = $addressParts[1];
                $subAddressParts = explode(" ", $addressParts[2]);
                if (count($subAddressParts) == 2){
	                $data["state"] = $subAddressParts[0];
	                $data["zip"] = $subAddressParts[1];
	            }
                $data["country"] = $addressParts[3];
                $data["street"] = trim($addressParts[0]);
                $data["address"] = $b["formatted_address"];
	        }else{
	        	$data["vicinity"] = trim($b["vicinity"]);
	        	$data["city"] = "";
                $data["state"] = "";
                $data["zip"] = "";
                $data["country"] = "";
                $data["street"] = "";
                $data["address"] = $b["vicinity"];
	        }

	        return $data;
        });

        $businesses->push([
            "id"=> 'custom',
            "text"=> 'Don\'t See Your Business?'
        ]);
        //dd($businesses);
        return $businesses;
	}

}

