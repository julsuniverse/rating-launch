<?php

namespace App\Services\Locations;

use Stevenmaguire\Yelp\v3\Client;

class YelpClientService extends Client
{
    /**
     * @param array $parameters
     * @return \Stevenmaguire\Yelp\Tool\stdClass
     * @link     https://www.yelp.com/developers/documentation/v3/business_match
     */
    public function getBusinessesMatches($parameters = [])
    {
        $path = $this->appendParametersToUrl('/v3/businesses/matches', $parameters);
        $request = $this->getRequest('GET', $path);

        return $this->processRequest($request);
    }
}