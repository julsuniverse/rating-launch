<?php 

namespace App\Services\Locations;

use Illuminate\Http\Request;

use LocationSearch;

class YelpService{
	
	static function searchFor($term, Request $request){
		$loc = LocationSearch::get($request->ip());
		if (!empty($loc)){
			$curl = curl_init();
            $params = [
              'term' => str_replace(' ', '+', $term),
              'radius' => 40000,
              'limit' => 50
            ];
			$url = "https://api.yelp.com/v3/businesses/search?" . urldecode(http_build_query($params));

            if ($request->has('zip')){
                $url .= "&location=". $request->zip;
            }elseif ($request->has('lat') && $request->has('long')){
				$url .= "&latitude=". $request->lat . "&longitude=" . $request->long;
			}else{
				$url .= "&location=". $loc->cityName . ", " . $loc->regionName;
			}

			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer 5W66_o0nUzm1KJqUAt4snQK4V0vHRkU6D085fhyb7ALYp2dMFVSJyJMOTEAsRXcy5sooyf-Ut8CHYt6XE1rB4d3-zMHjGBrZ1MiLOnEgfZcojSNv0P3wsm9LDsOTWnYx",
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if (!$err) {
				$ret = json_decode($response, true);
				if (empty($ret['businesses'])){
					return collect();
				}

				$businesses = collect($ret['businesses']);
				$businesses = $businesses->map(function($b){
					 return [
						"id"=> $b['id'],
						"text"=> $b['name'],
						"city"=> $b['location']['city'],
						"state"=> $b['location']['state'],
						"zip"=> $b['location']['zip_code'],
						"country"=> $b['location']['country'],
						"street"=> trim($b['location']['address1'] . " " . $b['location']['address2'] . " " . $b['location']['address3']),
						"url"=> $b['url'],
						"image"=> $b['image_url']
					];
				});

				if (!empty($businesses)){
					return $businesses; 
				}
			}
        }

        return collect();
	}

}

