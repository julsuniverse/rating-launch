<?php

namespace App\Services\Locations;

class FindYelpPlaceService
{
    public static function getBusinessId($request)
    {
        $client = new YelpClientService(array(
            'accessToken' => config('yelp.token'),
        ));

        $lead = json_decode($request->lead);

        $params = [
            'name' => $request->name,
            'address1' => $request->full_address,
            'city' => $lead->city,
            'state' => $lead->state,
            'country' => $lead->country,
        ];

        $yelp_place = $client->getBusinessesMatches($params);
        if (!empty($yelp_place->businesses)) {
            return $yelp_place->businesses[0]->id;
        }

        return null;
    }
}