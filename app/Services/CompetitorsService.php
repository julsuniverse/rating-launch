<?php

namespace App\Services;

use App\Adapter\LocationAdapter;
use App\Models\Location;
use App\Models\PlaceType;
use App\Services\Locations\FindGooglePlaceService;

class CompetitorsService
{
    private $googlePlaceService;
    private $locationAdapter;

    public function __construct(
        FindGooglePlaceService $googlePlaceService,
        LocationAdapter $locationAdapter
    )
    {
        $this->googlePlaceService = $googlePlaceService;
        $this->locationAdapter = $locationAdapter;
    }

    /**
     * @param Location $location
     * @return \Illuminate\Support\Collection|mixed
     * @throws \Exception
     */
    public function getCompetitors(Location $location)
    {
        if(!$location->place_type) {
            throw new \Exception('Place type is not set for this location. You can set it in settings.');
        }
        $place_type = PlaceType::select('name')->where('id', $location->place_type)->first()->name;

        $myLocation = $this->googlePlaceService->find($this->locationAdapter->adaptLocation($location));
        $myLocation['my_location'] = 1;

        $competitors = $this->googlePlaceService->findCompetitors($this->locationAdapter->adaptLocation($location), $place_type);
        $competitors->push($myLocation);
        $competitors = $competitors->sortByDesc('rating');

        return $competitors;
    }
}