<?php


namespace App\Services;

use App\Models\Subscription;

class SubscriptionCounter
{
    public $this_month;
    public $three_months_ago;
    public $two_months_ago;
    public $one_month_ago;

    public $three_m_paid;
    public $two_m_paid;
    public $one_m_paid;
    public $this_m_paid;
    public $three_m_canceled;
    public $two_m_canceled;
    public $one_m_canceled;
    public $this_m_canceled;

    public function __construct()
    {
        $this->this_month = new \DateTime('first day of');
        $this->three_months_ago = new \DateTime('first day of 3 months ago');
        $this->two_months_ago = new \DateTime('first day of 2 months ago');
        $this->one_month_ago = new \DateTime('first day of 1 month ago');

        $this->three_m_paid = $this->getCount('active', $this->three_months_ago->getTimestamp(), $this->two_months_ago->getTimestamp());
        $this->two_m_paid = $this->getCount('active', $this->two_months_ago->getTimestamp(), $this->one_month_ago->getTimestamp());
        $this->one_m_paid = $this->getCount('active', $this->one_month_ago->getTimestamp(), $this->this_month->getTimestamp());
        $this->this_m_paid = $this->getCount('active', $this->this_month->getTimestamp());

        $this->three_m_canceled = $this->getCount('canceled', $this->three_months_ago->getTimestamp(), $this->two_months_ago->getTimestamp());
        $this->two_m_canceled = $this->getCount('canceled', $this->two_months_ago->getTimestamp(), $this->one_month_ago->getTimestamp());
        $this->one_m_canceled = $this->getCount('canceled', $this->one_month_ago->getTimestamp(), $this->this_month->getTimestamp());
        $this->this_m_canceled = $this->getCount('canceled', $this->this_month->getTimestamp());
    }

    private function getCount($status = "active", $date_from = null, $date_to = null)
    {
        $res = Subscription::where(array_filter([
            $date_from ? ['created_at', '>', $date_from] : false,
            $date_to ? ['created_at', '<', $date_to] : false,
        ]));

        if ($status == "active") {
            $res->active();
        } elseif ($status == "canceled") {
            $res->canceled();
        }
        return $res->count();
    }
}