<?php

namespace App\Services\Reviews;


use Yangqi\Htmldom\Htmldom;

class ParseYelpReviewsService
{
    public function getItems($url)
    {
        $html = null;
        $container = null;

        try {
            $html = new Htmldom($url);
            if (!$html) {
                throw new \Exception('empty html');
            }
            $container = $html->find('.reviews');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        if (!$container) {
            return null;
        }

        $items = null;

        if (count($container) == 1) {
            $container = $container[0];
            $items = $container->find('li');
        }

        return $items;
    }

    public function getReviewContent($dom)
    {
        return $dom->find('.review-content', 0);
    }

    public function getText($content)
    {
        return strip_tags($content->find('p', 0));
    }

    public function getName($dom)
    {
        return $dom->find('.user-name', 0)->find('a', 0)->innertext;
    }

    public function getImgSource($dom)
    {
        return $dom->find('.photo-box-img', 0)->src;
    }

    public function getReviewId($dom)
    {
        return trim($dom->find(".review", 0)->getAttribute("data-review-id"));
    }

    public function getReviewDate($content)
    {
        $reviewDate = $content->find(".rating-qualifier", 0)->innertext;
        $reviewDate = trim(substr(trim($reviewDate), 0, 10));

        return $reviewDate;
    }

    public function getRating($dom)
    {
        $ratingText = $dom->find('.i-stars', 0)->title;
        return explode(" ", $ratingText)[0];
    }

}