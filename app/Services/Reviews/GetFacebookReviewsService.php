<?php

namespace App\Services\Reviews;

class GetFacebookReviewsService
{
    public function getResponse($source1, $source2)
    {
        try {
            $fb = new \Facebook\Facebook([
                'app_id' => env('FB_PUB'),
                'app_secret' => env('FB_SEC'),
                'default_graph_version' => 'v2.10',
            ]);

            $response = $fb->get(
                '/'.$source1.'/ratings',
                $source2
            );
            return $response;

        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage() . PHP_EOL;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage() . PHP_EOL;
        }

        return null;
    }
}