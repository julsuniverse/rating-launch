<?php 

namespace App\Services;

use Twilio\Rest\Client;

use App\Mail\PleaseRateEmail;
use Illuminate\Support\Facades\Mail;

class CommunicationService{
	
	static $twilio_number = "2133548464";
	static $account_sid = 'ACb6466329edd47390d3b559c71fb4d2f2';
	static $auth_token = 'cc43e7568758319e63ffb7acaa507d20';

	static function sendCommunication($communication){
		if ($communication->method == "sms"){
			self::sendSMS($communication);
		}else if ($communication->method == "email"){
			self::sendEmail($communication);
		}
	}

	static function sendSMS($communication){
		$client = new Client(self::$account_sid, self::$auth_token);
		$client->messages->create(
		    $communication->reviewer->phone,
		    array(
		        'from' => self::$twilio_number,
		        'body' => $communication->text
		    )
		);
	}

	static function sendEmail($communication){
		$reviewer = $communication->reviewer;
		$location = $communication->reviewer->location;

		Mail::to($reviewer->email)->send(new PleaseRateEmail($reviewer, $location));
		
	}

}

