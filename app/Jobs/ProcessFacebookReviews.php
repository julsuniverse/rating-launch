<?php

namespace App\Jobs;

use App\Services\Reviews\GetFacebookReviewsService;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Review;

use Carbon\Carbon;

class ProcessFacebookReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;
    protected $source;
    private $reviewsService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l, $s)
    {
        $this->location = $l;
        $this->source = $s;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FacebookSDKException
     */
    public function handle(GetFacebookReviewsService $reviewsService)
    {
        $this->reviewsService = $reviewsService;
        $response = $this->reviewsService->getResponse($this->source->data1, $this->source->data1);
        if (!$response) {
            return;
        }

        $ratingEdge = $response->getGraphEdge();

        foreach ($ratingEdge as $ratingNode) {

            $ratingArr = $ratingNode->asArray();
            $time = new Carbon($ratingArr["created_time"]->format(DATE_ISO8601));
            $rating = $ratingNode->getField('rating');
            $text = $ratingNode->getField('review_text');
            if (empty($text)) {
                $text = "--";
            }

            $name = $ratingNode->getField('reviewer')->getField('name');
            $imgSrc = "/images/users/hidden_user.jpg";
            $reviewId = $ratingNode->getField('reviewer')->getField('id');

            $review = Review::where('native_review_id', $reviewId)->first();
            if (empty($review)) {
                $review = new Review;
            } else if ($review->updated_at > Carbon::now()->subDays(7)) {
                continue;
            }

            $review->review_source_id = 3;
            $review->location_id = $this->location->id;
            $review->company_id = $this->location->company_id;
            $review->rating = floatval($rating) / 5;
            $review->text = $text;
            $review->user_fullname = $name;
            $review->user_image_url = $imgSrc;
            $review->native_review_id = $reviewId;
            $review->native_created_at = $time;
            $review->save();

            echo("Facebook Review From: $name saved with id $reviewId<br>");
        }
    }
}
