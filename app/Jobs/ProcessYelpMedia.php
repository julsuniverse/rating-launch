<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\LocationMedia;
use App\Models\Review;

use Carbon\Carbon;

class ProcessYelpMedia implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;
    protected $source;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l, $s){
        $this->location = $l;
        $this->source = $s;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $url = str_replace("https://www.yelp.com/biz", "https://www.yelp.com/biz_photos", $this->source->url);
        $html = new \Htmldom($url);

        $container = $html->find('.photos');
        if (count($container) == 1){
            $container = $container[0];

            $items = $container->find('li');
            foreach ($items as $imageDom) {

                $imgUrl = $imageDom->find('.photo-box-img', 0)->src;
                $nativeId = $imageDom->getAttribute("data-photo-id");

                $media = new LocationMedia;
                $media->location_id = $this->location->id;
                $media->native_id = $nativeId;
                $media->review_source_url_id = $this->source->id;
                $media->company_id = $this->location->company_id;

                $media->url = $imgUrl;
                $media->review_source_id = 1;
                $media->save();
            }

        }
    }
}
