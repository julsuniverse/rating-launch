<?php

namespace App\Jobs;

use App\Models\Notification;
use App\Models\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $review;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = $this->review->company->users;
        $rating = $this->review->rating * 5;
        if($rating <= 1) {
            $stars = 'star';
        } else {
            $stars = 'stars';
        }

        foreach ($users as $user) {
            Notification::create([
                'text' => $this->review->user_fullname ." just gave you " . $rating . " "  . $stars .  " on " . $this->review->source->text . "!",
                'uri' => 'reviews/'.$this->review->id,
                'notification_type_id' => 1,
                'read' => 0,
                'user_id' => $user->id
            ]);
        }

    }
}
