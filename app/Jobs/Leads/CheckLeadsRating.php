<?php

namespace App\Jobs\Leads;

use App\Mail\LeadsRating;
use App\Models\Lead;
use App\Services\Locations\FindGooglePlaceService;
use App\Services\Locations\YelpClientService;
use App\Services\ParseFacebook;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckLeadsRating implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const RATING_MAX = 3.5;
    private $fb;

    /**
     * Execute the job.
     *
     * @param ParseFacebook $fb
     * @return void
     */
    public function handle(ParseFacebook $fb)
    {
        $this->fb = $fb;

        $this->checkGoogle();
        $this->checkYelp();
        $this->checkFacebook();
    }

    public function checkGoogle()
    {
        $leads = Lead::where([
            ['google_place_id', '!=', null],
        ])->get();

        foreach ($leads as $lead) {
            $place = FindGooglePlaceService::findById($lead->google_place_id);
            if(isset($place['rating'])) {
                $rating = $place['rating'];
            }
            $this->checkRating($lead, $rating, 'google', Carbon::now());
        }
    }

    public function checkYelp()
    {
        $leads = Lead::where([['yelp_id', '!=', null]])->get();

        $client = new YelpClientService(array(
            'accessToken' => config('yelp.token'),
        ));

        foreach ($leads as $lead) {
            $rating = $client->getBusiness($lead->yelp_id)->rating;
            $this->checkRating($lead, $rating, 'yelp', Carbon::now()->addDays(4));
        }
    }

    public function checkFacebook()
    {
        $leads = Lead::where([['fb_id', '!=', null]])->get();

        foreach ($leads as $lead) {
            $rating = $this->fb->getRating($lead->fb_id);
            $this->checkRating($lead, $rating, 'facebook', Carbon::now()->addDays(4));
        }
    }

    public function checkRating($lead, $rating, $platform, $when)
    {
        if($rating < self::RATING_MAX) {
            $this->sendMail($lead, $platform, $rating, $when);
        }
    }

    /**
     * @param Lead $lead
     * @param string $platform
     * @param double $rating
     * @param Carbon $when
     */
    public function sendMail($lead, $platform, $rating, $when)
    {
        //TODO: change email to lead's
        \Mail::to('admin@ratinglaunch.com')->later($when, new LeadsRating($lead, $platform, $rating));
    }

}
