<?php

namespace App\Jobs\Leads;

use App\Mail\LeadsRating;
use App\Models\Lead;
use App\Services\Locations\FindGooglePlaceService;
use App\Services\Reviews\ParseYelpReviewsService;
use App\Services\Locations\YelpClientService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckLeadsReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const RATING_MAX = 3.5;
    private $reviewService;

    /**
     * Execute the job.
     *
     * @param ParseYelpReviewsService $reviewsService
     * @return void
     */
    public function handle(ParseYelpReviewsService $reviewsService)
    {
        $this->reviewService = $reviewsService;
        $this->checkGoogle();
        $this->checkYelp();
    }

    public function checkGoogle()
    {
        $leads = Lead::where([
            'checked' => 1
        ])->get();
        foreach ($leads as $lead) {
            $place = FindGooglePlaceService::findById($lead->google_place_id);

            if ($place != 0 && isset($place['reviews'])) {
                $reviews = $place['reviews'];

                $rating = [];

                foreach ($reviews as $review) {
                    if ($review['time'] > Carbon::now()->subDays(16)->timestamp) {
                        if ($review['rating'] < 3) {;
                            $rating[] = $review['rating'];
                        }
                    }
                }

                if (!empty($rating)) {
                    $rating = min($rating);
                    $this->checkRating($lead, $rating, 'google', Carbon::now());
                }

            } else {
                break;
            }
        }
    }

    public function checkYelp()
    {
        $leads = Lead::where([['yelp_id', '!=', null]])->get();

        $client = new YelpClientService(array(
            'accessToken' => config('yelp.token'),
        ));

        foreach ($leads as $lead) {
            $place = $client->getBusiness($lead->yelp_id);
            $items = $this->reviewService->getItems($place->url);
            if (!$items) {
                return;
            }

            $rating = [];

            foreach ($items as $reviewDom) {
                $reviewContent = $this->reviewService->getReviewContent($reviewDom);

                if($reviewContent) {
                    try {
                        if(strtotime($this->reviewService->getReviewDate($reviewContent)) > Carbon::now()->subDays(16)->timestamp); {
                            $rating[] = $this->reviewService->getRating($reviewDom);
                        }
                    } catch (\Exception $e) {
                        \Log::error($e->getMessage());
                        continue;
                    }
                }
            }
            if (!empty($rating)) {
                $rating = min($rating);
                $this->checkRating($lead, $rating, 'yelp', Carbon::now()->addDays(4));
            }
        }
    }

    public function checkFacebook()
    {
        //
    }

    public function checkRating($lead, $rating, $platform, $when)
    {
        if($rating < self::RATING_MAX) {
            $this->sendMail($lead, $platform, $rating, $when);
        }
    }

    /**
     * @param Lead $lead
     * @param string $platform
     * @param double $rating
     * @param $when
     */
    public function sendMail($lead, $platform, $rating, $when)
    {
        //TODO: change email to lead's
        \Mail::to('admin@ratinglaunch.com')->later($when, new LeadsRating($lead, $platform, $rating));
    }

}
