<?php

namespace App\Jobs;

use App\Mail\StartEmail;
use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessUsersWithoutAccounts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $empty_locations = array();

        $users = User::where('created_at', "<", Carbon::now()->subDays(2))
            ->where('created_at', ">", Carbon::now()->subDays(3))
            ->get();

        foreach ($users as $user) {
            if($user->locations) {
                foreach($user->locations as $location) {
                    $account = ReviewSourceUrl::select('id')->where('location_id', $location->id)->get()->pluck('id')->toArray();

                    if(!$account) {
                        $empty_locations[] = $location;
                    }
                }

                if($empty_locations) {
                    try {
                        $this->sendMail($user, collect($empty_locations));
                    } catch(\Exception $exception) {
                        \Log::error("Email wasn't sent to user $user->id");
                        \Log::error($exception->getMessage());
                    }
                }
            }
        }
    }

    public function sendMail($user, $locations)
    {
        \Mail::to($user->email)->send(new StartEmail($user, $locations));
    }
}
