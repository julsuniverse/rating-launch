<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Review;
use App\Models\SentimentCategory;

class RateReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $categories = SentimentCategory::all();
        $reviews = Review::all();

        foreach ($categories as $cat) {
            $slug = strtolower($cat->text);
            foreach ($reviews as $review) {
                $score = $this->reviewScoreForWords($review, $cat->words);
                $review->{$slug} = $score;
                $review->save();
            }
        }

        echo("Rated " . $reviews->count() . " reviews\n");
    }

    function reviewScoreForWords($review, $words){
        $reviewWords = str_word_count(strtolower($review->text), 1);
        $score = 0;
        $wordCnt = 0;
        $lastWord = "";

        foreach ($reviewWords as $rWord) {
            foreach ($words as $word) {
                if ($rWord == $word->word){
                    if (in_array($lastWord, ["not", "non", "no"])){
                        $score -= $word->score;
                    }else{
                        $score += $word->score;
                    }
                }
            }
            $lastWord = $rWord;
            $wordCnt += 1;
        }
        return $score;
        return $score / max(1, $wordCnt);
    }
}
