<?php

namespace App\Jobs;

use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Subscription as StripeSubscription;

class ProcessSubscriptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param $subsriptions
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

        $date = new \DateTime('yesterday');
        // gte - Return values where the created field is after or equal to this timestamp.
        $stripe_subscriptions = StripeSubscription::all(['created' => ['gte' => $date->getTimestamp()]])->data;

        foreach($stripe_subscriptions as $stripe_subscription) {

                if(!empty($old_subscription = Subscription::where('stripe_id', $stripe_subscription->id)->first())) {
                    $subscription = $old_subscription;
                } else {
                    $subscription = new Subscription();
                }

                $subscription->customer = $stripe_subscription->customer;
                $subscription->stripe_id = $stripe_subscription->id;
                $subscription->status = $stripe_subscription->status;
                $subscription->created_at = $stripe_subscription->created;
                $subscription->save();

                echo "Subscription $stripe_subscription->id saved <br>";

        }

        echo "All subscriptions saved";
    }
}
