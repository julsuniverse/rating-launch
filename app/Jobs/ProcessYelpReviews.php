<?php

namespace App\Jobs;

use App\Services\Reviews\ParseYelpReviewsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Review;

use Carbon\Carbon;

class ProcessYelpReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;
    protected $source;
    private $reviewService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l, $s)
    {
        $this->location = $l;
        $this->source = $s;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ParseYelpReviewsService $reviewsService)
    {
        $this->reviewService = $reviewsService;

        $items = $this->reviewService->getItems($this->source->url);
        if (!$items) {
            return;
        }
        foreach ($items as $reviewDom) {

            $reviewContent = $this->reviewService->getReviewContent($reviewDom);

            if($reviewContent) {
                try {
                    $text = $this->reviewService->getText($reviewContent);
                    $name = $this->reviewService->getName($reviewDom);
                    $imgSrc = $this->reviewService->getImgSource($reviewDom);
                    $reviewId = $this->reviewService->getReviewId($reviewDom);
                    $reviewDate = $this->reviewService->getReviewDate($reviewContent);
                    $rating = $this->reviewService->getRating($reviewDom);
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    continue;
                }


                $review = Review::where('native_review_id', $reviewId)->first();

                if (empty($review)) {
                    $review = new Review;
                } else if ($review->updated_at > Carbon::now()->subDays(7)) {
                    continue;
                }

                try {
                    $review->review_source_id = 1;
                    $review->location_id = $this->location->id;
                    $review->company_id = $this->location->company_id;
                    $review->rating = floatval($rating) / 5;
                    $review->text = $text;
                    $review->user_fullname = $name;
                    $review->user_image_url = $imgSrc;
                    $review->native_review_id = $reviewId;
                    $review->native_created_at = Carbon::parse($reviewDate);
                    $review->save();
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    continue;
                }

                $this->getMessages($name, $reviewId, $review->native_created_at, $reviewDate);
            }
        }
    }

    public function getMessages($name, $reviewId, $created_at, $reviewDate)
    {
        echo("Yelp Review From: $name saved with id $reviewId \n<br>");
        echo("native_created_at: $created_at \n<br>");
        echo("reviewDate: $reviewDate \n<br>");
    }
}
