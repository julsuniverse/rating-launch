<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\ReviewSourceUrl;
use App\Models\ReviewSource;
use App\Models\ReviewCount;
use App\Models\Review;

use Carbon\Carbon;

class TotalReviewCounts implements ShouldQueue{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l){
        $this->location = $l;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        ReviewCount::where('location_id', $this->location->id)->where('created_at', '>=', Carbon::today())->delete();

        foreach (ReviewSource::all() as $source) {
            $reviews = Review::where('location_id', $this->location->id)->where('review_source_id', $source->id)->get();
            if ($reviews->count() > 0){
                $cnt = new ReviewCount;
                $cnt->location_id = $this->location->id;
                $cnt->review_source_id = $source->id;
                $cnt->count = $reviews->count();
                $cnt->rating = $reviews->sum('rating') / $reviews->count();
                $cnt->company_id = $this->location->company_id;
                $cnt->save();
            }
        }
    }
}
