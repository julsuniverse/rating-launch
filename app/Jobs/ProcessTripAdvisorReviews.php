<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\Review;

class ProcessTripAdvisorReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;
    protected $source;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l, $s){
        $this->location = $l;
        $this->source = $s;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $html = new \Htmldom($this->source->url);
        echo ($html);
        
        $items = $html->find('.review-container');
        foreach ($items as $reviewDom) {
            echo($reviewDom);
            continue;
            $reviewContent = $reviewDom->find('.review-content', 0);
            
            if (empty($reviewContent)){
                continue;
            }

            $text = strip_tags($reviewContent->find('p', 0));
            $name = $reviewDom->find('.user-name', 0)->find('a', 0)->innertext;
            $imgSrc = $reviewDom->find('.photo-box-img', 0)->src;
            $reviewId = trim($reviewDom->find(".review", 0)->getAttribute("data-review-id"));

            $ratingText = $reviewDom->find('.i-stars', 0)->title;
            $rating = explode(" ", $ratingText)[0];

            $review = Review::where('native_review_id', $reviewId)->first();
            if (empty($review)){
                $review = new Review;
            }

            $review->review_source_id = 1;
            $review->location_id = $this->location->id;
            $review->company_id = $this->location->company_id;
            $review->rating = floatval($rating) / 5;
            $review->text = $text;
            $review->user_fullname = $name;
            $review->user_image_url = $imgSrc;
            $review->native_review_id = $reviewId;
            $review->save();

            echo("Yelp Review From: $name saved with id $reviewId<br>");
        }

    }
}
