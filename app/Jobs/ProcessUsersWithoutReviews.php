<?php

namespace App\Jobs;

use App\Mail\StartCollectReviewsEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessUsersWithoutReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $no_review_locations = array();

        $users = User::where('created_at', "<", Carbon::now()->subDays(3))
            ->where('created_at', ">", Carbon::now()->subDays(4))
            ->get();

        foreach ($users as $user) {
            if($user->locations) {
                $no_review_locations = array();
                foreach($user->locations as $location) {
                    $count = $location->reviews->count();

                    if(!$count > 0) {
                        $no_review_locations[] = $location;
                    }
                }
                if($no_review_locations) {
                    try {
                        $this->sendMail($user, collect($no_review_locations));
                    } catch(\Exception $exception) {
                        \Log::error("Email wasn't sent to user $user->id");
                        \Log::error($exception->getMessage());
                    }
                }

            }
        }
    }

    public function sendMail($user, $locations)
    {
        \Mail::to($user->email)->send(new StartCollectReviewsEmail($locations));
    }
}
