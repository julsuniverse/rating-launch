<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Location;
use App\Models\ReviewSourceUrl;
use App\Models\Review;

use Carbon\Carbon;

class ProcessGoogleReviews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $location;
    protected $source;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($l, $s){
        $this->location = $l;
        $this->source = $s;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $client = new \Google_Client();
        $client->setAuthConfig(env('GOOGLE_BUSSINESS_PATH'));
        $client->refreshToken($this->source->data2);

        require app_path('/Services/Google_Service_MyBusiness.php'); //Fuck google
        $bussiness = new \Google_Service_MyBusiness($client); 
        $gReviews = $bussiness->accounts_locations_reviews->listAccountsLocationsReviews($this->source->data1)["reviews"];

        if (!empty($gReviews)){
            foreach ($gReviews as $gReview) {
                $time = Carbon::parse($gReview["updateTime"]);
                $rating = ["ONE"=>.2, "TWO"=>.4, "THREE"=>.6, "FOUR"=>.8, "FIVE"=>1][$gReview["starRating"]];
                $text = $gReview["comment"];
                if (empty($text)){
                    $text = "--";
                }

                $name = $gReview["reviewer"]["displayName"];
                $imgSrc = "/images/users/hidden_user.jpg";
                $reviewId = $gReview["reviewId"];

                $review = Review::where('native_review_id', $reviewId)->first();
                if (empty($review)){
                    $review = new Review;
                }else if ($review->updated_at > Carbon::now()->subDays(7)){
                  continue;
                }

                $review->review_source_id = 6;
                $review->location_id = $this->location->id;
                $review->company_id = $this->location->company_id;
                $review->rating = $rating;
                $review->text = $text;
                $review->user_fullname = $name;
                $review->user_image_url = $imgSrc;
                $review->native_review_id = $reviewId;
                $review->native_created_at = $time;

                $review->save();

                echo("Google Review From: $name saved with id $reviewId<br>");
            }
        }

    }
}
