<?php

namespace App\Events;

use App\Models\Review;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CheckNewReviews
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $review;

    /*
     * @return void
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }
}
