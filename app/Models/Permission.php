<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;

    public function company_users()
    {
        return $this->belongsToMany('App\Models\CompanyUser', 'company_user_permissions');
    }
}
