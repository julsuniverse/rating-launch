<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model{
     
	protected $table='company';
    
	function locations(){
		return $this->hasMany('App\Models\Location');
	}

	function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

	function getAddressAttribute(){
		return $this->locations->first()->address;
	}

	function reviewCounts(){
		return $this->hasMany('App\Models\ReviewCount');
	}

	function media(){
		return $this->hasMany('App\Models\LocationMedia');
	}

	function getMediaUrlAttribute(){
		$media = $this->media;
		$primary = $media->filter(function($m){
			return $m->is_primary;
		});

		if ($primary->count() > 0){
			return $primary->first()->url;
		}else if ($media->count() > 0){
			return $media->first()->url;
		}
		return NULL;
	}
}
