<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReviewCount extends Model{
     
	protected $table='review_counts';
    
    function source(){
    	return $this->belongsTo('App\Models\ReviewSource', 'review_source_id', 'id');
    }
}
