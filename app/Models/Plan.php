<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model{
     
    function priceForCoupon($code=""){
    	\Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

        try {
            $coupon = \Stripe\Coupon::retrieve($code);

            return Plan::priceForCoupon($this->price, $coupon);
        } catch (\Stripe\Error\Base $e) {
        } catch (Exception $e) {
        }

        return $this->price;
    }

    static function priceWithCoupon($price, $coupon){
    	if (!empty($coupon) && isset($coupon)){
	    	if ($coupon->valid){
	        	if ($coupon->max_redemptions > $coupon->times_redeemed){
	        		if ($coupon->amount_off != NULL){
	        			$amount = doubleval($coupon->amount_off);
	        			return max(0, $price - $amount);
	        		}elseif ($coupon->percent_off != NULL){
	        			$percent = doubleval($coupon->percent_off / 100);
	        			return max(0, $price * $percent);
	        		}
	        	}
	        }
	    }

        return $price;
    }
}
