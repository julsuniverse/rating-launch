<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceType extends Model
{
    function locations()
    {
        return $this->hasMany('App\Models\Location');
    }
}
