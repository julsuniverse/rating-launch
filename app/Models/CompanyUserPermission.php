<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyUserPermission extends Model
{
    protected $table = 'company_user_permissions';

    protected $fillable = ['permission_id', 'company_user_id'];

    public $timestamps = false;
}
