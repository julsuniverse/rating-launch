<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Communication extends Model{
     
	function reviewer(){
		return $this->belongsTo('App\Models\Reviewer');
	}

	function sender(){
		return $this->belongsTo('App\Models\User', 'sender_id');
	}
}
