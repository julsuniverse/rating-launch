<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReviewSourceUrl extends Model{
     
	protected $table='review_source_url';
    
    function type(){
    	return $this->belongsTo('App\Models\ReviewSource', 'review_category_id');
    }

    function getRateUrlAttribute(){
    	if ($this->review_category_id == 3){
    		return "https://www.facebook.com/".$this->data1."/reviews/";
    	}elseif ($this->review_category_id == 6){
    		return "https://search.google.com/local/writereview?placeid=".$this->data3;
    	}elseif ($this->review_category_id == 1){
            return "https://www.yelp.com/writeareview/biz/".$this->data1;
        }
    	return $this->url;
    }
}
