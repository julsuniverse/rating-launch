<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Location extends Model{
     
    use SoftDeletes;
    
	function sources(){
		return $this->hasMany('App\Models\ReviewSourceUrl');
	}

	function reviews(){
		return $this->hasMany('App\Models\Review');
	}

	function reviewCounts(){
		return $this->hasMany('App\Models\ReviewCount');
	}

	function media(){
		return $this->hasMany('App\Models\LocationMedia');
	}

    function placeType()
    {
        return $this->belongsTo('App\Models\PlaceType', 'place_type');
    }

	function getAverageRatingAttribute(){
		$reviews = $this->reviews;
		$cnt = max(1, $reviews->count());
		return number_format($reviews->sum('rating') / $cnt * 5, 1);
	}

	function getAddressAttribute(){
		return $this->street . ", " . $this->city . ", " . $this->state . " " . $this->zip;
	}

	function getHashedIdAttribute(){
		return base64_encode($this->id * 12345);
	}

	function getMediaUrlAttribute(){
		$media = $this->media;
		$primary = $media->filter(function($m){
			return $m->is_primary;
		});

		if ($primary->count() > 0){
			return $primary->first()->url;
		}else if ($media->count() > 0){
			return $media->first()->url;
		}
		return NULL;
	}

	function getRatingAttribute(){
		$reviews = $this->reviews;
		if ($reviews->count() > 0){
	        return $reviews->sum('rating') / $reviews->count();
	    }else{
	        return 1;
	    }
	}

	static function findForHash($hash){
		$id = intval(base64_decode($hash) / 12345);
		return Location::find($id);
	}

}
