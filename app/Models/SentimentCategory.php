<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SentimentCategory extends Model{
     
	protected $table='sentiment_category';
    
    function words(){
    	return $this->hasMany('App\Models\SentimentWord');
    }
}
