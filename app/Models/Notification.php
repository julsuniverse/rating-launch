<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notification extends Model{
     
	protected $table='notifications';

	protected $fillable = ['text', 'uri', 'notification_type_id', 'read', 'user_id', 'review_id'];

    /**
     * @param $value
     * @return string
     */
	public function getUriAttribute($value)
    {
       return $value . '/1';
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $review_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSetNotificationRead($query, $review_id)
    {
        return $query->where([
            'uri' => 'reviews/'.$review_id,
            'read' => 0
        ])->update(['read' => 1]);
    }
}
