<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EditableLocation extends Model
{
    protected $fillable = ['company_user_permissions_id', 'location_id'];

    public $timestamps = false;
}
