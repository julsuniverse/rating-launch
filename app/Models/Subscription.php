<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function scopeActive($query)
    {
       return $query->where('status', 'active');
    }

    public function scopeCanceled($query)
    {
       return $query->where('status', 'canceled');
    }

    public function scopeTrialing($query)
    {
       return $query->where('status', 'trialing');
    }
}
