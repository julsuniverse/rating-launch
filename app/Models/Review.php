<?php

namespace App\Models;

use App\Events\CheckNewReviews;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model{
     
    use SoftDeletes;

    protected $events = [
        //'saved' => CheckNewReviews::class,
        'created' => CheckNewReviews::class,
    ];

    function location(){
    	return $this->belongsTo('App\Models\Location');
    }

    function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    function source()
    {
        return $this->belongsTo('App\Models\ReviewSource', 'review_source_id');
    }

    function getFormattedTimeAttribute(){
    	if (!empty($this->native_created_at)){
    		$time = strtotime($this->native_created_at);
    		if ($time > time()-86400){ // today
    			return "Today";
    		}else if ($time > time() - 86400 * 2){ // yesterday
    			return "Yesterday";
    		}else if ($time > time() - 86400 * 7){ // last week
    			return "This Week";
    		}else if ($time < time() - 86400 * 365 * 10){ // 10 years ago
    			return "A Long Time Ago";
    		}
            return date('F  j, Y', $time);
    	}
    	return "";
    }

    function reviewer(){
        return $this->hasOne('App\Models\Reviewer');
    }

    function getIconHtmlAttribute(){
        if ($this->review_source_id == 1){
           return '<i class="mdi mdi-yelp"></i>';
        }elseif ($this->review_source_id == 2){
            return '<i class="mdi mdi-foursquare"></i>';
        }elseif ($this->review_source_id == 3){
            return '<i class="mdi mdi-facebook"></i>';
        }elseif ($this->review_source_id == 5){
            return '<i class="mdi mdi-home-modern"></i>';
        }
        return '';
    }

    function getShortTextAttribute(){
        if (strlen($this->text) > 100){
            return substr($this->text, 0, 100) . "..";
        }
        return $this->text;
    }
}
