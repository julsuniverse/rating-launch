<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reviewer extends Model{

	function review(){
		return $this->belongsTo('App\Models\Review');
	}

	function communications(){
		return $this->hasMany('App\Models\Communication');
	}

	function location(){
    	return $this->belongsTo('App\Models\Location');
    }

    
}