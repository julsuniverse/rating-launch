<?php

namespace App\Models;

use App\Notifications\PasswordReset;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    function companies(){
        return $this->belongsToMany('App\Models\Company');
    }

    function companyUser()
    {
        return $this->hasOne(CompanyUser::class);
    }

    function getLocationsAttribute(){
        $locations = $this->companies->load('locations')->map(function($c){
            return $c->locations;
        })->reduce(function ($carry, $item) {
            if ($carry != null){
                return $carry->merge($item);
            }
            return $item;
        });
        return $locations;
    }

    function getGravatarUrlAttribute(){
        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->email ) ) ) . "&s=" . "100";
    }

    function createStripeCustomer(){
        if ($this->stripe_id == NULL){
            \Stripe\Stripe::setApiKey(env('STRIPE_SEC'));

            $customer = \Stripe\Customer::create([
                'email' => $this->email,
            ]);

            $this->stripe_id = $customer["id"];
            $this->save();
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    function getHashedIdAttribute()
    {
        return base64_encode($this->id * 12345);
    }

    static function findByHash($hash)
    {
        $id = intval(base64_decode($hash) / 12345);
        return self::find($id);
    }

}
