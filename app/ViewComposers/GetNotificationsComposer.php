<?php

namespace App\ViewComposers;

use App\Models\Notification;
use Illuminate\View\View;

class GetNotificationsComposer
{
    public function compose(View $view){
        if($user = \Auth::user()) {

            $notifications = Notification::where([
                'user_id' => $user->id,
                'read' => 0
            ]);

            $notifications_count = $notifications->count();
            $notifications = $notifications->orderBy('id', 'DESC')->limit(5)->get();

            $view->with('notifications', $notifications ?? null)->with('notifications_count', $notifications_count);
        }
    }
}