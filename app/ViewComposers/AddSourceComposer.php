<?php

namespace App\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Services\Google_Service_MyBusiness;

class AddSourceComposer{

    public function __construct(){

    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     * @throws \Google_Exception
     */
    public function compose(View $view){
        $request = request();
        $currentUri = $request->path();

        $client = new \Google_Client();
        $client->setAuthConfig(env('GOOGLE_BUSSINESS_PATH'));

        if (empty($request->cookie('google_refresh'))){
            $client->addScope("https://www.googleapis.com/auth/plus.business.manage");
            $client->setRedirectUri(config('app.url').'/oauth/googleredirect');
            $client->setState($currentUri);
            $client->setAccessType("offline");
            $client->setApprovalPrompt("force");
            $url = $client->createAuthUrl();

            $view->with('googleUrl', $url);
            $view->with('googleLoggedIn', false);
        }else{
            $view->with('googleLoggedIn', true);
        }
    }
}