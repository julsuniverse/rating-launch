<?php

namespace App\Mail;

use App\Models\Location;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class StartEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $link;
    public $user;
    public $locations;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Collection $location
     */
    public function __construct(User $user, Collection $location)
    {
        $this->locations = $location;
        $this->user = $user;
        $this->link = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Start Using RatingLaunch Today')
            ->view('email.start');
    }
}
