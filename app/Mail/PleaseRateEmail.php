<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PleaseRateEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $reviewer;
    protected $location;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($r, $l)
    {
        $this->reviewer = $r;
        $this->location = $l;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->view('email.please-rate', ['location'=> $this->location, 'reviewer'=> $this->reviewer])
                ->subject('Please Rate Your Recent Experience With Us!')
                ->from(env('SENDING_EMAIL'), $this->location->text);
    }
}
