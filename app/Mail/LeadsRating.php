<?php

namespace App\Mail;

use App\Models\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeadsRating extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $lead;
    public $platform;
    public $rating;
    public $stars;
    public $company;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Lead $lead, $platform, $rating)
    {
        $this->lead = $lead;
        $this->platform = $platform;
        $this->rating = $rating;
        $this->company = "RatingLaunch";
        $this->stars = $this->countStars($this->rating);
        $this->link = config('app.url').'/';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //TODO: change email template by place_type
        return $this->subject("Is the $this->rating star rating on $this->platform true?")
            ->view('email.leads.dental');
    }

    public function countStars($rating)
    {
        if($rating > 1) {
            return "stars";
        } else {
            return "star";
        }
    }
}
