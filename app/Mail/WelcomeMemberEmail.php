<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeMemberEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $link;
    public $password;

    /**
     * Create a new message instance.
     *
     * @param $password
     */
    public function __construct($password)
    {
        $this->link = config('app.url').'/';
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('You\'re Now on RatingLaunch')
            ->view('email.welcome');
    }
}
