<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;


class StartCollectReviewsEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $link;
    public $locations;

    /**
     * Create a new message instance.
     *
     * @param Collection $location
     */
    public function __construct(Collection $locations)
    {
        $this->locations = $locations;
        $this->link = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.collect-reviews')
            ->subject('Start Collecting Reviews Today');
    }
}
