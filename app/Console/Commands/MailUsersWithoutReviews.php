<?php

namespace App\Console\Commands;

use App\Jobs\ProcessUsersWithoutReviews;
use Illuminate\Console\Command;

class MailUsersWithoutReviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:reviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to users who have not collected reviews yet.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ProcessUsersWithoutReviews::dispatch();
        $this->info('Done!');
    }
}
