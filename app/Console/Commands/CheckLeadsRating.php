<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckLeadsRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:rating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check lead rating';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Checking Leads Rating...');
        \App\Jobs\Leads\CheckLeadsRating::dispatch();

        $this->info('Checking Leads Reviews...');
        \App\Jobs\Leads\CheckLeadsReviews::dispatch();

        $this->info('Done!');
    }
}
