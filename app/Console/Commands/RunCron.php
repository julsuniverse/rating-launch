<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;

class RunCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:reviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run cron for reviews';

    public function handle()
    {
        $request = Request::create(('cron/reviews'), 'GET');
        $this->info(app()->make(\Illuminate\Contracts\Http\Kernel::class)->handle($request));
    }
}
