<?php

namespace App\Console\Commands;

use App\Jobs\ProcessUsersWithoutAccounts;
use Illuminate\Console\Command;

class MailUsersWithoutAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to new users without related accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ProcessUsersWithoutAccounts::dispatch();
        $this->info('Done!');
    }
}
