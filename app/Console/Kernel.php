<?php

namespace App\Console;

use App\Console\Commands\CheckLeadsRating;
use App\Console\Commands\MailUsersWithoutReviews;
use App\Console\Commands\MailUsersWithoutAccounts;
use App\Console\Commands\RunCron;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RunCron::class,
        MailUsersWithoutAccounts::class,
        MailUsersWithoutReviews::class,
        CheckLeadsRating::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
