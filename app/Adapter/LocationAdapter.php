<?php

namespace App\Adapter;

/**
 * Class LocationAdapter
 * @package App\Adapter
 * @property string $address
 * @property string $company_name
 */
class LocationAdapter
{
    public $address;
    public $company_name;

    public function adaptLead($lead)
    {
        if (isset($lead->address_1)) {
            $this->address = $lead->address_1;
        } else if (isset($lead->address_2)) {
            $this->address = $lead->address_2;
        }

        if (isset($lead->city)) {
            $this->address .= ' ' . $lead->city;
        }
        if (isset($lead->state)) {
            $this->address .= ' ' . $lead->state;
        }
        if (isset($lead->zip)) {
            $this->address .= ' ' . $lead->zip;
        }

        if (isset($lead->company_name)) {
            $this->company_name = $lead->company_name;
        }

        return $this;
    }

    public function adaptLocation($location)
    {
        if (isset($location->street)) {
            $this->address = $location->street;
        }

        if (isset($location->city)) {
            $this->address .= ' ' . $location->city;
        }
        if (isset($location->state)) {
            $this->address .= ' ' . $location->state;
        }
        if (isset($location->zip)) {
            $this->address .= ' ' . $location->zip;
        }
        if (isset($location->country)) {
            $this->address .= ' ' . $location->country;
        }

        if (isset($location->text)) {
            $this->company_name = $location->text;
        }

        return $this;
    }
}